package com.example.yopolskyi.taskmanager;


import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;

import com.example.yopolskyi.taskmanager.views.loginView.LoginActivity;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static org.hamcrest.Matchers.notNullValue;

@RunWith(AndroidJUnit4.class)
public class LoginActivityTest {

    @Rule
    public ActivityTestRule<LoginActivity> rule = new ActivityTestRule<>(LoginActivity.class);

    private LoginActivity activity;

    @Before
    public void setUp(){
        activity = rule.getActivity();
    }

    @Test
    public void testInitItems() throws Exception {
        onView(withId(R.id.activity_login_ed_username)).check(matches(notNullValue()));
        onView(withId(R.id.activity_login_ed_password)).check(matches(notNullValue()));
        onView(withId(R.id.activity_login_btn_sign_in)).check(matches(notNullValue()));
    }
}
