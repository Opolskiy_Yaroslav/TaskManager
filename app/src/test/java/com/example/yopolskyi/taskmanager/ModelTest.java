package com.example.yopolskyi.taskmanager;

import com.example.yopolskyi.taskmanager.common.Constants;
import com.example.yopolskyi.taskmanager.common.ResultObject;
import com.example.yopolskyi.taskmanager.common.TokensCombination;
import com.example.yopolskyi.taskmanager.common.Utils;
import com.example.yopolskyi.taskmanager.common.dependencyInjection.main.MainComponentAssistant;
import com.example.yopolskyi.taskmanager.common.enumerations.AuthorizationTokenTypes;
import com.example.yopolskyi.taskmanager.common.enumerations.ResultMessage;
import com.example.yopolskyi.taskmanager.model.IModel;
import com.example.yopolskyi.taskmanager.model.dal.IRepository;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.Calendar;
import java.util.Date;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class ModelTest {

    @Mock
    IRepository repository;

    private IModel model;

    @Before
    public void setUp(){
        MockitoAnnotations.initMocks(this);

        MainComponentAssistant.createComponent();
        model = MainComponentAssistant.getComponent().getModel();

        when(repository.getUser(Constants.USERNAME_1, Constants.PASSWORD_1)).thenReturn(1);
        when(repository.getUser(Constants.USERNAME_2, Constants.PASSWORD_2)).thenReturn(2);
        when(repository.getUser("incorrect_user", "incorrect_password")).thenReturn(-1);

        model.setRepository(repository);
    }

    @Test
    public void testDoLogin() throws Exception{
        // First username/password combination.
        ResultObject doLoginResultObject = model.doLogin(Constants.USERNAME_1, Constants.PASSWORD_1);

        assertEquals(ResultMessage.SUCCESSFUL, doLoginResultObject.getResultMessage());
        assertEquals(1, doLoginResultObject.getResultValue());


        // Second username/password combination.
        doLoginResultObject = model.doLogin(Constants.USERNAME_2, Constants.PASSWORD_2);

        assertEquals(ResultMessage.SUCCESSFUL, doLoginResultObject.getResultMessage());
        assertEquals(2, doLoginResultObject.getResultValue());


        // Incorrect username/password combination.
        doLoginResultObject = model.doLogin("incorrect_user", "incorrect_password");

        assertEquals(ResultMessage.SUCCESSFUL, doLoginResultObject.getResultMessage());
        assertEquals(-1, doLoginResultObject.getResultValue());
    }

    @Test
    public void testGenerateTokens() throws Exception{
        int userId = 1;
        TokensCombination tokensCombination = model.generateTokens(userId);

        String currentDate = Utils.convertDateToString(new Date(), Constants.TOKENS_DATE_FORMAT);

        Calendar calendar = Calendar.getInstance();
        calendar.add(Calendar.DATE, Constants.ACCESS_TOKEN_VALIDITY_IN_DAYS);
        Date accessTokenExpirationDate = calendar.getTime();

        calendar = Calendar.getInstance();
        calendar.add(Calendar.DATE, Constants.REFRESH_TOKEN_VALIDITY_IN_DAYS);
        Date refreshTokenExpirationDate = calendar.getTime();

        assertEquals(tokensCombination.getAccessToken().getUserID(), userId);
        assertEquals(tokensCombination.getAccessToken().getTokenType(), AuthorizationTokenTypes.Access);
        assertEquals(tokensCombination.getAccessToken().getTokenValue(), userId + "_" + currentDate);
        assertEquals(
                Utils.convertDateToString(tokensCombination.getAccessToken().getTokenExpirationDate(),
                    Constants.TOKENS_DATE_FORMAT),
                Utils.convertDateToString(accessTokenExpirationDate,
                    Constants.TOKENS_DATE_FORMAT));

        assertEquals(tokensCombination.getRefreshToken().getUserID(), userId);
        assertEquals(tokensCombination.getRefreshToken().getTokenType(), AuthorizationTokenTypes.Refresh);
        assertEquals(tokensCombination.getRefreshToken().getTokenValue(), userId + "_" + currentDate);
        assertEquals(
                Utils.convertDateToString(tokensCombination.getRefreshToken().getTokenExpirationDate(),
                    Constants.TOKENS_DATE_FORMAT),
                Utils.convertDateToString(refreshTokenExpirationDate,
                        Constants.TOKENS_DATE_FORMAT));
    }
}
