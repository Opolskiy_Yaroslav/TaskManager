package com.example.yopolskyi.taskmanager;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;

import com.example.yopolskyi.taskmanager.common.dependencyInjection.main.MainComponentAssistant;
import com.example.yopolskyi.taskmanager.model.IModel;

import javax.inject.Inject;

import static android.content.Context.MODE_PRIVATE;

/**
 * Catches alarms every day at 00:00 and refreshes task statuses to overdue if needed.
 */
public class TaskStatusesRefreshReceiver extends BroadcastReceiver {

    @Inject
    IModel model;

    private SharedPreferences sharedPreferences;
    private String accessToken;

    @Override
    public void onReceive(Context context, Intent intent) {
        MainComponentAssistant.createComponent();
        MainComponentAssistant.getComponent().injectTaskStatusesRefreshReceiver(this);

        MainComponentAssistant.getComponent().getRepository().setContext(context);

        sharedPreferences = context.getSharedPreferences(context.getPackageName()
                + "_preferences", MODE_PRIVATE);

        accessToken = sharedPreferences.getString("Access token", "Not defined");

        model.refreshTaskStatuses(accessToken);
    }
}
