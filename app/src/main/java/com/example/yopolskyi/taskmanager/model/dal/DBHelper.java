package com.example.yopolskyi.taskmanager.model.dal;

import android.content.ContentValues;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import com.example.yopolskyi.taskmanager.common.Constants;
import com.example.yopolskyi.taskmanager.common.enumerations.AuthorizationTokenTypes;
import com.example.yopolskyi.taskmanager.common.enumerations.TaskExecutionStatuses;
import com.example.yopolskyi.taskmanager.common.enumerations.TaskRepeatFrequencies;

/**
 * Represents application database structure and implement methods for creating and updating database.
 */
public class DBHelper extends SQLiteOpenHelper {

    // For logging.
    private final String TAG = "DBHelper";

    private static final String DB_NAME = "dbTaskManager";
    private static final int DB_VERSION = 1;

    /**
     * Table "Users". Contains a username-password combination.
     * Values are determined statically in the /common/Constants file.
     */
    static final String TABLE_USERS = "tblUsers";

    static final String TABLE_USERS_KEY_ID = "iUserId";
    static final String TABLE_USERS_KEY_USERNAME = "tUsername";
    static final String TABLE_USERS_KEY_PASSWORD = "tPassword";

    /**
     * Table "Token types". Contains token types determined in
     * common/enumerations/AuthorizationTokenTypes enum.
     */
    private static final String TABLE_TOKEN_TYPES = "tblTokenTypes";

    private static final String TABLE_TOKEN_TYPES_KEY_ID = "iTokenTypeId";
    private static final String TABLE_TOKEN_TYPES_KEY_VALUE = "tTokenTypeValue";

    /**
     * Table "Authorization tokens". Contains tokens for every user. By default every user has
     * a access and refresh token with their own expiration dates (tokens validity determines
     * in the /common/Constants file). New record creates every time when user successfully sign in.
     * Record deletes when user is logged out.
     */
    static final String TABLE_AUTHORIZATION_TOKENS = "tblAuthorizationTokens";

    private static final String TABLE_AUTHORIZATION_TOKENS_KEY_ID = "iTokenId";
    static final String TABLE_AUTHORIZATION_TOKENS_KEY_USER_ID = "iUserId";
    static final String TABLE_AUTHORIZATION_TOKENS_KEY_VALUE = "tTokenValue";
    static final String TABLE_AUTHORIZATION_TOKENS_KEY_TOKEN_TYPE_ID = "iTokenTypeId";
    static final String TABLE_AUTHORIZATION_TOKENS_KEY_EXPIRATION_DATE = "tExpirationDate";

    /**
     * Table "Task repeat frequencies". Contains task repeat frequencies determined in
     * common/enumerations/TaskRepeatFrequencies enum.
     */
    private static final String TABLE_TASK_REPEAT_FREQUENCIES = "tblTaskRepeatFrequencies";

    private static final String TABLE_TASK_REPEAT_FREQUENCIES_KEY_ID = "iRepeatFrequencyId";
    private static final String TABLE_TASK_REPEAT_FREQUENCIES_KEY_VALUE = "tRepeatFrequencyValue";

    /**
     * Table "Task statuses". Contains task repeat frequencies determined in
     * common/enumerations/TaskExecutionStatuses enum.
     */
    private static final String TABLE_TASK_STATUSES = "tblTaskStatuses";

    private static final String TABLE_TASK_STATUSES_KEY_ID = "iTaskStatusId";
    private static final String TABLE_TASK_STATUSES_KEY_VALUE = "tTaskStatusValue";

    /**
     * Table "Tasks". Contains information about every task created by users.
     */
    static final String TABLE_TASKS = "tblTasks";

    static final String TABLE_TASKS_KEY_ID = "iTaskId";
    static final String TABLE_TASKS_KEY_CONTENT = "tTaskContent";
    static final String TABLE_TASKS_KEY_DUE_DATE = "tDueDate";
    static final String TABLE_TASKS_KEY_DUE_TIME = "tDueTime";
    static final String TABLE_TASKS_KEY_REPEAT_FREQUENCY = "iRepeatFrequencyId";
    static final String TABLE_TASKS_KEY_STATUS_ID = "iStatusId";
    static final String TABLE_TASKS_KEY_USER_ID = "iUserId";


    // FIELDS
    private static Context context;
    private ContentValues contentValues;

    /**
     * Class constructor.
     */
    public DBHelper() {
        super(context, DB_NAME, null, DB_VERSION);
        contentValues = new ContentValues();
    }

    public static void setContext(Context context){
        DBHelper.context = context;
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        Log.d(TAG, ">> [onCreate]");

        Log.d(TAG, "Calls createTables method.");
        createTables(db);

        Log.d(TAG, "Calls populateTables method.");
        populateTables(db);

        Log.d(TAG, "<< [onCreate]");
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }

    /**
     * Creates all application database tables.
     * @param db application database
     */
    private void createTables(SQLiteDatabase db){
        Log.d(TAG, ">> [createTables]");

        // Authorization tables.
        try{
            db.execSQL(String.format("CREATE TABLE %s (%s INTEGER PRIMARY KEY AUTOINCREMENT, %s TEXT, " +
                            "%s TEXT);",
                    TABLE_USERS,
                    TABLE_USERS_KEY_ID,
                    TABLE_USERS_KEY_USERNAME,
                    TABLE_USERS_KEY_PASSWORD ));

            Log.d(TAG, "Table " + TABLE_USERS + " was successfully created.");
        }
        catch (Exception e){
            Log.e(TAG, "Could not create " + TABLE_USERS + " table. " + e.toString());
            throw e;
        }

        try{
            db.execSQL(String.format("CREATE TABLE %s(%s INTEGER PRIMARY KEY AUTOINCREMENT, %s TEXT);",
                    TABLE_TOKEN_TYPES,
                    TABLE_TOKEN_TYPES_KEY_ID,
                    TABLE_TOKEN_TYPES_KEY_VALUE));
            Log.d(TAG, "Table " + TABLE_TOKEN_TYPES + " was successfully created.");
        }
        catch (Exception e){
            Log.e(TAG, "Could not create " + TABLE_TOKEN_TYPES + " table. " + e.toString());
            throw e;
        }

        try{
            db.execSQL(String.format("CREATE TABLE %s(%s INTEGER PRIMARY KEY AUTOINCREMENT, " +
                            "%s INTEGER, %s TEXT, %s INTEGER, %s TEXT);",
                    TABLE_AUTHORIZATION_TOKENS,
                    TABLE_AUTHORIZATION_TOKENS_KEY_ID,
                    TABLE_AUTHORIZATION_TOKENS_KEY_USER_ID,
                    TABLE_AUTHORIZATION_TOKENS_KEY_VALUE,
                    TABLE_AUTHORIZATION_TOKENS_KEY_TOKEN_TYPE_ID,
                    TABLE_AUTHORIZATION_TOKENS_KEY_EXPIRATION_DATE));
            Log.d(TAG, "Table " + TABLE_AUTHORIZATION_TOKENS + " was successfully created.");
        }
        catch (Exception e){
            Log.e(TAG, "Could not create " + TABLE_AUTHORIZATION_TOKENS + " table. "
                    + e.toString());
            throw e;
        }

        // Tasks tables.
        try{
            db.execSQL(String.format("CREATE TABLE %s(%s INTEGER PRIMARY KEY AUTOINCREMENT, %s TEXT);",
                    TABLE_TASK_REPEAT_FREQUENCIES,
                    TABLE_TASK_REPEAT_FREQUENCIES_KEY_ID,
                    TABLE_TASK_REPEAT_FREQUENCIES_KEY_VALUE));
            Log.d(TAG, "Table " + TABLE_TASK_REPEAT_FREQUENCIES + " was successfully created.");
        }
        catch (Exception e){
            Log.e(TAG, "Could not create " + TABLE_TASK_REPEAT_FREQUENCIES + " table. " +
                    e.toString());
            throw e;
        }

        try{
            db.execSQL(String.format("CREATE TABLE %s(%s INTEGER PRIMARY KEY AUTOINCREMENT, %s TEXT);",
                    TABLE_TASK_STATUSES,
                    TABLE_TASK_STATUSES_KEY_ID,
                    TABLE_TASK_STATUSES_KEY_VALUE));
            Log.d(TAG, "Table " + TABLE_TASK_STATUSES + " was successfully created.");
        }
        catch (Exception e){
            Log.e(TAG, "Could not create " + TABLE_TASK_STATUSES + " table. " + e.toString());
            throw e;
        }

        try{
            db.execSQL(String.format("CREATE TABLE %s (%s INTEGER PRIMARY KEY AUTOINCREMENT, " +
                            "%s TEXT, %s TEXT, %s TEXT, %s INTEGER, %s INTEGER, %s INTEGER);",
                    TABLE_TASKS,
                    TABLE_TASKS_KEY_ID,
                    TABLE_TASKS_KEY_CONTENT,
                    TABLE_TASKS_KEY_DUE_DATE,
                    TABLE_TASKS_KEY_DUE_TIME,
                    TABLE_TASKS_KEY_REPEAT_FREQUENCY,
                    TABLE_TASKS_KEY_STATUS_ID,
                    TABLE_TASKS_KEY_USER_ID));
            Log.d(TAG, "Table " + TABLE_TASKS + " was successfully created.");
        }
        catch (Exception e){
            Log.e(TAG, "Could not create " + TABLE_TASKS + " table. " + e.toString());
            throw e;
        }

        Log.d(TAG, "All tables were created.");
        Log.d(TAG, "<< [createTables]");
    }

    /**
     * Fills the tables with initial values (token types, task repeat frequencies,
     * task execution statuses and default username-password combinations).
     * @param db application database
     */
    private void populateTables(SQLiteDatabase db){
        Log.d(TAG, ">> [populateTables]");

        Log.d(TAG, "Try to populate " + TABLE_TOKEN_TYPES + " table.");

        for(AuthorizationTokenTypes tokenType : AuthorizationTokenTypes.values()){
            contentValues.put(TABLE_TOKEN_TYPES_KEY_VALUE, tokenType.toString());
            try{
                long id = db.insert(TABLE_TOKEN_TYPES, null, contentValues);
                Log.d(TAG, "ID = " + id + " Value = " + tokenType.toString());
            }
            catch (Exception e){
                Log.e(TAG, "Could not insert data into the "
                        + TABLE_TOKEN_TYPES + " table. " +  e.toString());
                throw e;
            }
            finally {
                contentValues.clear();
            }
        }

        Log.d(TAG, "Try to populate " + TABLE_TASK_REPEAT_FREQUENCIES + " table.");

        for(TaskRepeatFrequencies repeatFrequency : TaskRepeatFrequencies.values()){
            contentValues.put(TABLE_TASK_REPEAT_FREQUENCIES_KEY_VALUE, repeatFrequency.toString());
            try{
                long id = db.insert(TABLE_TASK_REPEAT_FREQUENCIES, null, contentValues);
                Log.d(TAG, "ID = " + id + " Value = " + repeatFrequency.toString());
            }
            catch (Exception e){
                Log.e(TAG, "Could not insert data into the "
                                + TABLE_TASK_REPEAT_FREQUENCIES + " table. " + e.toString());
                throw e;
            }
            finally {
                contentValues.clear();
            }
        }

        Log.d(TAG, "Try to populate " + TABLE_TASK_STATUSES + " table.");

        for(TaskExecutionStatuses executionStatus : TaskExecutionStatuses.values()){
            contentValues.put(TABLE_TASK_STATUSES_KEY_VALUE, executionStatus.toString());
            try{
                long id = db.insert(TABLE_TASK_STATUSES, null, contentValues);
                Log.d(TAG, "ID = " + id + " Value = " + executionStatus.toString());
            }
            catch (Exception e){
                Log.e(TAG, "Could not insert data into the "
                        + TABLE_TASK_STATUSES + " table. " + e.toString());
                throw e;
            }
            finally {
                contentValues.clear();
            }
        }


        // Insert default username-password combinations.
        Log.d(TAG, "Try to populate  " + TABLE_USERS + " table.");

        contentValues.put(TABLE_USERS_KEY_USERNAME, Constants.USERNAME_1);
        contentValues.put(TABLE_USERS_KEY_PASSWORD, Constants.PASSWORD_1);

        try{
            long id =  db.insert(TABLE_USERS, null, contentValues);
            Log.d(TAG, "ID = " + id + " Username = " + Constants.USERNAME_1
                    + " Password = " + Constants.PASSWORD_1);
        }
        catch (Exception e){
            Log.e(TAG, "Could not insert data into the " + TABLE_USERS + " table. "
                    + e.toString());
        }
        finally {
            contentValues.clear();
        }

        contentValues.put("tUsername", Constants.USERNAME_2);
        contentValues.put("tPassword", Constants.PASSWORD_2);
        try{
            long id = db.insert(TABLE_USERS, null, contentValues);
            Log.d(TAG, "ID = " + id + " Username = " + Constants.USERNAME_2
                    + " Password = " + Constants.PASSWORD_2);
        }
        catch (Exception e){
            Log.e(TAG, "Could not insert data into the " + TABLE_USERS
                    + " table. " + e.toString());
            throw e;
        }
        finally {
            contentValues.clear();
        }

        Log.d(TAG, "<< [populateTables]");
    }
}
