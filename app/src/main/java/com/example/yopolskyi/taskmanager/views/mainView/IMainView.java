package com.example.yopolskyi.taskmanager.views.mainView;

import java.util.Date;

public interface IMainView {
    void showNewTaskFragmentDueDateEditTextError(boolean isEnabled);
    void showNewTaskFragmentDueTimeEditTextError(boolean isEnabled);

    void showNewTaskFragmentTaskContentEditTextErrorToast();
    void showNewTaskFragmentDueDateEditTextErrorToast();
    void showNewTaskFragmentDueTimeEditTextErrorToast();
    void showNewTaskFragmentTaskAddedToast();
    void showTaskCategoryFragmentTaskDeletedToast();

    void showInternalErrorDialog();

    void setDrawerVisibility(boolean isEnabled);
    void setFabVisibility(boolean isEnabled);
    void setHomeButtonVisibility(boolean isEnabled);
    void setActionBarTitle(String title);
    void changeToolbarMenuState(ToolbarMenuStates menuState);

    IMainPresenter getPresenter();

    void onBackPressed();
    void onBackPressedDefault();

    String getAccessToken();

    void setUpDaySummaryNotificationAlarmManager(boolean isAlarmEnabled);
    void createTaskNotificationAlarm(int taskId, Date alarmDay, Date alarmTime);
    void deleteTaskNotificationAlarm(int taskId);
}
