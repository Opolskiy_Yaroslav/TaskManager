package com.example.yopolskyi.taskmanager.common.enumerations;

public enum AuthorizationTokenTypes {
    Access,
    Refresh
}
