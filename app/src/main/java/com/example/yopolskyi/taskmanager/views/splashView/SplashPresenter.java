package com.example.yopolskyi.taskmanager.views.splashView;

import android.util.Log;

import com.example.yopolskyi.taskmanager.common.Constants;
import com.example.yopolskyi.taskmanager.common.ResultObject;
import com.example.yopolskyi.taskmanager.common.TokensCombination;
import com.example.yopolskyi.taskmanager.common.Utils;
import com.example.yopolskyi.taskmanager.common.dependencyInjection.main.MainComponentAssistant;
import com.example.yopolskyi.taskmanager.common.enumerations.ResultMessage;
import com.example.yopolskyi.taskmanager.model.IModel;

import java.util.Calendar;
import java.util.Date;

import javax.inject.Inject;

public class SplashPresenter implements ISplashPresenter {

    private ISplashView view;

    @Inject
    IModel model;

    // For logging.
    private final String TAG = "SplashPresenter";

    public SplashPresenter(ISplashView view){
        this.view = view;
        MainComponentAssistant.getComponent().injectSplashPresenter(this);
    }

    /**
     * Calls model method to check access token relevance. If token is actual, calls view method to
     * launch main activity. If token is not actual, is trying to refresh it. If refreshing was
     * successful, calls view method to launch main activity. In other case calls view method to
     * launch login activity.
     */
    @Override
    public void processTokensAndLaunchNextActivity(String accessToken, Date accessTokenExpirationDate,
                                                   String refreshToken, Date refreshTokenExpirationDate){
        Log.d(TAG, ">> [processTokensAndLaunchNextActivity]");

        // If default shared preference values
        if(accessToken.equals("Not defined") || refreshToken.equals("Not defined") ||
                accessTokenExpirationDate == null || refreshTokenExpirationDate == null){

            Log.d(TAG, "Tokens are not defined. Launch login activity.");
            view.launchLoginActivity();
            return;
        }
        else {
            Log.d(TAG, "Input arguments:"
                        + " Access token value = " + accessToken
                        + " Access token exp date = " + accessTokenExpirationDate
                        + " Refresh token value = " + refreshToken
                        + " Refresh token exp date = " + refreshTokenExpirationDate);
        }

        if(isTokenActual(accessTokenExpirationDate)){
            Log.d(TAG, "Access tokens is relevant. Launch main activity.");
            view.launchMainActivity();
        }
        // Check if access token can be refreshed.
        else if(isTokenActual(refreshTokenExpirationDate)){

            Log.d(TAG, "Access token can be refreshed. Launch model " +
                    "refreshTokens method.");


            Log.d(TAG, "Calling the model getUserIdByAccessToken method.");
            ResultObject getUserIdResult = model.getUserIdByAccessToken(accessToken);
            if(getUserIdResult.getResultMessage() != ResultMessage.SUCCESSFUL){
                view.showInternalErrorDialog();
                return;
            }

            int userId = (int) getUserIdResult.getResultValue();
            Log.d(TAG, "User ID = " + userId);


            Log.d(TAG, "Calling the model refreshTokens method.");
            ResultObject refreshTokensResult = model.refreshTokens(userId);
            if(refreshTokensResult.getResultMessage() != ResultMessage.SUCCESSFUL){
                view.showInternalErrorDialog();
                return;
            }

            TokensCombination tokens = (TokensCombination) refreshTokensResult.getResultValue();

            Log.d(TAG, "Access token value = " + tokens.getAccessToken().getTokenValue());
            Log.d(TAG, "Access token expiration date = " + tokens.getAccessToken()
                    .getTokenExpirationDate());
            Log.d(TAG, "Refresh token value = " + tokens.getRefreshToken().getTokenValue());
            Log.d(TAG, "Refresh token expiration date = " + tokens.getRefreshToken()
                    .getTokenExpirationDate());


            Log.d(TAG, "Calls view saveTokensCombination method to write tokens to shared preferences");
            view.saveTokensCombination(tokens);

            Log.d(TAG, "Launch main activity.");
            view.launchMainActivity();
        }
        // If access token can't be refreshed.
        else{

            Log.d(TAG, "Calling the model getUserIdByAccessToken method.");
            ResultObject getUserIdResult = model.getUserIdByAccessToken(accessToken);
            if(getUserIdResult.getResultMessage() != ResultMessage.SUCCESSFUL){
                view.showInternalErrorDialog();
                return;
            }

            int userId = (int) getUserIdResult.getResultValue();
            Log.d(TAG, "User ID = " + userId);


            Log.d(TAG, "Calls model deleteToken method.");
            ResultObject deleteTokens = model.deleteTokens(userId);
            if(deleteTokens.getResultMessage() != ResultMessage.SUCCESSFUL){
                view.showInternalErrorDialog();
                return;
            }

            Log.d(TAG, "Launch login activity.");
            view.launchLoginActivity();
        }

        Log.d(TAG, "<< [processTokensAndLaunchNextActivity]");
    }

    /**
     * Compares token expiration date and current date and returns result.
     * @param tokenExpirationDate, tokenExpirationDate
     * @return "true" if token expiration date is larger than current date, "false" in other case.
     */
    private Boolean isTokenActual(Date tokenExpirationDate){
        Log.d(TAG, ">> [isTokenActual]");
        Log.d(TAG, "Input argument: " + "Token expiration date = "
                + Utils.convertDateToString(tokenExpirationDate, Constants.TOKENS_DATE_FORMAT));

        Date currentDate = Calendar.getInstance().getTime();

        Log.d(TAG, "Current date = "
                + Utils.convertDateToString(currentDate, Constants.TOKENS_DATE_FORMAT));

        if (currentDate.before(tokenExpirationDate))
        {
            Log.d(TAG, "Token expiration date is later than current date. Return true.");
            Log.d(TAG, "<< [isTokenActual]");
            return true;
        }
        Log.d(TAG, "Token expiration date is less than current date. Return false.");
        Log.d(TAG, "<< [isTokenActual]");

        return false;
    }
}
