package com.example.yopolskyi.taskmanager.model;

import com.example.yopolskyi.taskmanager.common.ResultObject;
import com.example.yopolskyi.taskmanager.common.Task;
import com.example.yopolskyi.taskmanager.common.TokensCombination;
import com.example.yopolskyi.taskmanager.common.enumerations.TaskExecutionStatuses;
import com.example.yopolskyi.taskmanager.model.dal.IRepository;

public interface IModel {

    void setRepository(IRepository repository);

    ResultObject doLogin(String username, String password);
    ResultObject getUserIdByAccessToken(String accessToken);
    ResultObject getUsernameByUserId(int userId);

    TokensCombination generateTokens(int userId);
    ResultObject addTokens(TokensCombination tokensCombination);
    ResultObject refreshTokens(int userId);
    ResultObject deleteTokens(int userId);

    ResultObject addTask(Task task);
    ResultObject getTask(int taskId, String accessToken);
    ResultObject getTasks(String accessToken);
    ResultObject getTasks(String criterion, String accessToken);
    ResultObject getTasks(TaskExecutionStatuses executionStatus, String accessToken);
    ResultObject refreshTaskStatuses(String accessToken);
    ResultObject refreshRepeatableTaskDueDates(String accessToken);
    ResultObject refreshRepeatableTaskDueDate(int taskId, String accessToken);
    ResultObject extendRepeatableTaskDueDate(int taskId, String accessToken);
    ResultObject updateTask(int taskId, Task task, String accessToken);
    ResultObject removeTask(int taskId, String accessToken);
}
