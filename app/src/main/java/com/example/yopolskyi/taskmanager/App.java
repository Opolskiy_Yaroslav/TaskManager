package com.example.yopolskyi.taskmanager;

import android.app.Activity;
import android.app.Application;

import com.example.yopolskyi.taskmanager.common.PreferencesAssistant;
import com.example.yopolskyi.taskmanager.common.dependencyInjection.main.MainComponentAssistant;
import com.example.yopolskyi.taskmanager.model.dal.IRepository;

import javax.inject.Inject;

public class App extends Application {

    @Inject
    IRepository repository;

    @Inject
    PreferencesAssistant preferencesAssistant;

    private static Activity activeActivity;


    public static Activity getActiveActivity() {
        return activeActivity;
    }
    public static void setActiveActivity(Activity activeActivity) {
        App.activeActivity = activeActivity;
    }

    @Override
    public void onCreate() {
        super.onCreate();

        // Dependency injection task manager component instance creating.
        MainComponentAssistant.createComponent();

        MainComponentAssistant.getComponent().injectApp(this);

        repository.setContext(getApplicationContext());
        preferencesAssistant.setContext(getApplicationContext());
    }
}
