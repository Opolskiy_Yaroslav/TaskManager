package com.example.yopolskyi.taskmanager.views.mainView.fragments.TaskCategoryFragment;

import android.content.Context;
import android.content.DialogInterface;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.yopolskyi.taskmanager.R;
import com.example.yopolskyi.taskmanager.common.Constants;
import com.example.yopolskyi.taskmanager.common.Task;
import com.example.yopolskyi.taskmanager.common.Utils;
import com.example.yopolskyi.taskmanager.common.enumerations.TaskExecutionStatuses;
import com.example.yopolskyi.taskmanager.common.enumerations.TaskFilterDateIntervals;
import com.example.yopolskyi.taskmanager.common.enumerations.TaskRepeatFrequencies;
import com.example.yopolskyi.taskmanager.views.mainView.MainActivity;
import com.example.yopolskyi.taskmanager.views.mainView.ToolbarMenuStates;
import com.example.yopolskyi.taskmanager.views.mainView.fragments.AllTasksFragment.IAllTaskFragment;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;

public class TaskAdapter extends RecyclerView.Adapter<TaskAdapter.ViewHolder> {

    private Context context;
    private ITaskCategoryFragment parentFragment;

    private List<Task> fullTaskList;
    private List<Task> filteredTaskList;

    private int selectedItemPosition = -1;

    private TaskExecutionStatuses executionStatus;

    // For logging.
    private final String TAG = "TaskAdapter";


    // Getters and setters.
    public int getSelectedItemPosition() {
        return selectedItemPosition;
    }
    public void setSelectedItemPosition(int selectedItemPosition) {
        this.selectedItemPosition = selectedItemPosition;
    }


    class ViewHolder extends RecyclerView.ViewHolder {

        private TextView tvContent;
        private TextView tvDueDate;
        private TextView tvRepeatFrequency;
        private CheckBox checkBox;

        private LinearLayout holderLayout;

        private Task currentTask;

        private DialogInterface.OnClickListener repeatTaskDialogClickListener;


        ViewHolder(View view) {
            super(view);
            tvContent = (TextView) view.findViewById(
                    R.id.activity_main_card_view_content_text_view);
            tvDueDate = (TextView) view.findViewById(
                    R.id.activity_main_card_view_due_date_text_view);
            tvRepeatFrequency = (TextView) view.findViewById(
                    R.id.activity_main_card_view_repeat_text_view);
            checkBox = (CheckBox) view.findViewById(
                    R.id.activity_main_card_view_checkbox);

            holderLayout = (LinearLayout) view.findViewById(
                    R.id.activity_main_card_view_layout);

            final ViewHolder holder = this;

            // Set checkBox onChecked listener.
            checkBox.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    onCheckBoxClickHandler(holder, checkBox.isChecked());
                }
            });

            // Set holder onLongClick listener.
            holder.holderLayout.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View v) {
                    parentFragment.setToolbarItemsVisibility(ToolbarMenuStates.context);

                    selectedItemPosition = holder.getAdapterPosition();

                    // If there are selected item from other recycler view - unselect it.
                    if(parentFragment instanceof IAllTaskFragment){
                        ((IAllTaskFragment)parentFragment).makeOnlyOneTaskSelected(executionStatus);
                    }

                    notifyDataSetChanged();

                    return true;
                }
            });
        }

        private void onCheckBoxClickHandler(TaskAdapter.ViewHolder holder, boolean isChecked){
            Log.d(TAG, ">> [onCheckBoxClickHandler]");

            int position = holder.getAdapterPosition();
            Log.d(TAG, "Selected task position = " + position);

            // Get user access token.
            MainActivity parentActivity = (MainActivity) context;
            String accessToken = parentActivity.getAccessToken();

            // Get current task.
            currentTask = filteredTaskList.get(position);
            Log.d(TAG, "Selected task = " + currentTask);

            //Get current task ID.
            int taskId = currentTask.getId();
            Log.d(TAG, "Selected task ID = " + taskId);

            Log.d(TAG, "Checkbox state = " + isChecked);

            // If task status - "In progress" or "Overdue" - change to "Finished".
            // If task status - "Finished" - change to "In progress".
            if (isChecked) {

                // If repeat frequency set - ask user if he want do repeat this task.
                if(currentTask.getRepeatFrequency() != TaskRepeatFrequencies.noRepeat){
                    // If user want to repeat this task - update its due date.
                    showRepeatTaskDialog(position, accessToken);
                    return;
                }
                // If repeat frequency is not set - change execution status to finished.
                else {
                    currentTask.setTaskExecutionStatus(TaskExecutionStatuses.finished);
                }
            }
            else {
                currentTask.setTaskExecutionStatus(TaskExecutionStatuses.inProgress);
            }

            // Call presenter updateTask method to update task in the storage.
            parentActivity.getPresenter().updateTask(currentTask);

            // Check task dates and change execution statuses to "Overdue" if needed.
            parentActivity.getPresenter().refreshTaskStatuses(accessToken);

            // Create or delete notification for this task.
            parentActivity.getPresenter().setUpTaskNotificationAlarmManager(taskId);

            // For view animation.
            fullTaskList.remove(position);
            filteredTaskList.remove(position);
            notifyItemRemoved(position);
            notifyItemRangeChanged(position, filteredTaskList.size());

            parentFragment.updateTaskList();

            // After updating task list will be shown full task list. If we use searching - we need
            // to refresh query to show the right task list.
            if(parentActivity.getToolbarMenuState() == ToolbarMenuStates.search){
                parentFragment.refreshSearchViewQuery();
            }

            setTaskCategoriesVisibility();

            Log.d(TAG, "<< [onCheckBoxClickHandler]");
        }

        private void setCardStyle(TaskAdapter.ViewHolder holder, TaskExecutionStatuses executionStatus){

            if(holder.tvRepeatFrequency.getText() != TaskRepeatFrequencies.noRepeat.toString()){
                holder.tvRepeatFrequency.setVisibility(View.VISIBLE);
            }
            else{
                holder.tvRepeatFrequency.setVisibility(View.GONE);
            }

            switch(executionStatus){
                case inProgress:
                    holder.tvContent.setTextColor(ContextCompat.getColor(context,
                            R.color.activity_main_in_progress_card_content_tv_color));
                    holder.tvDueDate.setTextColor(ContextCompat.getColor(context,
                            R.color.activity_main_in_progress_card_due_date_tv_color));
                    holder.checkBox.setChecked(false);
                    break;
                case overdue:
                    holder.tvContent.setTextColor(ContextCompat.getColor(context,
                            R.color.activity_main_overdue_card_content_tv_color));
                    holder.tvDueDate.setTextColor(ContextCompat.getColor(context,
                            R.color.activity_main_overdue_card_due_date_tv_color));
                    holder.checkBox.setChecked(false);
                    break;
                case finished:
                    holder.tvContent.setTextColor(ContextCompat.getColor(context,
                            R.color.activity_main_finished_card_content_tv_color));
                    holder.tvDueDate.setTextColor(ContextCompat.getColor(context,
                            R.color.activity_main_finished_card_due_date_tv_color));
                    holder.tvRepeatFrequency.setVisibility(View.GONE);
                    holder.checkBox.setChecked(true);
            }
        }

        private void showRepeatTaskDialog(final int holderPosition, final String accessToken){
            // Dialog that asks if user wants to delete repeatable task.
            repeatTaskDialogClickListener = new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    switch (which){
                        case DialogInterface.BUTTON_POSITIVE:
                            MainActivity parentActivity = (MainActivity)context;
                            parentActivity.getPresenter().extendRepeatableTaskDueDate(
                                    currentTask.getId(), parentActivity.getAccessToken());

                            parentFragment.updateTaskList();
                            setTaskCategoriesVisibility();

                            checkBox.setChecked(false);
                            break;

                        case DialogInterface.BUTTON_NEGATIVE:
                            parentActivity = (MainActivity)context;
                            currentTask.setTaskExecutionStatus(TaskExecutionStatuses.finished);

                            parentActivity.deleteTaskNotificationAlarm(currentTask.getId());

                            // Call presenter updateTask method to update task in the storage.
                            parentActivity.getPresenter().updateTask(currentTask);

                            // Check task dates and change execution statuses to "Overdue" if needed.
                            parentActivity.getPresenter().refreshTaskStatuses(accessToken);

                            // For view animation.
                            fullTaskList.remove(holderPosition);
                            filteredTaskList.remove(holderPosition);
                            notifyItemRemoved(holderPosition);
                            notifyItemRangeChanged(holderPosition, filteredTaskList.size());

                            parentFragment.updateTaskList();
                            setTaskCategoriesVisibility();
                            break;
                    }
                }
            };

            AlertDialog.Builder builder = new AlertDialog.Builder(context);
            builder.setTitle(context.getString(R.string.activity_main_repeat_task_dialog_title))
                    .setPositiveButton(context.getString(
                            R.string.activity_main_repeat_task_dialog_positive_button_text), repeatTaskDialogClickListener)
                    .setNegativeButton(context.getString(
                            R.string.activity_main_repeat_task_dialog_negative_button_text), repeatTaskDialogClickListener)
                    .show();
        }
    }


    public TaskAdapter(Context context, List<Task> taskList, ITaskCategoryFragment parentFragment,
                       TaskExecutionStatuses executionStatus) {
        this.context = context;
        this.fullTaskList = taskList;
        this.parentFragment = parentFragment;
        this.executionStatus = executionStatus;

        filteredTaskList = new ArrayList<>(fullTaskList);
    }



    @Override
    public TaskAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new TaskAdapter.ViewHolder(LayoutInflater.from(parent.getContext())
                .inflate(R.layout.activity_main_task_card, parent, false));
    }

    @Override
    public void onBindViewHolder(final TaskAdapter.ViewHolder holder, int position) {
        Log.d(TAG, ">> [onBindViewHolder]");

        Task task = filteredTaskList.get(position);
        Log.d(TAG, "Selected task = " + task);

        holder.tvContent.setText(task.getContent());

        String dueDate = Utils.convertDateToString(task.getDueDate(), Constants.DATE_FORMAT_FOR_USER);
        String dueTime = Utils.convertDateToString(task.getDueTime(), Constants.EXPIRATION_TIME_FORMAT);

        // If due time set - show date and time. If due time is not set - show only date.
        // If due date is not set - hide TextView.
        if (dueDate == null) {
            holder.tvDueDate.setVisibility(View.GONE);
        }
        else {
            holder.tvDueDate.setVisibility(View.VISIBLE);
        }

        if (dueTime != null) {
            holder.tvDueDate.setText(dueDate + ", " + dueTime);
        } else {
            holder.tvDueDate.setText(dueDate);
        }

        holder.tvRepeatFrequency.setText(task.getRepeatFrequency().toString());

        Log.d(TAG, "Task execution status = " + executionStatus);


        // Set the content and due date color based on the execution status.
        holder.setCardStyle(holder, executionStatus);


        // Change background of selected card.
        if(selectedItemPosition == position){
            holder.holderLayout.setBackgroundColor(ContextCompat.getColor(context,
                    R.color.activity_main_selected_card_view_background_color));
        }
        else{
            holder.holderLayout.setBackgroundColor(ContextCompat.getColor(context,
                    R.color.activity_main_card_view_default_background_color));
        }

        Log.d(TAG, "<< [onBindViewHolder]");
    }

    @Override
    public int getItemCount() {
        return filteredTaskList.size();
    }

    /**
     * Filters task list by date interval (from toolbar spinner).
     */
    public List<Task> filterTasksByDateInterval(List<Task> taskList, TaskFilterDateIntervals dateInterval){
        Log.d(TAG, ">> [filterTasksByDateInterval]");
        Log.d(TAG, "Input argument: " + "Date interval = " + dateInterval);

        fullTaskList = new ArrayList<>(taskList);
        filteredTaskList = new ArrayList<>(fullTaskList);

        Calendar currentDate = Calendar.getInstance();
        currentDate.setFirstDayOfWeek(Constants.FIRST_DAY_OF_THE_WEEK);

        // Last date for filtering.
        Calendar deadline = Calendar.getInstance();
        deadline.setFirstDayOfWeek(Constants.FIRST_DAY_OF_THE_WEEK);

        Calendar taskDueDate = Calendar.getInstance();
        taskDueDate.setFirstDayOfWeek(Constants.FIRST_DAY_OF_THE_WEEK);

        int currentWeek, currentMonth, currentYear;

        switch (dateInterval){
            case today:
                for (Iterator<Task> taskListIterator = filteredTaskList.listIterator(); taskListIterator.hasNext(); ) {
                    Task currentTask = taskListIterator.next();
                    Date currentTaskDueDate = currentTask.getDueDate();
                    if(currentTaskDueDate == null){
                        taskListIterator.remove();
                        continue;
                    }
                    else{
                        // Set current task due date to calendar instance for further comparing.
                        taskDueDate.setTime(currentTaskDueDate);
                    }

                    if (taskDueDate.get(Calendar.DAY_OF_YEAR) != deadline.get(Calendar.DAY_OF_YEAR)){
                        taskListIterator.remove();
                    }
                }
                break;
            case thisWeek:
                currentWeek = currentDate.get(Calendar.WEEK_OF_YEAR);
                currentYear = currentDate.get(Calendar.YEAR);

                for (Iterator<Task> taskListIterator = filteredTaskList.listIterator(); taskListIterator.hasNext(); ) {
                    Task currentTask = taskListIterator.next();

                    Date currentTaskDueDate = currentTask.getDueDate();
                    if(currentTaskDueDate == null){
                        taskListIterator.remove();
                        continue;
                    }
                    else{
                        taskDueDate.setTime(currentTaskDueDate);
                    }

                    int taskWeek = taskDueDate.get(Calendar.WEEK_OF_YEAR);
                    int taskYear = taskDueDate.get(Calendar.YEAR);

                    if (taskWeek != currentWeek || taskYear != currentYear){
                        taskListIterator.remove();
                    }
                }
                break;
            case thisMonth:
                currentMonth = currentDate.get(Calendar.MONTH);
                currentYear = currentDate.get(Calendar.YEAR);

                for (Iterator<Task> taskListIterator = filteredTaskList.listIterator(); taskListIterator.hasNext(); ) {
                    Task currentTask = taskListIterator.next();

                    Date currentTaskDueDate = currentTask.getDueDate();
                    if(currentTaskDueDate == null){
                        taskListIterator.remove();
                        continue;
                    }
                    else{
                        taskDueDate.setTime(currentTaskDueDate);
                    }

                    int taskMonth = taskDueDate.get(Calendar.MONTH);
                    int taskYear = taskDueDate.get(Calendar.YEAR);

                    if (taskMonth != currentMonth || taskYear != currentYear){
                        taskListIterator.remove();
                    }
                }
                break;
            case thisYear:
                currentYear = currentDate.get(Calendar.YEAR);

                for (Iterator<Task> taskListIterator = filteredTaskList.listIterator(); taskListIterator.hasNext(); ) {
                    Task currentTask = taskListIterator.next();

                    Date currentTaskDueDate = currentTask.getDueDate();
                    if(currentTaskDueDate == null){
                        taskListIterator.remove();
                        continue;
                    }
                    else{
                        taskDueDate.setTime(currentTaskDueDate);
                    }

                    int taskYear = taskDueDate.get(Calendar.YEAR);

                    if (taskYear != currentYear){
                        taskListIterator.remove();
                    }
                }
                break;
            case noDate:
                for (Iterator<Task> taskListIterator = filteredTaskList.listIterator(); taskListIterator.hasNext(); ) {
                    Task currentTask = taskListIterator.next();

                    if (currentTask.getDueDate() != null) {
                        taskListIterator.remove();
                    }
                }
                break;
        }
        notifyDataSetChanged();

        // Hide category name if it's do not contain tasks.
        setTaskCategoriesVisibility();

        Log.d(TAG, "<< [filterTasksByDateInterval]");

        return filteredTaskList;
    }
     /**
     * Filters task list by content (task searching implementation).
     */
    public List<Task> filterTaskListByQuery(String query) {
        Log.d(TAG, ">> [filterTaskListByQuery]");
        Log.d(TAG, "Input argument: " + "Query = " + query);

        query = query.toLowerCase(Locale.getDefault());

        filteredTaskList.clear();

        if (query.length() == 0) {
            filteredTaskList.addAll(fullTaskList);
        } else {
            for (Task task : fullTaskList) {
                if (task.getContent().toLowerCase(Locale.getDefault()).contains(query)) {
                    filteredTaskList.add(task);
                }
            }
        }
        notifyDataSetChanged();

        setTaskCategoriesVisibility();

        Log.d(TAG, "<< [filterTaskListByQuery]");

        return filteredTaskList;
    }

    /**
     * Sets AllTaskFragment task categories names visibility based on task list items number.
     */
    private void setTaskCategoriesVisibility(){
        if(parentFragment instanceof IAllTaskFragment){
            if(!filteredTaskList.isEmpty()){
                switch (executionStatus){
                    case inProgress:
                        ((IAllTaskFragment)parentFragment).showInProgressTaskCategoryName(View.VISIBLE);
                        break;
                    case overdue:
                        ((IAllTaskFragment)parentFragment).showOverdueTaskCategoryName(View.VISIBLE);
                        break;
                    case finished:
                        ((IAllTaskFragment)parentFragment).showFinishedTaskCategoryName(View.VISIBLE);
                        break;
                }
            }
            else{
                switch (executionStatus){
                    case inProgress:
                        ((IAllTaskFragment)parentFragment).showInProgressTaskCategoryName(View.GONE);
                        break;
                    case overdue:
                        ((IAllTaskFragment)parentFragment).showOverdueTaskCategoryName(View.GONE);
                        break;
                    case finished:
                        ((IAllTaskFragment)parentFragment).showFinishedTaskCategoryName(View.GONE);
                        break;
                }
            }
        }
    }

    /**
     * Returns task ID by TaskAdapter selectedItemPosition field.
     * @return selected task ID
     */
    public int getSelectedTaskId(){
        Log.d(TAG, ">> [getSelectedTaskId]");

        Task selectedTask = null;

        if(selectedItemPosition != -1){
            selectedTask = filteredTaskList.get(selectedItemPosition);

        }

        if(selectedTask != null){
            Log.d(TAG, "Selected item position = " + selectedItemPosition);
            Log.d(TAG, "Selected task ID = " + selectedTask.getId());

            Log.d(TAG, "<< [getSelectedTaskId]");

            return selectedTask.getId();
        }
        else{
           return -1;
        }
    }
}
