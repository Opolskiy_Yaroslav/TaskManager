package com.example.yopolskyi.taskmanager.views.mainView.fragments.TaskCategoryFragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.MenuItemCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.example.yopolskyi.taskmanager.R;
import com.example.yopolskyi.taskmanager.common.Task;
import com.example.yopolskyi.taskmanager.common.Utils;
import com.example.yopolskyi.taskmanager.common.enumerations.TaskExecutionStatuses;
import com.example.yopolskyi.taskmanager.common.enumerations.TaskFilterDateIntervals;
import com.example.yopolskyi.taskmanager.views.mainView.IMainView;
import com.example.yopolskyi.taskmanager.views.mainView.MainActivity;
import com.example.yopolskyi.taskmanager.views.mainView.ToolbarMenuStates;

import java.util.List;

/**
 * Uses for displaying "In progress", "Overdue" and "Finished" task lists.
 */
public class TaskCategoryFragment extends Fragment implements ITaskCategoryFragment {

    private TaskExecutionStatuses taskExecutionStatus;

    private IMainView parentView;

    private TaskRecyclerView recyclerView;
    private TaskAdapter taskAdapter;

    private List<Task> taskList;

    protected Toolbar toolbar;
    protected DrawerLayout drawer;
    protected Spinner toolbarSpinner;

    protected MenuItem searchItem;
    protected SearchView searchView;

    protected TextView tvTaskListEmpty;

    // For logging.
    private final String TAG = "TaskCategoryFragment";

    public TaskCategoryFragment() {
        // Required empty public constructor
    }

    @Override
    public boolean isSearchViewExpanded() {
        return !searchView.isIconified();
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        parentView = (MainActivity) getActivity();

        Bundle args = getArguments();
        if (args != null) {
            taskExecutionStatus = TaskExecutionStatuses.values()[args.getInt("status", -1)];
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        setHasOptionsMenu(true);

        View rootView = inflater.inflate(R.layout.fragment_task_category, container, false);
        initItems(rootView);

        return rootView;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);

        menu.setGroupVisible(R.id.activity_main_toolbar_item_group_main, true);
        menu.setGroupVisible(R.id.activity_main_toolbar_item_group_context, false);
        menu.setGroupVisible(R.id.activity_main_toolbar_item_group_edit, false);

        toolbar = (Toolbar) getActivity().findViewById(R.id.toolbar);
        drawer = (DrawerLayout) getActivity().findViewById(R.id.drawer_layout);

        initToolbarMenuItems(menu);
    }

    @Override
    public void onResume() {
        super.onResume();

        parentView.changeToolbarMenuState(ToolbarMenuStates.common);

        if (taskExecutionStatus != null) {
            switch (taskExecutionStatus) {
                case inProgress:
                    parentView.setActionBarTitle(getResources().getString(
                            R.string.activity_main_nav_menu_in_progress_item));
                    break;
                case overdue:
                    parentView.setActionBarTitle(getResources().getString(
                            R.string.activity_main_nav_menu_overdue_item));
                    break;
                case finished:
                    parentView.setActionBarTitle(getResources().getString(
                            R.string.activity_main_nav_menu_finished_item));
                    break;
            }
        }

        // Change task execution statuses to "Overdue" if needed.
        parentView.getPresenter().refreshTaskStatuses(parentView.getAccessToken());

        updateTaskList();
    }

    /**
     * Gets task list from the storage, sorts and filters it.
     */
    @Override
    public void updateTaskList() {
        Log.d(TAG, ">> [updateTaskList]");

        // If toolbar menu is not created yet.
        if (toolbarSpinner == null) {
            return;
        }

        // Get and sort task list.
        Log.d(TAG, "Try to get task list.");
        taskList = parentView.getPresenter().getTasks(taskExecutionStatus,
                parentView.getAccessToken());

        Log.d(TAG, "Loaded task list:");
        for (Task task : taskList) {
            Log.d(TAG, task.toString());
        }

        Log.d(TAG, "Try to sort task list.");
        taskList = Utils.sortTaskListByDate(taskList);

        Log.d(TAG, "Sorted task list:");
        for (Task task : taskList) {
            Log.d(TAG, task.toString());
        }

        Log.d(TAG, "Try to filter task list.");
        filterTasksByDateInterval();

        Log.d(TAG, "<< [updateTaskList]");
    }

    protected void initItems(View view) {

        recyclerView = (TaskRecyclerView) view.findViewById(
                R.id.activity_main_task_category_fragment_recycler_view);

        tvTaskListEmpty = (TextView) view.findViewById(
                R.id.activity_main_task_category_fragment_recycler_view_empty_message_tv);

        recyclerView.setEmptyViewMessage(tvTaskListEmpty);

        taskList = parentView.getPresenter().getTasks(taskExecutionStatus, parentView.getAccessToken());
        taskList = Utils.sortTaskListByDate(taskList);

        taskAdapter = new TaskAdapter(getActivity(), taskList, this, taskExecutionStatus);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(taskAdapter);


    }

    protected void initToolbarMenuItems(final Menu menu) {

        // Init spinner.
        toolbarSpinner = (Spinner) MenuItemCompat.getActionView(
                menu.findItem(R.id.activity_main_toolbar_spinner));

        toolbarSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parentView, View selectedItemView,
                                       int position, long id) {
                updateTaskList();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parentView) {

            }
        });


        // Init search view.
        searchItem = menu.findItem(R.id.activity_main_toolbar_search_view);
        if (searchItem != null) {
            searchView = (SearchView) searchItem.getActionView();
        }

        MenuItemCompat.setOnActionExpandListener(searchItem, new MenuItemCompat.OnActionExpandListener() {
            @Override
            public boolean onMenuItemActionExpand(MenuItem item) {
                parentView.changeToolbarMenuState(ToolbarMenuStates.search);
                parentView.setFabVisibility(false);

                return true;
            }

            @Override
            public boolean onMenuItemActionCollapse(MenuItem item) {
                parentView.changeToolbarMenuState(ToolbarMenuStates.common);
                parentView.setFabVisibility(true);

                return true;
            }
        });

        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {

            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                filterTaskListByQuery(newText);
                return false;
            }
        });
    }

    /**
     * Calls adapter method to filter task list by date interval.
     */
    protected void filterTasksByDateInterval() {
        Log.d(TAG, ">> [filterTasksByDateInterval]");

        TaskFilterDateIntervals dateInterval = TaskFilterDateIntervals
                .values()[toolbarSpinner.getSelectedItemPosition()];

        taskAdapter.filterTasksByDateInterval(taskList, dateInterval);

        Log.d(TAG, "<< [filterTasksByDateInterval]");
    }

    /**
     * Calls adapter method to filter task list by content.
     */
    @Override
    public void filterTaskListByQuery(String query) {
        Log.d(TAG, ">> [filterTaskListByQuery]");

        taskAdapter.filterTaskListByQuery(query);

        Log.d(TAG, "<< [filterTaskListByQuery]");
    }

    @Override
    public void setToolbarItemsVisibility(ToolbarMenuStates menuState) {
        parentView.changeToolbarMenuState(menuState);
    }

    @Override
    public void clearRecycleViewFormatting() {
        int selectedItemPosition = taskAdapter.getSelectedItemPosition();
        if (selectedItemPosition != -1) {
            recyclerView.getChildAt(selectedItemPosition)
                    .findViewById(R.id.activity_main_card_view_layout)
                    .setBackgroundColor(ContextCompat.getColor(getContext(),
                            R.color.activity_main_card_view_default_background_color));
            taskAdapter.setSelectedItemPosition(-1);
        }
    }

    @Override
    public int getSelectedTaskId() {
        return taskAdapter.getSelectedTaskId();
    }

    @Override
    public void removeSelectedTask() {

        int selectedTaskId = getSelectedTaskId();
        String accessToken = parentView.getAccessToken();

        parentView.getPresenter().removeTask(selectedTaskId, accessToken);
        taskAdapter.notifyDataSetChanged();

        // Remove notification for deleted task.
        parentView.deleteTaskNotificationAlarm(selectedTaskId);

        Toast.makeText((MainActivity) parentView, getString(R.string.activity_main_task_deleted_toast_message),
                Toast.LENGTH_LONG).show();
    }

    @Override
    public void showTaskDeletedToast() {
        Toast.makeText(getActivity(), getString(R.string.activity_main_task_deleted_toast_message),
                Toast.LENGTH_LONG).show();
    }

    /**
     * Recalls presenter searching method to programmatically update search task list.
     */
    @Override
    public void refreshSearchViewQuery() {
        filterTaskListByQuery(searchView.getQuery().toString());
    }
}
