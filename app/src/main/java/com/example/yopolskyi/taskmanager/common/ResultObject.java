package com.example.yopolskyi.taskmanager.common;

import com.example.yopolskyi.taskmanager.common.enumerations.ResultMessage;

public class ResultObject {
    private ResultMessage resultMessage;
    private Object resultValue;

    public ResultObject(ResultMessage resultMessage, Object resultValue){
        this.resultMessage = resultMessage;
        this.resultValue = resultValue;
    }

    public ResultMessage getResultMessage() {
        return resultMessage;
    }
    public Object getResultValue() {
        return resultValue;
    }
}
