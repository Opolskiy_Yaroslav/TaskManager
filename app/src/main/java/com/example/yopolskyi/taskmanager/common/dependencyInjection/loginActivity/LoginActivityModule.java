package com.example.yopolskyi.taskmanager.common.dependencyInjection.loginActivity;

import com.example.yopolskyi.taskmanager.views.loginView.ILoginPresenter;
import com.example.yopolskyi.taskmanager.views.loginView.LoginActivity;
import com.example.yopolskyi.taskmanager.views.loginView.LoginPresenter;

import dagger.Module;
import dagger.Provides;

@Module
public class LoginActivityModule {

    private final LoginActivity loginActivity;

    public LoginActivityModule(LoginActivity loginActivity) {
        this.loginActivity = loginActivity;
    }

    @Provides
    @LoginActivityScope
    ILoginPresenter provideLoginPresenter(){
        return new LoginPresenter(loginActivity);
    }
}
