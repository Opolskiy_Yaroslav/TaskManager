package com.example.yopolskyi.taskmanager.views.mainView;

import com.example.yopolskyi.taskmanager.common.Task;
import com.example.yopolskyi.taskmanager.common.enumerations.TaskExecutionStatuses;

import java.util.List;

public interface IMainPresenter {
    Boolean isDateValid(String date);
    Boolean isTimeValid(String date, String time);

    void addTask(String content, String date, String time, int repeatFrequencyId);
    Task getTask(int taskId, String accessToken);
    List<Task> getTasks(String accessToken);
    List<Task> getTasks(String criterion, String accessToken);
    List<Task> getTasks(TaskExecutionStatuses executionStatus, String accessToken);
    void refreshTaskStatuses(String accessToken);
    void refreshRepeatableTaskDueDate(int taskId, String accessToken);
    void refreshRepeatableTaskDueDates(String accessToken);
    void extendRepeatableTaskDueDate(int taskId, String accessToken);
    void updateTask(int taskId, String content, String date, String time, int repeatFrequencyId,
                    TaskExecutionStatuses taskExecutionStatus);
    void updateTask(Task newTask);
    void removeTask(int taskId, String accessToken);

    void deleteTokens(String accessToken);

    void refreshesAllUserNotificationAlarms();
    void disableAllUserNotificationAlarms();
    void setUpTaskNotificationAlarmManager(int taskId);
}
