package com.example.yopolskyi.taskmanager.model.dal;

import android.content.Context;

import com.example.yopolskyi.taskmanager.common.Task;
import com.example.yopolskyi.taskmanager.common.TokensCombination;
import com.example.yopolskyi.taskmanager.common.enumerations.TaskExecutionStatuses;

import java.util.List;

public interface IRepository {

    void setContext(Context context);

    long addTask(Task newTask);
    void updateTask(int taskId, Task task, int userId);
    void removeTask(int taskId, int userId);
    Task getTaskByTaskId(int taskId, int userId);
    List<Task> getTasks(int userId);
    List<Task> getTasksByContent(String criterion, int userId);
    List<Task> getTasksByExecutionStatus(TaskExecutionStatuses executionStatus, int userId);

    int getUser(String username, String password);

    void addTokens(TokensCombination tokensCombination);
    void updateTokens(int userId, TokensCombination tokensCombination);
    void removeTokens(int userId);
    int getUserIdByAccessToken(String accessToken);
    String getUsernameByUserId(int userId);
    int getUserIdByUsername(String username);
}
