package com.example.yopolskyi.taskmanager.views.splashView;

import com.example.yopolskyi.taskmanager.common.TokensCombination;

public interface ISplashView {
    void launchLoginActivity();
    void launchMainActivity();
    void saveTokensCombination(TokensCombination tokensCombination);

    void showInternalErrorDialog();
}
