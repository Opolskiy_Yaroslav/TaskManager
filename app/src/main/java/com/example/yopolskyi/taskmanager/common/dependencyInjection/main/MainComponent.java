package com.example.yopolskyi.taskmanager.common.dependencyInjection.main;

import com.example.yopolskyi.taskmanager.App;
import com.example.yopolskyi.taskmanager.common.PreferencesAssistant;
import com.example.yopolskyi.taskmanager.model.IModel;
import com.example.yopolskyi.taskmanager.model.Model;
import com.example.yopolskyi.taskmanager.model.dal.IRepository;
import com.example.yopolskyi.taskmanager.model.dal.Repository;
import com.example.yopolskyi.taskmanager.views.loginView.LoginPresenter;
import com.example.yopolskyi.taskmanager.views.mainView.MainPresenter;
import com.example.yopolskyi.taskmanager.NotificationReceiver;
import com.example.yopolskyi.taskmanager.TaskStatusesRefreshReceiver;
import com.example.yopolskyi.taskmanager.views.splashView.SplashPresenter;

import dagger.Component;

@Component(modules = {ModelModule.class, StorageModule.class})
@MainScope
public interface MainComponent {
    void injectApp(App app);

    void injectSplashPresenter(SplashPresenter splashPresenter);
    void injectLoginPresenter(LoginPresenter loginPresenter);
    void injectMainPresenter(MainPresenter mainPresenter);

    void injectNotificationReceiver(NotificationReceiver notificationReceiver);
    void injectTaskStatusesRefreshReceiver(TaskStatusesRefreshReceiver taskStatusesRefreshReceiver);

    void injectModel(Model model);
    void injectRepository(Repository repository);

    IModel getModel();
    PreferencesAssistant getPreferenceAssistant();
    IRepository getRepository();
}
