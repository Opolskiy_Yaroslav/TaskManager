package com.example.yopolskyi.taskmanager.common.dependencyInjection.loginActivity;


import com.example.yopolskyi.taskmanager.common.dependencyInjection.main.MainComponent;
import com.example.yopolskyi.taskmanager.views.loginView.LoginActivity;

import dagger.Component;

@LoginActivityScope
@Component(modules = LoginActivityModule.class, dependencies = MainComponent.class)
public interface LoginActivityComponent {
    void injectLoginActivity(LoginActivity loginActivity);
}
