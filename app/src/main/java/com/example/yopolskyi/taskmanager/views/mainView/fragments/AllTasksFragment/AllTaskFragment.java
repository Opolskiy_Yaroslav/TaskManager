package com.example.yopolskyi.taskmanager.views.mainView.fragments.AllTasksFragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.yopolskyi.taskmanager.R;
import com.example.yopolskyi.taskmanager.common.Task;
import com.example.yopolskyi.taskmanager.views.mainView.fragments.TaskCategoryFragment.TaskRecyclerView;
import com.example.yopolskyi.taskmanager.common.Utils;
import com.example.yopolskyi.taskmanager.common.enumerations.TaskExecutionStatuses;
import com.example.yopolskyi.taskmanager.common.enumerations.TaskFilterDateIntervals;
import com.example.yopolskyi.taskmanager.views.mainView.IMainView;
import com.example.yopolskyi.taskmanager.views.mainView.MainActivity;
import com.example.yopolskyi.taskmanager.views.mainView.fragments.TaskCategoryFragment.TaskAdapter;
import com.example.yopolskyi.taskmanager.views.mainView.fragments.TaskCategoryFragment.ITaskCategoryFragment;
import com.example.yopolskyi.taskmanager.views.mainView.fragments.TaskCategoryFragment.TaskCategoryFragment;

import java.util.List;

public class AllTaskFragment extends TaskCategoryFragment implements ITaskCategoryFragment, IAllTaskFragment {

    private IMainView parentView;

    private TextView tvOverdue;
    private TextView tvInProgress;
    private TextView tvFinished;

    private TaskRecyclerView inProgressRecyclerView;
    private TaskRecyclerView overdueTaskRecyclerView;
    private TaskRecyclerView finishedTaskRecyclerView;

    private TaskAdapter inProgressTaskAdapter;
    private TaskAdapter overdueTaskAdapter;
    private TaskAdapter finishedTaskAdapter;

    private List<Task> inProgressTaskList;
    private List<Task> overdueTaskList;
    private List<Task> finishedTaskList;

    // For logging.
    private final String TAG = "AllTaskFragment";

    public AllTaskFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);

        parentView = (MainActivity)getActivity();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_all_tasks, container, false);
        initItems(rootView);

        return rootView;
    }

    @Override
    public void onResume() {
        super.onResume();

        parentView.setActionBarTitle(getResources().getString(R.string.activity_main_nav_menu_all_tasks_item));

        // Refresh due dates for overdue repeatable tasks.
        parentView.getPresenter().refreshRepeatableTaskDueDates(parentView.getAccessToken());
        // Change task execution statuses to "Overdue" if needed.
        parentView.getPresenter().refreshTaskStatuses(parentView.getAccessToken());
    }

    /**
     * Calls presenter getTasks method and updates taskList by it's result.
     */
    @Override
    public void updateTaskList(){
        Log.d(TAG, ">> [updateTaskList]");

        // If toolbar menu is not created yet.
        if(toolbarSpinner == null){
            return;
        }

        Log.d(TAG, "Try to get task lists.");
        overdueTaskList = parentView.getPresenter().getTasks(TaskExecutionStatuses.overdue,
                parentView.getAccessToken());
        inProgressTaskList = parentView.getPresenter().getTasks(TaskExecutionStatuses.inProgress,
                parentView.getAccessToken());
        finishedTaskList = parentView.getPresenter().getTasks(TaskExecutionStatuses.finished,
                parentView.getAccessToken());
        Log.d(TAG, "Completed.");


        // Sort task lists by date.
        Log.d(TAG, "Try to sort task lists by date.");
        overdueTaskList = Utils.sortTaskListByDate(overdueTaskList);
        inProgressTaskList = Utils.sortTaskListByDate(inProgressTaskList);
        finishedTaskList = Utils.sortTaskListByDate(finishedTaskList);
        Log.d(TAG, "Completed.");


        Log.d(TAG, "Try to filter task lists by date interval.");
        TaskFilterDateIntervals dateInterval = TaskFilterDateIntervals
                .values()[toolbarSpinner.getSelectedItemPosition()];

        overdueTaskList = overdueTaskAdapter.filterTasksByDateInterval(overdueTaskList, dateInterval);
        inProgressTaskList = inProgressTaskAdapter.filterTasksByDateInterval(inProgressTaskList, dateInterval);
        finishedTaskList = finishedTaskAdapter.filterTasksByDateInterval(finishedTaskList, dateInterval);
        Log.d(TAG, "Completed.");

        showMessageIfTaskListsEmpty();

        Log.d(TAG, "<< [updateTaskList]");
    }

    @Override
    protected void initItems(View view){

        // Finding category names ("Overdue", "In progress", "Finished").
        tvOverdue = (TextView) view.findViewById(R.id.activity_main_all_task_fragment_overdue_tv);
        tvInProgress = (TextView) view.findViewById(R.id.activity_main_all_task_fragment_in_progress_tv);
        tvFinished = (TextView) view.findViewById(R.id.activity_main_all_task_fragment_finished_tv);

        // Finding recycler views for each category.
        overdueTaskRecyclerView = (TaskRecyclerView) view.findViewById(
                R.id.activity_main_all_tasks_fragment_overdue_recycler_view);
        inProgressRecyclerView = (TaskRecyclerView) view.findViewById(
                R.id.activity_main_all_tasks_fragment_in_progress_recycler_view);
        finishedTaskRecyclerView = (TaskRecyclerView) view.findViewById(
                R.id.activity_main_all_tasks_fragment_finished_recycler_view);

        // Finding the empty list TextView.
        tvTaskListEmpty = (TextView) view.findViewById(
                R.id.activity_main_all_tasks_fragment_recycler_view_empty_message_tv);

        // Get the task list for each category.
        overdueTaskList = parentView.getPresenter().getTasks(TaskExecutionStatuses.overdue,
                parentView.getAccessToken());
        inProgressTaskList = parentView.getPresenter().getTasks(TaskExecutionStatuses.inProgress,
                parentView.getAccessToken());
        finishedTaskList = parentView.getPresenter().getTasks(TaskExecutionStatuses.finished,
                parentView.getAccessToken());

        // Sort task lists by date.
        overdueTaskList = Utils.sortTaskListByDate(overdueTaskList);
        inProgressTaskList = Utils.sortTaskListByDate(inProgressTaskList);
        finishedTaskList = Utils.sortTaskListByDate(finishedTaskList);

        // Hide recyclerView and show a message if all task lists is empty.
        if (overdueTaskList.isEmpty() && inProgressTaskList.isEmpty() && finishedTaskList.isEmpty()) {
            overdueTaskRecyclerView.setVisibility(View.GONE);
            inProgressRecyclerView.setVisibility(View.GONE);
            finishedTaskRecyclerView.setVisibility(View.GONE);
            tvTaskListEmpty.setVisibility(View.VISIBLE);
        }
        else {
            overdueTaskRecyclerView.setVisibility(View.VISIBLE);
            inProgressRecyclerView.setVisibility(View.VISIBLE);
            finishedTaskRecyclerView.setVisibility(View.VISIBLE);
            tvTaskListEmpty.setVisibility(View.GONE);
        }

        if(!overdueTaskList.isEmpty()){
            tvOverdue.setVisibility(View.VISIBLE);
        }
        else {
            tvOverdue.setVisibility(View.GONE);
        }
        if(!inProgressTaskList.isEmpty()){
            tvInProgress.setVisibility(View.VISIBLE);
        }
        else {
            tvInProgress.setVisibility(View.GONE);
        }
        if(!finishedTaskList.isEmpty()){
            tvFinished.setVisibility(View.VISIBLE);
        }
        else {
            tvFinished.setVisibility(View.GONE);
        }

        overdueTaskAdapter = new TaskAdapter((MainActivity) parentView,
                overdueTaskList, this, TaskExecutionStatuses.overdue);
        inProgressTaskAdapter = new TaskAdapter((MainActivity) parentView,
                inProgressTaskList, this, TaskExecutionStatuses.inProgress);
        finishedTaskAdapter = new TaskAdapter((MainActivity) parentView,
                finishedTaskList, this, TaskExecutionStatuses.finished);

        RecyclerView.LayoutManager overdueTaskLayoutManager = new LinearLayoutManager(
                (MainActivity) parentView){
            @Override
            public boolean canScrollVertically() {
                return false;
            }
        };
        overdueTaskRecyclerView.setLayoutManager(overdueTaskLayoutManager);
        overdueTaskRecyclerView.setItemAnimator(new DefaultItemAnimator());
        overdueTaskRecyclerView.setAdapter(overdueTaskAdapter);

        RecyclerView.LayoutManager inProgressLayoutManager = new LinearLayoutManager(
                (MainActivity) parentView){
            @Override
            public boolean canScrollVertically() {
                return false;
            }
        };
        inProgressRecyclerView.setLayoutManager(inProgressLayoutManager);
        inProgressRecyclerView.setItemAnimator(new DefaultItemAnimator());
        inProgressRecyclerView.setAdapter(inProgressTaskAdapter);

        RecyclerView.LayoutManager finishedTaskLayoutManager = new LinearLayoutManager(
                (MainActivity) parentView){
            @Override
            public boolean canScrollVertically() {
                return false;
            }
        };
        finishedTaskRecyclerView.setLayoutManager(finishedTaskLayoutManager);
        finishedTaskRecyclerView.setItemAnimator(new DefaultItemAnimator());
        finishedTaskRecyclerView.setAdapter(finishedTaskAdapter);
    }

    /**
     * Filters task list by query pattern (task searching implementation).
     */
    @Override
    public void filterTaskListByQuery(String query) {
        Log.d(TAG, ">> [filterTaskListByQuery]");

        inProgressTaskList = inProgressTaskAdapter.filterTaskListByQuery(query);
        overdueTaskList = overdueTaskAdapter.filterTaskListByQuery(query);
        finishedTaskList = finishedTaskAdapter.filterTaskListByQuery(query);

        showMessageIfTaskListsEmpty();

        Log.d(TAG, "<< [filterTaskListByQuery]");
    }

    @Override
    public void showInProgressTaskCategoryName(int visibilityState) {
        tvInProgress.setVisibility(visibilityState);
    }

    @Override
    public void showOverdueTaskCategoryName(int visibilityState) {
        tvOverdue.setVisibility(visibilityState);
    }

    @Override
    public void showFinishedTaskCategoryName(int visibilityState) {
        tvFinished.setVisibility(visibilityState);
    }

    /**
     * Hide recyclerView and show a message if all task lists is empty.
     */
    private void showMessageIfTaskListsEmpty(){
        if (overdueTaskList.isEmpty() && inProgressTaskList.isEmpty() && finishedTaskList.isEmpty()) {
            overdueTaskRecyclerView.setVisibility(View.GONE);
            inProgressRecyclerView.setVisibility(View.GONE);
            finishedTaskRecyclerView.setVisibility(View.GONE);
            tvTaskListEmpty.setVisibility(View.VISIBLE);
        }
        else {
            overdueTaskRecyclerView.setVisibility(View.VISIBLE);
            inProgressRecyclerView.setVisibility(View.VISIBLE);
            finishedTaskRecyclerView.setVisibility(View.VISIBLE);
            tvTaskListEmpty.setVisibility(View.GONE);
        }
    }

    /**
     * Synchronizes all recycler views selected items. Only one item can be selected.
     */
    @Override
    public void makeOnlyOneTaskSelected(TaskExecutionStatuses executionStatus) {

        int inProgressRecyclerViewSelectedItem, overdueRecyclerViewSelectedItem,
                finishedRecyclerViewSelectedItem;

        switch (executionStatus){
            case inProgress:
                overdueRecyclerViewSelectedItem = overdueTaskAdapter.getSelectedItemPosition();

                if(overdueRecyclerViewSelectedItem != -1){
                    overdueTaskRecyclerView.getChildAt(overdueRecyclerViewSelectedItem)
                            .findViewById(R.id.activity_main_card_view_layout)
                            .setBackgroundColor(ContextCompat.getColor(getContext(),
                                    R.color.activity_main_card_view_default_background_color));
                    overdueTaskAdapter.setSelectedItemPosition(-1);
                    overdueTaskRecyclerView.invalidate();
                }


                finishedRecyclerViewSelectedItem = finishedTaskAdapter.getSelectedItemPosition();

                if(finishedRecyclerViewSelectedItem != -1){
                    finishedTaskRecyclerView.getChildAt(finishedRecyclerViewSelectedItem)
                            .findViewById(R.id.activity_main_card_view_layout)
                            .setBackgroundColor(ContextCompat.getColor(getContext(),
                                    R.color.activity_main_card_view_default_background_color));
                    finishedTaskAdapter.setSelectedItemPosition(-1);
                    finishedTaskRecyclerView.invalidate();
                }

                break;

            case overdue:
                inProgressRecyclerViewSelectedItem = inProgressTaskAdapter.getSelectedItemPosition();

                if(inProgressRecyclerViewSelectedItem != -1){
                    inProgressRecyclerView.getChildAt(inProgressRecyclerViewSelectedItem)
                            .findViewById(R.id.activity_main_card_view_layout)
                            .setBackgroundColor(ContextCompat.getColor(getContext(),
                                    R.color.activity_main_card_view_default_background_color));
                    inProgressTaskAdapter.setSelectedItemPosition(-1);
                    inProgressRecyclerView.invalidate();
                }


                finishedRecyclerViewSelectedItem = finishedTaskAdapter.getSelectedItemPosition();

                if(finishedRecyclerViewSelectedItem != -1){
                    finishedTaskRecyclerView.getChildAt(finishedRecyclerViewSelectedItem)
                            .findViewById(R.id.activity_main_card_view_layout)
                            .setBackgroundColor(ContextCompat.getColor(getContext(),
                                    R.color.activity_main_card_view_default_background_color));
                    finishedTaskAdapter.setSelectedItemPosition(-1);
                    finishedTaskRecyclerView.invalidate();
                }
                break;
            case finished:
                inProgressRecyclerViewSelectedItem = inProgressTaskAdapter.getSelectedItemPosition();

                if(inProgressRecyclerViewSelectedItem != -1){
                    inProgressRecyclerView.getChildAt(inProgressRecyclerViewSelectedItem)
                            .findViewById(R.id.activity_main_card_view_layout)
                            .setBackgroundColor(ContextCompat.getColor(getContext(),
                                    R.color.activity_main_card_view_default_background_color));
                    inProgressTaskAdapter.setSelectedItemPosition(-1);
                    inProgressRecyclerView.invalidate();
                }


                overdueRecyclerViewSelectedItem = overdueTaskAdapter.getSelectedItemPosition();

                if(overdueRecyclerViewSelectedItem != -1){
                    overdueTaskRecyclerView.getChildAt(overdueRecyclerViewSelectedItem)
                            .findViewById(R.id.activity_main_card_view_layout)
                            .setBackgroundColor(ContextCompat.getColor(getContext(),
                                    R.color.activity_main_card_view_default_background_color));
                    overdueTaskAdapter.setSelectedItemPosition(-1);
                    overdueTaskRecyclerView.invalidate();
                }

                break;
        }
    }

    @Override
    public void clearRecycleViewFormatting() {
        int inProgressRecycleViewSelectedItemPosition = inProgressTaskAdapter.getSelectedItemPosition();
        if(inProgressRecycleViewSelectedItemPosition != -1){
            inProgressRecyclerView.getChildAt(inProgressRecycleViewSelectedItemPosition)
                    .findViewById(R.id.activity_main_card_view_layout)
                    .setBackgroundColor(ContextCompat.getColor(getContext(),
                            R.color.activity_main_card_view_default_background_color));
            inProgressTaskAdapter.setSelectedItemPosition(-1);
        }

        int overdueRecycleViewSelectedItemPosition = overdueTaskAdapter.getSelectedItemPosition();
        if(overdueRecycleViewSelectedItemPosition != -1){
            overdueTaskRecyclerView.getChildAt(overdueRecycleViewSelectedItemPosition)
                    .findViewById(R.id.activity_main_card_view_layout)
                    .setBackgroundColor(ContextCompat.getColor(getContext(),
                            R.color.activity_main_card_view_default_background_color));
            overdueTaskAdapter.setSelectedItemPosition(-1);
        }

        int finishedRecycleViewSelectedItemPosition = finishedTaskAdapter.getSelectedItemPosition();
        if(finishedRecycleViewSelectedItemPosition != -1){
            finishedTaskRecyclerView.getChildAt(finishedRecycleViewSelectedItemPosition)
                    .findViewById(R.id.activity_main_card_view_layout)
                    .setBackgroundColor(ContextCompat.getColor(getContext(),
                            R.color.activity_main_card_view_default_background_color));
            finishedTaskAdapter.setSelectedItemPosition(-1);
        }
    }

    @Override
    public int getSelectedTaskId() {
        int inProgressRecyclerViewSelectedItem = inProgressTaskAdapter.getSelectedTaskId();
        int overdueRecyclerViewSelectedItem = overdueTaskAdapter.getSelectedTaskId();
        int finishedRecyclerViewSelectedItem = finishedTaskAdapter.getSelectedTaskId();

        if(inProgressRecyclerViewSelectedItem != -1){
            return inProgressRecyclerViewSelectedItem;
        }
        else
            if(overdueRecyclerViewSelectedItem != -1){
                return overdueRecyclerViewSelectedItem;
            }
            else
                if(finishedRecyclerViewSelectedItem != -1){
                    return finishedRecyclerViewSelectedItem;
                }

        return -1;
    }

    @Override
    public void removeSelectedTask() {

        int selectedTaskId = -1;
        String accessToken;

        int inProgressRecyclerViewSelectedItem = inProgressTaskAdapter.getSelectedItemPosition();
        int overdueRecyclerViewSelectedItem = overdueTaskAdapter.getSelectedItemPosition();
        int finishedRecyclerViewSelectedItem = finishedTaskAdapter.getSelectedItemPosition();

        if(inProgressRecyclerViewSelectedItem != -1){

            selectedTaskId = inProgressTaskAdapter.getSelectedTaskId();
            accessToken = parentView.getAccessToken();

            parentView.getPresenter().removeTask(selectedTaskId, accessToken);
            inProgressTaskAdapter.notifyDataSetChanged();
        }
        else
            if(overdueRecyclerViewSelectedItem != -1){

            selectedTaskId = overdueTaskAdapter.getSelectedTaskId();
            accessToken = parentView.getAccessToken();

            parentView.getPresenter().removeTask(selectedTaskId, accessToken);
            overdueTaskAdapter.notifyDataSetChanged();
        }
        else
            if(finishedRecyclerViewSelectedItem != -1){

            selectedTaskId = finishedTaskAdapter.getSelectedTaskId();
            accessToken = parentView.getAccessToken();

            parentView.getPresenter().removeTask(selectedTaskId, accessToken);
            finishedTaskAdapter.notifyDataSetChanged();
        }

        // Remove notification for deleted task.
        parentView.deleteTaskNotificationAlarm(selectedTaskId);
    }
}
