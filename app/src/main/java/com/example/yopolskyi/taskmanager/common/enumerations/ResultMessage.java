package com.example.yopolskyi.taskmanager.common.enumerations;

public enum ResultMessage {
    SUCCESSFUL,

    DATABASE_ERROR
}
