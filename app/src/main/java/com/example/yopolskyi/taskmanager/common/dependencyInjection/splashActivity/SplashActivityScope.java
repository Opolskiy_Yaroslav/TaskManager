package com.example.yopolskyi.taskmanager.common.dependencyInjection.splashActivity;

import javax.inject.Scope;

@Scope
public @interface SplashActivityScope {
}
