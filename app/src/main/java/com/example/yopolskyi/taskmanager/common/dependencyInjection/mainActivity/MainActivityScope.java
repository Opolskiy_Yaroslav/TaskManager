package com.example.yopolskyi.taskmanager.common.dependencyInjection.mainActivity;

import javax.inject.Scope;

@Scope
public @interface MainActivityScope {
}
