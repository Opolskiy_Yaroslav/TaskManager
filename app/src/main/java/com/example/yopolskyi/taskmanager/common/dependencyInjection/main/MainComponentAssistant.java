package com.example.yopolskyi.taskmanager.common.dependencyInjection.main;

public class MainComponentAssistant {
    private static MainComponent component;

    public static void createComponent(){
        component = DaggerMainComponent.builder().build();
    }

    public static MainComponent getComponent() {
        return component;
    }
}
