package com.example.yopolskyi.taskmanager.views.loginView;

public interface ILoginPresenter {
    void doLogin(String username, String password);
}
