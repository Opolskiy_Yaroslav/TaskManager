package com.example.yopolskyi.taskmanager.views.mainView.fragments.NewTaskFragment;

public interface INewTaskFragment {
    void showDueDateEditTextError(Boolean isEnabled);
    void showDueTimeEditTextError(Boolean isEnabled);

    void showTaskContentEditTextErrorToast();
    void showDueDateEditTextErrorToast();
    void showDueTimeEditTextErrorToast();
    void showTaskAddedToast();

    boolean wereFieldsChanged();

    void onConfirmButtonClick();
}
