package com.example.yopolskyi.taskmanager.views.mainView.fragments.TaskCategoryFragment;

import com.example.yopolskyi.taskmanager.views.mainView.ToolbarMenuStates;

public interface ITaskCategoryFragment {
    boolean isSearchViewExpanded();

    void updateTaskList();
    int getSelectedTaskId();
    void removeSelectedTask();
    void filterTaskListByQuery(String query);
    void setToolbarItemsVisibility(ToolbarMenuStates menuState);
    void clearRecycleViewFormatting();

    void refreshSearchViewQuery();

    void showTaskDeletedToast();
}
