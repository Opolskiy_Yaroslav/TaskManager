package com.example.yopolskyi.taskmanager.common;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.util.Log;

import com.example.yopolskyi.taskmanager.common.enumerations.AuthorizationTokenTypes;

import java.util.Date;

public class PreferencesAssistant {

    private final String TAG = "PreferencesAssistant";

    private Context context;

    public void setContext (Context context){
        this.context = context;
    }
    public Context getContext() {
        return context;
    }

    /**
     * Writes tokens combination to the shared preferences.
     * @param tokensCombination combination of access and refresh token
     */
    public void saveTokensCombination(TokensCombination tokensCombination) {
        Log.d(TAG, ">> [saveTokensCombination]");
        Log.d(TAG, "Input arguments:");
        Log.d(TAG, tokensCombination.getAccessToken().toString());
        Log.d(TAG, tokensCombination.getRefreshToken().toString());

        Log.d(TAG, "Writing input argument to the shared preferences.");

        SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = sharedPref.edit();

        editor.putString("Access token", tokensCombination.getAccessToken().getTokenValue());

        Date accessTokenExpDate = tokensCombination.getAccessToken().getTokenExpirationDate();
        editor.putString("Access token expiration date", Utils.convertDateToString(accessTokenExpDate,
                Constants.TOKENS_DATE_FORMAT));

        editor.putString("Refresh token", tokensCombination.getRefreshToken().getTokenValue());

        Date refreshTokenExpDate = tokensCombination.getRefreshToken().getTokenExpirationDate();
        editor.putString("Refresh token expiration date", Utils.convertDateToString(refreshTokenExpDate,
                Constants.TOKENS_DATE_FORMAT));

        editor.apply();

        Log.d(TAG, "Completed.");
        Log.d(TAG, "<< [saveTokensCombination]");
    }

    /**
     * Returns tokens combination from shared preferences.
     */
    public TokensCombination getTokensCombination(){
        Log.d(TAG, ">> [getTokensCombination]");

        Log.d(TAG, "Try to get shared preferences.");
        SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(context);

        // Access token.
        String accessTokenValue = sharedPref.getString("Access token", "Not defined");
        Log.d(TAG, "Access token = " + accessTokenValue);


        // Access token expiration date.
        String sharedAccessTokenExpDate = sharedPref.getString("Access token expiration date",
                "Not defined");

        Date accessTokenExpirationDate = null;
        if(!sharedAccessTokenExpDate.equals("Not defined")){
            accessTokenExpirationDate = Utils.convertStringToDate(sharedAccessTokenExpDate,
                    Constants.TOKENS_DATE_FORMAT);
            Log.d(TAG, "Access token exp date = " + accessTokenExpirationDate);
        }
        else {
            Log.d(TAG, "Access token exp date is not defined.");
        }





        // Refresh token.
        String refreshTokenValue = sharedPref.getString("Refresh token", "Not defined");
        Log.d(TAG, "Refresh token = " + refreshTokenValue);


        // Refresh token expiration date.
        String sharedRefreshTokenExpDate = sharedPref.getString("Refresh token expiration date",
                "Not defined");

        Date refreshTokenExpirationDate = null;
        if(!sharedRefreshTokenExpDate.equals("Not defined")){
            refreshTokenExpirationDate = Utils.convertStringToDate(sharedRefreshTokenExpDate,
                    Constants.TOKENS_DATE_FORMAT);
            Log.d(TAG, "Refresh token exp date = " + refreshTokenExpirationDate);
        }
        else {
            Log.d(TAG, "Refresh token exp date is not defined.");
        }

        // Shared preferences do not contains user ID value, so set it "-1".
        AuthorizationToken accessToken = new AuthorizationToken(-1, accessTokenValue,
                accessTokenExpirationDate, AuthorizationTokenTypes.Access);
        AuthorizationToken refreshToken = new AuthorizationToken(-1, refreshTokenValue,
                refreshTokenExpirationDate, AuthorizationTokenTypes.Refresh);

        Log.d(TAG, "<< [getTokensCombination]");

        return new TokensCombination(accessToken, refreshToken);
    }

    public void clearSharedPreferences(){
        Log.d(TAG, ">> [clearSharedPreferences]");

        Log.d(TAG, "Try to get shared preferences.");
        SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(context);

        Log.d(TAG, "Try to remove values from shared preferences.");

        SharedPreferences.Editor editor = sharedPref.edit();
        editor.remove("Access token");
        editor.remove("Access token expiration date");
        editor.remove("Refresh token");
        editor.remove("Refresh token expiration date");

        editor.apply();

        Log.d(TAG, "Completed.");

        Log.d(TAG, "<< [clearSharedPreferences]");
    }
}
