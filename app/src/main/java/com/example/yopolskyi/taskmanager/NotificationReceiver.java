package com.example.yopolskyi.taskmanager;

import android.app.AlarmManager;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v4.app.Fragment;
import android.support.v4.app.NotificationCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;

import com.example.yopolskyi.taskmanager.common.Constants;
import com.example.yopolskyi.taskmanager.common.ResultObject;
import com.example.yopolskyi.taskmanager.common.Task;
import com.example.yopolskyi.taskmanager.common.Utils;
import com.example.yopolskyi.taskmanager.common.dependencyInjection.main.MainComponentAssistant;
import com.example.yopolskyi.taskmanager.common.enumerations.ResultMessage;
import com.example.yopolskyi.taskmanager.common.enumerations.TaskExecutionStatuses;
import com.example.yopolskyi.taskmanager.model.IModel;
import com.example.yopolskyi.taskmanager.views.mainView.MainActivity;
import com.example.yopolskyi.taskmanager.views.mainView.fragments.TaskCategoryFragment.ITaskCategoryFragment;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import javax.inject.Inject;

import static android.content.Context.ALARM_SERVICE;
import static android.content.Context.MODE_PRIVATE;
import static android.support.v4.app.NotificationCompat.DEFAULT_LIGHTS;
import static android.support.v4.app.NotificationCompat.DEFAULT_SOUND;
import static android.support.v4.app.NotificationCompat.DEFAULT_VIBRATE;
import static com.example.yopolskyi.taskmanager.common.Constants.DAY_SUMMARY_NOTIFICATION_INTENT_ACTION;
import static com.example.yopolskyi.taskmanager.common.Constants.DAY_SUMMARY_NOTIFICATION_REQUEST_CODE;
import static com.example.yopolskyi.taskmanager.common.Constants.TASK_NOTIFICATION_INTENT_ACTION;
import static com.example.yopolskyi.taskmanager.common.Constants.TASK_NOTIFICATION_INTENT_TASK_ID_EXTRA;
import static com.example.yopolskyi.taskmanager.common.Constants.TASK_NOTIFICATION_REQUEST_CODE;

/**
 * Catches notification alarm manager alarm and creates a notifications.
 */
public class NotificationReceiver extends BroadcastReceiver {

    @Inject
    IModel model;

    private SharedPreferences sharedPreferences;

    private String accessToken;

    private NotificationManager notificationManager;
    private AlarmManager alarmManager;

    // For logging.
    private final String TAG = "NotificationReceiver";



    @Override
    public void onReceive(Context context, Intent intent) {

        initFields(context);

        if(intent.getAction() != null){

            // Restore notification after device rebooting.
            if(intent.getAction().equals("android.intent.action.BOOT_COMPLETED")){
                refreshesAllUserNotificationAlarms(context);
                return;
            }
            if(intent.getAction().equals(DAY_SUMMARY_NOTIFICATION_INTENT_ACTION)){
                showDaySummaryNotification(context);
                return;
            }
            if(intent.getAction().equals(TASK_NOTIFICATION_INTENT_ACTION)){
                showTaskNotification(context, intent);

                // Try to refresh task list in real time if TaskCategoryFragment is opened when
                // notification shows.

                // Get active activity (if application is running).
                if (App.getActiveActivity() instanceof MainActivity) {

                    // Get active fragment.
                    Fragment currentVisibleFragment = ((AppCompatActivity)App.getActiveActivity())
                            .getSupportFragmentManager().findFragmentByTag("current_fragment");

                    if(currentVisibleFragment instanceof ITaskCategoryFragment){
                        if (currentVisibleFragment.isVisible()) {

                            // Refresh task content.
                            ((MainActivity) App.getActiveActivity()).getPresenter()
                                    .refreshTaskStatuses(accessToken);
                            ((ITaskCategoryFragment) currentVisibleFragment).updateTaskList();

                            if(((ITaskCategoryFragment)currentVisibleFragment).isSearchViewExpanded()){
                                ((ITaskCategoryFragment) currentVisibleFragment).refreshSearchViewQuery();
                            }
                        }
                    }
                }
            }
        }
    }

    private void showDaySummaryNotification(Context context){

        Intent tasksForTodayIntent = new Intent(context, MainActivity.class)
                .setAction(DAY_SUMMARY_NOTIFICATION_INTENT_ACTION)
                .setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);

        PendingIntent pendingIntent = PendingIntent.getActivity(context,
                DAY_SUMMARY_NOTIFICATION_REQUEST_CODE, tasksForTodayIntent, PendingIntent.FLAG_UPDATE_CURRENT);

        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(context)
                .setContentIntent(pendingIntent)
                .setSmallIcon(R.mipmap.ic_launcher)
                .setAutoCancel(true);


        int numberOfTasksForToday = calculateNumberOfTasksForToday();

        // Set notification title and text based on the number of tasks for today.
        if(numberOfTasksForToday > 0){
            if(numberOfTasksForToday == 1){
                notificationBuilder
                        .setContentTitle(context.getString(R.string.notification_day_summary_title_part_1)
                                + " " + numberOfTasksForToday + " "
                                + context.getString(R.string.notification_day_summary_title_one_task_part_2))
                        .setContentText(context.getString(R.string.notification_day_summary_text));
            }
            else {
                notificationBuilder
                        .setContentTitle(context.getString(R.string.notification_day_summary_title_part_1)
                                + " " + numberOfTasksForToday + " "
                                + context.getString(R.string.notification_day_summary_title_many_tasks_part_2))
                        .setContentText(context.getString(R.string.notification_day_summary_text));
            }
        }
        else {
            notificationBuilder
                    .setContentTitle(context.getString(R.string.notification_day_summary_title_no_tasks_for_today))
                    .setContentText(context.getString(R.string.notification_day_summary_text_no_tasks_for_today));
        }

        // Get sound and vibrations settings from shared preferences.
        boolean isSoundEnabled = sharedPreferences.getBoolean(
                "pref_key_settings_sound", false);
        boolean isVibrationEnabled = sharedPreferences.getBoolean(
                "pref_key_settings_vibration", false);

        if(isSoundEnabled && isVibrationEnabled){
            notificationBuilder.setDefaults(DEFAULT_SOUND | DEFAULT_VIBRATE | DEFAULT_LIGHTS);
        }
        else if(isSoundEnabled){
            notificationBuilder.setDefaults(DEFAULT_SOUND | DEFAULT_LIGHTS);
        }
        else {
            notificationBuilder.setDefaults(DEFAULT_VIBRATE | DEFAULT_LIGHTS);
        }

        if (notificationManager != null) {
            notificationManager.notify(DAY_SUMMARY_NOTIFICATION_REQUEST_CODE, notificationBuilder.build());
        }
    }

    private int calculateNumberOfTasksForToday(){

        Log.d(TAG, "Calling the model getTasks method.");
        ResultObject getTasksResult = model.getTasks(accessToken);
        if(getTasksResult.getResultMessage() != ResultMessage.SUCCESSFUL){
            return -1;
        }

        // Get all tasks for current user.
        List<Task> taskList = (ArrayList) getTasksResult.getResultValue();

        // Leave only tasks for current day.
        Calendar taskDueDate = Calendar.getInstance();

        for (Iterator<Task> taskListIterator = taskList.listIterator(); taskListIterator.hasNext(); ) {
            Task currentTask = taskListIterator.next();
            Date currentTaskDueDate = currentTask.getDueDate();

            // If task due date do not set - remove this task.
            if(currentTaskDueDate == null){
                taskListIterator.remove();
                continue;
            }

            // If task finished - remove this task.
            if(currentTask.getTaskExecutionStatus() == TaskExecutionStatuses.finished){
                taskListIterator.remove();
                continue;
            }

            else{
                taskDueDate.setTime(currentTaskDueDate);
            }

            if (taskDueDate.get(Calendar.DAY_OF_YEAR) != Calendar.getInstance().get(Calendar.DAY_OF_YEAR)){
                taskListIterator.remove();
            }
        }

        // Return a number of tasks for today.
        return taskList.size();
    }

    private void showTaskNotification(Context context, Intent intent){

        Intent taskNotificationIntent = new Intent(context, MainActivity.class)
                .setAction(TASK_NOTIFICATION_INTENT_ACTION)
                .setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);

        PendingIntent pendingIntent = PendingIntent.getActivity(context,
                TASK_NOTIFICATION_REQUEST_CODE, taskNotificationIntent, PendingIntent.FLAG_UPDATE_CURRENT);

        int taskId = intent.getIntExtra(TASK_NOTIFICATION_INTENT_TASK_ID_EXTRA, -1);

        ResultObject getTaskResult = model.getTask(taskId, accessToken);
        if(getTaskResult.getResultMessage() != ResultMessage.SUCCESSFUL){
            return;
        }

        Task currentTask = (Task) getTaskResult.getResultValue();


        // Task title in format "Task at hh:mm".
        String taskTitle = context.getString(R.string.notification_task_title) + " " + Utils.convertDateToString(currentTask.getDueTime(),
                Constants.EXPIRATION_TIME_FORMAT);

        String taskContent = currentTask.getContent();

        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(context)
                .setContentIntent(pendingIntent)
                .setSmallIcon(R.mipmap.ic_launcher)
                .setAutoCancel(true)
                .setContentTitle(taskTitle)
                .setContentText(taskContent);

        // Get sound and vibrations settings from shared preferences.
        boolean isSoundEnabled = sharedPreferences.getBoolean(
                "pref_key_settings_sound", false);
        boolean isVibrationEnabled = sharedPreferences.getBoolean(
                "pref_key_settings_vibration", false);

        if(isSoundEnabled && isVibrationEnabled){
            notificationBuilder.setDefaults(DEFAULT_SOUND | DEFAULT_VIBRATE | DEFAULT_LIGHTS);
        }
        else if(isSoundEnabled){
            notificationBuilder.setDefaults(DEFAULT_SOUND | DEFAULT_LIGHTS)
                                // Disable vibration.
                                .setVibrate(new long[]{0L});
        }
        else {
            notificationBuilder.setDefaults(DEFAULT_VIBRATE | DEFAULT_LIGHTS);
        }

        if (notificationManager != null) {
            notificationManager.notify(TASK_NOTIFICATION_REQUEST_CODE, notificationBuilder.build());
        }
    }

    private void refreshesAllUserNotificationAlarms(Context context){

        // Day summary notification alarm.

        boolean isDaySummaryNotificationEnabledInSettings = sharedPreferences.getBoolean(
                "pref_key_settings_day_summary", false);

        Intent intent = new Intent(context, NotificationReceiver.class)
                .setAction(Constants.DAY_SUMMARY_NOTIFICATION_INTENT_ACTION);

        PendingIntent pendingIntent = PendingIntent.getBroadcast(context,
                0,
                intent, PendingIntent.FLAG_UPDATE_CURRENT);

        // Remove previous alarm.
        if (alarmManager != null) {
            alarmManager.cancel(pendingIntent);
        }

        // If user checked day summary notification preference - create alarm.
        if(isDaySummaryNotificationEnabledInSettings){
            // Get alarm time from shared preferences.

            int daySummaryTimeInMinutesAfterMidnight = sharedPreferences.getInt(
                    "pref_key_settings_day_summary_time", -1);

            int hours = daySummaryTimeInMinutesAfterMidnight / 60;
            int minutes = daySummaryTimeInMinutesAfterMidnight % 60;

            Calendar alarmTimeCalendar = Calendar.getInstance();
            Date currentTime = alarmTimeCalendar.getTime();

            alarmTimeCalendar.set(Calendar.HOUR_OF_DAY, hours);
            alarmTimeCalendar.set(Calendar.MINUTE, minutes);
            alarmTimeCalendar.set(Calendar.SECOND, 0);
            Date alarmTime = alarmTimeCalendar.getTime();

            // If alarm time before current time - alarm will be called immediately after finishing
            // this method, so move it the next day.
            if(alarmTime.before(currentTime)){
                alarmTimeCalendar.add(Calendar.DAY_OF_WEEK, 1);
            }

            if (alarmManager != null) {
                alarmManager.setRepeating(AlarmManager.RTC_WAKEUP, alarmTimeCalendar.getTimeInMillis(),
                        AlarmManager.INTERVAL_DAY, pendingIntent);
            }
        }


        // Task notification alarms.

        // Get all tasks for current user.
        Log.d(TAG, "Calling the model getTasks method.");
        ResultObject getTasksResult = model.getTasks(accessToken);
        if(getTasksResult.getResultMessage() != ResultMessage.SUCCESSFUL){
            return;
        }

        // Get all tasks for current user.
        List<Task> taskList = (ArrayList) getTasksResult.getResultValue();

        // Refresh notification for every task.
        for (Task task : taskList) {

            if(task.getTaskExecutionStatus() != TaskExecutionStatuses.inProgress){
                break;
            }

            if(task.getDueDate() == null){
                break;
            }

            if(task.getDueTime() == null){
                break;
            }

            // Create notification receiver intent.
            Intent taskNotificationItent = new Intent(context, NotificationReceiver.class)
                    .setAction(Constants.TASK_NOTIFICATION_INTENT_ACTION)
                    .putExtra(TASK_NOTIFICATION_INTENT_TASK_ID_EXTRA, task.getId());
            PendingIntent taskNotificationPendingIntent = PendingIntent.getBroadcast(context,
                    task.getId(), taskNotificationItent, PendingIntent.FLAG_UPDATE_CURRENT);

            // Remove previous alarm for this task if it exist.
            if (alarmManager != null) {
                alarmManager.cancel(pendingIntent);
            }

            Calendar alarmCalendar = Calendar.getInstance();
            // Set alarm day.
            alarmCalendar.setTime(task.getDueDate());

            Calendar alarmTimeCalendar = Calendar.getInstance();
            alarmTimeCalendar.setTime(task.getDueDate());

            // Set alarm time.
            alarmCalendar.set(Calendar.HOUR_OF_DAY, alarmTimeCalendar.get(Calendar.HOUR_OF_DAY));
            alarmCalendar.set(Calendar.MINUTE, alarmTimeCalendar.get(Calendar.MINUTE));

            if (alarmManager != null) {
                alarmManager.set(AlarmManager.RTC_WAKEUP, alarmCalendar.getTimeInMillis(),
                        taskNotificationPendingIntent);
            }
        }
    }

    private void initFields(Context context){

        MainComponentAssistant.createComponent();
        MainComponentAssistant.getComponent().injectNotificationReceiver(this);

        MainComponentAssistant.getComponent().getRepository().setContext(context);

        sharedPreferences = context.getSharedPreferences(context.getPackageName()
                + "_preferences", MODE_PRIVATE);

        accessToken = sharedPreferences.getString("Access token", "Not defined");

        notificationManager = (NotificationManager) context
                .getSystemService(Context.NOTIFICATION_SERVICE);

        alarmManager = (AlarmManager) context.getSystemService(ALARM_SERVICE);
    }
}
