package com.example.yopolskyi.taskmanager.common.dependencyInjection.mainActivity;

import com.example.yopolskyi.taskmanager.views.mainView.IMainPresenter;
import com.example.yopolskyi.taskmanager.views.mainView.MainActivity;
import com.example.yopolskyi.taskmanager.views.mainView.MainPresenter;

import dagger.Module;
import dagger.Provides;

@Module
public class MainActivityModule {

    private final MainActivity mainActivity;

    public MainActivityModule(MainActivity mainActivity) {
        this.mainActivity = mainActivity;
    }

    @Provides
    @MainActivityScope
    IMainPresenter provideMainPresenter(){
        return new MainPresenter(mainActivity);
    }
}
