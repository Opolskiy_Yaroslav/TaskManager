package com.example.yopolskyi.taskmanager.views.mainView;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.GravityCompat;
import android.support.v4.view.MenuItemCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Spinner;

import com.example.yopolskyi.taskmanager.App;
import com.example.yopolskyi.taskmanager.NotificationReceiver;
import com.example.yopolskyi.taskmanager.R;
import com.example.yopolskyi.taskmanager.TaskStatusesRefreshReceiver;
import com.example.yopolskyi.taskmanager.common.Constants;
import com.example.yopolskyi.taskmanager.common.PreferencesAssistant;
import com.example.yopolskyi.taskmanager.common.TokensCombination;
import com.example.yopolskyi.taskmanager.common.dependencyInjection.main.MainComponentAssistant;
import com.example.yopolskyi.taskmanager.common.dependencyInjection.mainActivity.DaggerMainActivityComponent;
import com.example.yopolskyi.taskmanager.common.dependencyInjection.mainActivity.MainActivityModule;
import com.example.yopolskyi.taskmanager.common.enumerations.TaskExecutionStatuses;
import com.example.yopolskyi.taskmanager.common.enumerations.TaskFilterDateIntervals;
import com.example.yopolskyi.taskmanager.views.loginView.LoginActivity;
import com.example.yopolskyi.taskmanager.views.mainView.fragments.AllTasksFragment.AllTaskFragment;
import com.example.yopolskyi.taskmanager.views.mainView.fragments.EditTaskFragment.EditTaskFragment;
import com.example.yopolskyi.taskmanager.views.mainView.fragments.NewTaskFragment.INewTaskFragment;
import com.example.yopolskyi.taskmanager.views.mainView.fragments.NewTaskFragment.NewTaskFragment;
import com.example.yopolskyi.taskmanager.views.mainView.fragments.SettingsFragment.SettingsFragment;
import com.example.yopolskyi.taskmanager.views.mainView.fragments.TaskCategoryFragment.ITaskCategoryFragment;
import com.example.yopolskyi.taskmanager.views.mainView.fragments.TaskCategoryFragment.TaskCategoryFragment;

import java.util.Calendar;
import java.util.Date;

import javax.inject.Inject;

import static com.example.yopolskyi.taskmanager.common.Constants.TASK_NOTIFICATION_INTENT_TASK_ID_EXTRA;
import static com.example.yopolskyi.taskmanager.common.Constants.TASK_STATUSES_REFRESHING_REQUEST_CODE;

public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener, IMainView {

    // For logs.
    private final String TAG = "MainActivity";

    @Inject
    IMainPresenter presenter;

    @Inject
    PreferencesAssistant preferencesAssistant;

    private INewTaskFragment newTaskFragment;
    private ITaskCategoryFragment taskCategoryFragment;

    private Toolbar toolbar;
    private Spinner spinner;
    private DrawerLayout drawer;
    private ActionBarDrawerToggle toggle;

    private Menu toolbarMenu;
    private ToolbarMenuStates toolbarMenuState;

    private String actionBarTitle;

    // This dialog shows to confirm task deleting.
    private DialogInterface.OnClickListener removeTaskDialogClickListener;
    private DialogInterface.OnCancelListener removeTaskDialogCancelListener;

    // This dialog shows to warn the user that the entered data will not be saved.
    // Uses on add new task and edit task fragments.
    private DialogInterface.OnClickListener returnDialogClickListener;
    private DialogInterface.OnCancelListener returnDialogCancelListener;


    // Getters and setters
    public IMainPresenter getPresenter() {
        return presenter;
    }

    public ToolbarMenuStates getToolbarMenuState(){
        return toolbarMenuState;
    }

    // Methods
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        initItems();

        // Refresh due dates for overdue repeatable tasks.
        presenter.refreshRepeatableTaskDueDates(getAccessToken());
        // Change task execution statuses to "Overdue" if needed.
        presenter.refreshTaskStatuses(getAccessToken());

        presenter.refreshesAllUserNotificationAlarms();

        // Task Alarm for refreshing task execution statuses every day at 00:00.
        createTaskStatusesRefreshingAlarm();

        toolbarMenuState = ToolbarMenuStates.common;
    }

    @Override
    protected void onResume() {
        super.onResume();

        App.setActiveActivity(this);
    }

    @Override
    protected void onPause() {
        super.onPause();

        App.setActiveActivity(null);
    }

    @Override
    public void onBackPressed() {

        if(toolbarMenu != null)
        {
            switch (toolbarMenuState) {
                case context:
                    changeToolbarMenuState(ToolbarMenuStates.common);
                    taskCategoryFragment.clearRecycleViewFormatting();
                    break;

                case edit:
                    if(newTaskFragment.wereFieldsChanged()){
                        showReturnDialog();
                    }
                    else {
                        onBackPressedDefault();
                    }
                    break;
                default:
                    onBackPressedDefault();
                    break;
            }
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.activity_main_toolbar, menu);

        this.toolbarMenu = menu;
        initToolbarMenuItems(menu);


        if (getIntent().getAction() != null) {
            // If this activity opened from day summary or task notification - show tasks for today.
            if(getIntent().getAction().equals(Constants.DAY_SUMMARY_NOTIFICATION_INTENT_ACTION)
                    || getIntent().getAction().equals(Constants.TASK_NOTIFICATION_INTENT_ACTION)){
                spinner.setSelection(TaskFilterDateIntervals.today.ordinal());
            }
        }

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.activity_main_toolbar_confirm :
                newTaskFragment.onConfirmButtonClick();
                return true;
            case R.id.activity_main_toolbar_edit :
                int selectedTaskId = taskCategoryFragment.getSelectedTaskId();
                EditTaskFragment editTaskFragment = new EditTaskFragment();
                Bundle args = new Bundle();
                args.putInt("selectedTaskId", selectedTaskId);
                editTaskFragment.setArguments(args);

                replaceFragment(editTaskFragment);
                this.newTaskFragment = editTaskFragment;
                return true;
            case R.id.activity_main_toolbar_remove :
                showRemoveTaskDialog();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public boolean onNavigationItemSelected(MenuItem item) {

        TaskCategoryFragment categoryFragment;
        Bundle args;

        switch (item.getItemId()){
            case R.id.activity_main_nav_menu_all_tasks_item :
                categoryFragment = new AllTaskFragment();
                this.taskCategoryFragment = categoryFragment;
                replaceFragment(categoryFragment);
                break;

            case R.id.activity_main_nav_menu_in_progress_item :
                categoryFragment = new TaskCategoryFragment();

                args = new Bundle();
                args.putInt("status", TaskExecutionStatuses.inProgress.ordinal());
                categoryFragment.setArguments(args);

                this.taskCategoryFragment = categoryFragment;
                replaceFragment(categoryFragment);
                break;

            case R.id.activity_main_nav_menu_overdue_item :
                categoryFragment = new TaskCategoryFragment();

                args = new Bundle();
                args.putInt("status", TaskExecutionStatuses.overdue.ordinal());
                categoryFragment.setArguments(args);

                this.taskCategoryFragment = categoryFragment;
                replaceFragment(categoryFragment);
                break;

            case R.id.activity_main_nav_menu_finished_item :
                categoryFragment = new TaskCategoryFragment();

                args = new Bundle();
                args.putInt("status", TaskExecutionStatuses.finished.ordinal());
                categoryFragment.setArguments(args);

                this.taskCategoryFragment = categoryFragment;
                replaceFragment(categoryFragment);
                break;

            case R.id.activity_main_nav_menu_settings_item :
                replaceFragment(new SettingsFragment());
                break;

            case R.id.activity_main_nav_menu_logout_item :
                // Close activity.
                finish();

                // Delete authorization tokens.
                deleteTokens();

                // Disable all notifications for current user.
                presenter.disableAllUserNotificationAlarms();

                // Show login activity.
                startActivity(new Intent(getApplicationContext(), LoginActivity.class));
        }

        drawer.closeDrawer(GravityCompat.START);
        return true;
    }


    public void onFabNewTaskClick(View view) {
        NewTaskFragment newTaskFragment = new NewTaskFragment();
        replaceFragment(newTaskFragment);
        this.newTaskFragment = newTaskFragment;
    }


    @Override
    public void showNewTaskFragmentDueDateEditTextError(boolean isEnabled) {
        this.newTaskFragment.showDueDateEditTextError(isEnabled);
    }

    @Override
    public void showNewTaskFragmentDueTimeEditTextError(boolean isEnabled) {
        this.newTaskFragment.showDueTimeEditTextError(isEnabled);
    }

    @Override
    public void showNewTaskFragmentTaskContentEditTextErrorToast() {
        newTaskFragment.showTaskContentEditTextErrorToast();
    }

    @Override
    public void showNewTaskFragmentDueDateEditTextErrorToast() {
        newTaskFragment.showDueDateEditTextErrorToast();
    }

    @Override
    public void showNewTaskFragmentDueTimeEditTextErrorToast() {
        newTaskFragment.showDueTimeEditTextErrorToast();
    }

    @Override
    public void showNewTaskFragmentTaskAddedToast() {
        newTaskFragment.showTaskAddedToast();
    }

    @Override
    public void showTaskCategoryFragmentTaskDeletedToast() {
        taskCategoryFragment.showTaskDeletedToast();
    }

    @Override
    public void showInternalErrorDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(getString(R.string.internal_error_title))
                .setMessage(getString(R.string.internal_error_message))
                .setCancelable(false)
                .setNegativeButton("Ok",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                finish();
                            }
                        });
        AlertDialog alert = builder.create();
        alert.show();
    }

    private void showRemoveTaskDialog(){
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(getString(R.string.activity_main_remove_task_dialog_title))
                .setMessage(getString(R.string.activity_main_remove_task_dialog_message))
                .setPositiveButton(getString(
                        R.string.activity_main_remove_task_dialog_positive_button_text), removeTaskDialogClickListener)
                .setNegativeButton(getString(
                        R.string.activity_main_remove_task_dialog_negative_button_text), removeTaskDialogClickListener)
                .setOnCancelListener(removeTaskDialogCancelListener)
                .show();
    }

    /**
     * Displays dialog when user returns from edit task fragment with unsaved information.
     */
    private void showReturnDialog(){
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(getString(R.string.activity_main_return_dialog_title))
                .setMessage(getString(R.string.activity_main_return_dialog_message))
                .setPositiveButton(getString(
                        R.string.activity_main_return_dialog_positive_button_text), returnDialogClickListener)
                .setNegativeButton(getString(
                        R.string.activity_main_return_dialog_negative_button_text), returnDialogClickListener)
                .setOnCancelListener(returnDialogCancelListener)
                .show();
    }

    /**
     * Default actions for application back button.
     */
    public void onBackPressedDefault(){
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    /**
     * Gets tokens from shared preferences and returns access token value.
     * @return access token value
     */
    @Override
    public String getAccessToken() {
        return preferencesAssistant.getTokensCombination()
                                    .getAccessToken()
                                    .getTokenValue();
    }

    public void replaceFragment(Fragment newFragment){
        FragmentManager fragmentManager = getSupportFragmentManager();

        fragmentManager.beginTransaction()
                .replace(R.id.fragment_container, newFragment, "current_fragment")
                .addToBackStack(null)
                .commit();
    }

    /**
     * Sets action bar title text
     * @param title text that must be set
     */
    @Override
    public void setActionBarTitle(String title) {
        Log.d(TAG, ">> [setActionBarTitle]");
        Log.d(TAG, "Input argument:" + " Title text = " + title);

        Log.d(TAG, "Try to set title text.");

        // Remember last action bar title.
        if(title != null){
            if (!title.isEmpty()){
                actionBarTitle = title;
            }
        }

        try {
            getSupportActionBar().setTitle(title);
            Log.d(TAG, "Completed.");
        }
        catch (Exception e){
            Log.d(TAG, "Could not set title text. " + e.toString());
        }

        Log.d(TAG, "<< [setActionBarTitle]");
    }

    /**
     * Sets navigation (hamburger) bar state (must appears only on the particular fragments)
     * @param isEnabled navigation bar state
     */
    @Override
    public void setDrawerVisibility(boolean isEnabled) {
        Log.d(TAG, ">> [setDrawerVisibility]");
        Log.d(TAG, "Input argument:" + " IsEnabled = " + isEnabled);

        if (isEnabled) {
            drawer.setDrawerLockMode(DrawerLayout.LOCK_MODE_UNLOCKED);
            toggle.syncState();
        }
        else {
            drawer.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);
        }

        Log.d(TAG, "<< [setDrawerVisibility]");
    }

    /**
     * Sets float action button state(must appears only on the particular fragments).
     * @param isEnabled float action button state
     */
    @Override
    public void setFabVisibility(boolean isEnabled){
        Log.d(TAG, ">> [setFabVisibility]");
        Log.d(TAG, "Input argument:" + " IsEnabled = " + isEnabled);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fabNewTask);
        if(isEnabled){
            fab.setVisibility(View.VISIBLE);
        }
        else{
            fab.setVisibility(View.GONE);
        }

        Log.d(TAG, "<< [setFabVisibility]");
    }

    /**
     * Sets navigation menu back arrow state (must appears only on the particular fragments)
     * @param isEnabled back arrow button state
     */
    @Override
    public void setHomeButtonVisibility(boolean isEnabled){
        Log.d(TAG, ">> [setHomeButtonVisibility]");
        Log.d(TAG, "Input argument:" + " IsEnabled = " + isEnabled);

        if(getSupportActionBar() != null){
            getSupportActionBar().setDisplayHomeAsUpEnabled(isEnabled);
            getSupportActionBar().setDisplayShowHomeEnabled(isEnabled);
        }

        Log.d(TAG, "<< [setHomeButtonVisibility]");
    }

    @Override
    public void setUpDaySummaryNotificationAlarmManager(boolean isAlarmEnabled){

        Intent intent = new Intent(getApplicationContext(), NotificationReceiver.class)
                .setAction(Constants.DAY_SUMMARY_NOTIFICATION_INTENT_ACTION);

        PendingIntent pendingIntent = PendingIntent.getBroadcast(getApplicationContext(),
                0,
                intent, PendingIntent.FLAG_UPDATE_CURRENT);

        AlarmManager alarmManager = (AlarmManager) getSystemService(ALARM_SERVICE);

        // Remove previous alarm.
        if (alarmManager != null) {
            alarmManager.cancel(pendingIntent);
        }

        // If user checked day summary notification preference - create alarm.
        if(isAlarmEnabled){
            // Get alarm time from shared preferences.
            SharedPreferences sharedPref = android.preference.PreferenceManager
                    .getDefaultSharedPreferences(this);

            int daySummaryTimeInMinutesAfterMidnight = sharedPref.getInt(
                    "pref_key_settings_day_summary_time", -1);

            int hours = daySummaryTimeInMinutesAfterMidnight / 60;
            int minutes = daySummaryTimeInMinutesAfterMidnight % 60;

            Calendar alarmTimeCalendar = Calendar.getInstance();
            Date currentTime = alarmTimeCalendar.getTime();

            alarmTimeCalendar.set(Calendar.HOUR_OF_DAY, hours);
            alarmTimeCalendar.set(Calendar.MINUTE, minutes);
            alarmTimeCalendar.set(Calendar.SECOND, 0);
            Date alarmTime = alarmTimeCalendar.getTime();

            // If alarm time before current time - alarm will be called immediately after finishing
            // this method, so move it the next day.
            if(alarmTime.before(currentTime)){
                alarmTimeCalendar.add(Calendar.DAY_OF_WEEK, 1);
            }

            if (alarmManager != null) {
                alarmManager.setRepeating(AlarmManager.RTC_WAKEUP, alarmTimeCalendar.getTimeInMillis(),
                        AlarmManager.INTERVAL_DAY, pendingIntent);
            }
        }
    }

    @Override
    public void createTaskNotificationAlarm(int taskId, Date alarmDay, Date alarmTime) {

        // Create notification receiver intent and put task ID there.
        Intent intent = new Intent(getApplicationContext(), NotificationReceiver.class)
                .setAction(Constants.TASK_NOTIFICATION_INTENT_ACTION)
                .putExtra(TASK_NOTIFICATION_INTENT_TASK_ID_EXTRA, taskId);
        PendingIntent pendingIntent = PendingIntent.getBroadcast(getApplicationContext(),
                taskId, intent, PendingIntent.FLAG_UPDATE_CURRENT);

        AlarmManager alarmManager = (AlarmManager) getSystemService(ALARM_SERVICE);

        // Remove previous alarm for this task if it exist.
        if (alarmManager != null) {
            alarmManager.cancel(pendingIntent);
        }

        Calendar alarmCalendar = Calendar.getInstance();
        // Set alarm day.
        alarmCalendar.setTime(alarmDay);

        Calendar alarmTimeCalendar = Calendar.getInstance();
        alarmTimeCalendar.setTime(alarmTime);

        // Set alarm time.
        alarmCalendar.set(Calendar.HOUR_OF_DAY, alarmTimeCalendar.get(Calendar.HOUR_OF_DAY));
        alarmCalendar.set(Calendar.MINUTE, alarmTimeCalendar.get(Calendar.MINUTE));

        if (alarmManager != null) {
            alarmManager.set(AlarmManager.RTC_WAKEUP, alarmCalendar.getTimeInMillis(),
                    pendingIntent);
        }
    }

    /**
     * Creates a alarm manager that will be refresh task statuses every day at 00:00.
     */
    private void createTaskStatusesRefreshingAlarm(){

        // Create notification receiver intent and put task ID there.

        Intent intent = new Intent(getApplicationContext(), TaskStatusesRefreshReceiver.class)
                .setAction(Constants.REFRESH_TASK_EXECUTION_STATUSES_INTENT_ACTION);
        PendingIntent pendingIntent = PendingIntent.getBroadcast(getApplicationContext(),
                TASK_STATUSES_REFRESHING_REQUEST_CODE, intent, PendingIntent.FLAG_UPDATE_CURRENT);

        AlarmManager alarmManager = (AlarmManager) getSystemService(ALARM_SERVICE);

        // Remove previous alarm for this task if it exist.
        if (alarmManager != null) {
            alarmManager.cancel(pendingIntent);
        }

        Calendar alarmCalendar = Calendar.getInstance();

        // Set alarm time.
        alarmCalendar.set(Calendar.HOUR_OF_DAY, 0);
        alarmCalendar.set(Calendar.MINUTE, 0);

        if (alarmManager != null) {
            alarmManager.set(AlarmManager.RTC_WAKEUP, alarmCalendar.getTimeInMillis(),
                    pendingIntent);
        }
    }

    @Override
    public void deleteTaskNotificationAlarm(int taskId) {

        // Create notification receiver intent and put task ID there.
        Intent intent = new Intent(getApplicationContext(), NotificationReceiver.class)
                .setAction(Constants.TASK_NOTIFICATION_INTENT_ACTION)
                .putExtra(TASK_NOTIFICATION_INTENT_TASK_ID_EXTRA, taskId);
        PendingIntent pendingIntent = PendingIntent.getBroadcast(getApplicationContext(),
                taskId, intent, PendingIntent.FLAG_UPDATE_CURRENT);

        AlarmManager alarmManager = (AlarmManager) getSystemService(ALARM_SERVICE);

        // Remove previous alarm for this task if it exist.
        if (alarmManager != null) {
            alarmManager.cancel(pendingIntent);
        }

    }

    public void changeToolbarMenuState(ToolbarMenuStates menuState) {

        if(toolbarMenu == null){
            return;
        }

        // If toolbar menu is in the right state already.
        if(toolbarMenuState == menuState){
            return;
        }

        switch (menuState) {
            case common:
                setDrawerVisibility(true);
                setActionBarTitle(actionBarTitle);
                setFabVisibility(true);

                invalidateOptionsMenu();

                if(toolbar != null)
                    toolbar.setNavigationOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            if (drawer.isDrawerOpen(GravityCompat.START)) {
                                drawer.closeDrawer(GravityCompat.START);
                            } else {
                                drawer.openDrawer(GravityCompat.START);
                            }
                        }
                    });

                break;

            case search:
                for (int i = 0; i < toolbarMenu.size(); ++i) {
                    MenuItem item = toolbarMenu.getItem(i);
                    item.setVisible(false);
                }
                break;

            case context:
                toolbarMenu.setGroupVisible(R.id.activity_main_toolbar_item_group_main, false);
                toolbarMenu.setGroupVisible(R.id.activity_main_toolbar_item_group_context, true);
                toolbarMenu.setGroupVisible(R.id.activity_main_toolbar_item_group_edit, false);

                setDrawerVisibility(false);
                setActionBarTitle(null);
                setFabVisibility(false);

                // To change drawer toggle icon to back arrow.
                setHomeButtonVisibility(false);
                setHomeButtonVisibility(true);

                if(toolbar != null){
                    toolbar.setNavigationOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            changeToolbarMenuState(ToolbarMenuStates.common);
                            taskCategoryFragment.clearRecycleViewFormatting();
                        }
                    });
                }

                break;

            case edit:
                setFabVisibility(false);
                setDrawerVisibility(false);

                // To change drawer toggle icon to back arrow.
                setHomeButtonVisibility(false);
                setHomeButtonVisibility(true);

                break;

            case settings:
                setFabVisibility(false);
                break;
        }

        toolbarMenuState = menuState;
    }

    /**
     * Calls the presenter deleteTokens method with accessToken argument taken from shared preferences
     * and then clears shared preferences
     */
    private void deleteTokens(){
        Log.d(TAG, ">> [deleteTokens]");

        Log.d(TAG, "Try to get shared preferences.");

        TokensCombination tokensCombination = preferencesAssistant.getTokensCombination();

        String accessToken = tokensCombination.getAccessToken().getTokenValue();
        Log.d(TAG, "Access token = " + accessToken);

        Log.d(TAG, "Calling the presenter deleteTokens method");
        presenter.deleteTokens(accessToken);

        Log.d(TAG, "Try to remove values from shared preferences.");
        preferencesAssistant.clearSharedPreferences();

        Log.d(TAG, "Completed.");

        Log.d(TAG, "<< [deleteTokens]");
    }

    private void initItems(){
        // Dependency injection component instance creating.
        DaggerMainActivityComponent
                .builder()
                .mainActivityModule(new MainActivityModule(this))
                .mainComponent(MainComponentAssistant.getComponent())
                .build()
                .injectMainActivity(this);

        // If this activity opened from notification.
        if(preferencesAssistant.getContext() == null){
            preferencesAssistant.setContext(this);
        }

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        if (findViewById(R.id.fragment_container) != null) {
            AllTaskFragment allTasksFragment = new AllTaskFragment();
            getSupportFragmentManager().beginTransaction()
                    .add(R.id.fragment_container, allTasksFragment, "current_fragment").commit();
            taskCategoryFragment = allTasksFragment;
        }

        // Remove task dialog click listener.
        removeTaskDialogClickListener = new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                switch (which){
                    case DialogInterface.BUTTON_POSITIVE:
                        taskCategoryFragment.removeSelectedTask();
                        taskCategoryFragment.clearRecycleViewFormatting();
                        changeToolbarMenuState(ToolbarMenuStates.common);
                        break;

                    case DialogInterface.BUTTON_NEGATIVE:
                        taskCategoryFragment.clearRecycleViewFormatting();
                        changeToolbarMenuState(ToolbarMenuStates.common);
                        break;
                }
            }
        };

        removeTaskDialogCancelListener = new DialogInterface.OnCancelListener() {
            @Override
            public void onCancel(DialogInterface dialog) {
                taskCategoryFragment.clearRecycleViewFormatting();
                changeToolbarMenuState(ToolbarMenuStates.common);
            }
        };


        // Return dialog click listener.
        returnDialogClickListener = new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                switch (which){
                    case DialogInterface.BUTTON_POSITIVE:
                        onBackPressedDefault();
                        break;

                    case DialogInterface.BUTTON_NEGATIVE:
                        break;
                }
            }
        };

        returnDialogCancelListener = new DialogInterface.OnCancelListener() {
            @Override
            public void onCancel(DialogInterface dialog) {

            }
        };
    }

    private void initToolbarMenuItems(final Menu menu){
        spinner = (Spinner) MenuItemCompat.getActionView(
                menu.findItem(R.id.activity_main_toolbar_spinner));

        ArrayAdapter<TaskFilterDateIntervals> adapter = new ArrayAdapter<>(this,
                R.layout.activity_main_spinner_item, TaskFilterDateIntervals.values());
        adapter.setDropDownViewResource(R.layout.activity_main_spinner_dropdown_item);
        spinner.setAdapter(adapter);
    }
}
