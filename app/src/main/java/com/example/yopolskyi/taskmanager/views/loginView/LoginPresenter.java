package com.example.yopolskyi.taskmanager.views.loginView;

import android.util.Log;

import com.example.yopolskyi.taskmanager.common.ResultObject;
import com.example.yopolskyi.taskmanager.common.TokensCombination;
import com.example.yopolskyi.taskmanager.common.dependencyInjection.main.MainComponentAssistant;
import com.example.yopolskyi.taskmanager.common.enumerations.ResultMessage;
import com.example.yopolskyi.taskmanager.model.IModel;

import javax.inject.Inject;

public class LoginPresenter implements ILoginPresenter {

    // For logging.
    private final String TAG = "LoginPresenter";

    private ILoginView view;

    @Inject
    IModel model;

    public LoginPresenter(ILoginView view){
        this.view = view;
        MainComponentAssistant.getComponent().injectLoginPresenter(this);
    }

    /**
     * Validates username and password, calls the model doLogin method. Calls model method for
     * generating tokens, writes tokens to preferences and launches the main activity if sign in
     * was successful.
     * @param username entered username
     * @param password entered password
     */
    @Override
    public void doLogin(String username, String password){
        Log.d(TAG, ">> [doLogin]");
        Log.d(TAG, "Input arguments:" + " Username = " + username + " Password = " + password);


        Log.d(TAG, "Check if username is empty.");
        if(username.isEmpty()){
            Log.d(TAG, "Username is empty. Calls the view method to show error.");
            view.showUsernameEmptyError();
            return;
        }
        Log.d(TAG, "Username is not empty.");


        Log.d(TAG, "Check if password is empty.");
        if(password.isEmpty()){
            Log.d(TAG, "Password is empty. Calling the view method to show error.");
            view.showPasswordEmptyError();
            return;
        }
        Log.d(TAG, "Password is not empty.");


        Log.d(TAG, "Calling the model doLogin method.");

        ResultObject loginResult = model.doLogin(username, password);

        if(loginResult.getResultMessage() != ResultMessage.SUCCESSFUL){
            view.showInternalErrorDialog();
            return;
        }

        int userId = (int) loginResult.getResultValue();
        Log.d(TAG, "User ID = " + userId);

        if(userId != -1){
            Log.d(TAG, "Calling the model generateTokens method.");
            TokensCombination tokensCombination = model.generateTokens(userId);

            Log.d(TAG, "Calling the model addTokens method.");
            model.addTokens(tokensCombination);

            Log.d(TAG, "Calling the view launchMainActivity method.");
            view.launchMainActivity();

            Log.d(TAG, "Calling the view method to save user ID " +
                    "and access token.");
            view.saveTokensCombination(tokensCombination);
        }
        else{
            Log.d(TAG, "Calling the view method to show error.");
            view.showAuthorizationFailedError();
        }

        Log.d(TAG, "<< [doLogin]");
    }
}