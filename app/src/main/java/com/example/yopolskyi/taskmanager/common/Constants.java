package com.example.yopolskyi.taskmanager.common;

import java.util.Calendar;

public class Constants {

    // Default username-password combinations.
    public static final String USERNAME_1 = "user1";
    public static final String PASSWORD_1 = "pass1";
    public static final String USERNAME_2 = "user2";
    public static final String PASSWORD_2 = "pass2";

    // Date and time formats that is used in model and database.
    public static final String TOKENS_DATE_FORMAT = "yyyy/MM/dd_HH:mm:ss";
    public static final String EXPIRATION_DATE_FORMAT = "yyyy/MM/dd";
    public static final String EXPIRATION_TIME_FORMAT = "HH:mm";

    // Date format that us shown on the screen.
    public static final String DATE_FORMAT_FOR_USER = "EEE, MMM d, yyyy";

    public static final int ACCESS_TOKEN_VALIDITY_IN_DAYS = 2;
    public static final int REFRESH_TOKEN_VALIDITY_IN_DAYS = 4;

    public static final int FIRST_DAY_OF_THE_WEEK = Calendar.MONDAY;

    public static final String REFRESH_TASK_EXECUTION_STATUSES_INTENT_ACTION = "com.example.yopolskyi.taskmanager.action.REFRESH_TASK_STATUS";
    public static final String DAY_SUMMARY_NOTIFICATION_INTENT_ACTION = "com.example.yopolskyi.taskmanager.action.DAY_SUMMARY_NOTIFICATION";
    public static final String TASK_NOTIFICATION_INTENT_ACTION = "com.example.yopolskyi.taskmanager.action.TASK_NOTIFICATION";
    public static final String TASK_NOTIFICATION_INTENT_TASK_ID_EXTRA = "task_id";

    public static final int DAY_SUMMARY_NOTIFICATION_REQUEST_CODE = 0;
    public static final int TASK_NOTIFICATION_REQUEST_CODE = 1;
    public static final int TASK_STATUSES_REFRESHING_REQUEST_CODE = -1;
}
