package com.example.yopolskyi.taskmanager.views.splashView;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;

import com.example.yopolskyi.taskmanager.App;
import com.example.yopolskyi.taskmanager.R;
import com.example.yopolskyi.taskmanager.common.PreferencesAssistant;
import com.example.yopolskyi.taskmanager.common.TokensCombination;
import com.example.yopolskyi.taskmanager.common.dependencyInjection.main.MainComponentAssistant;
import com.example.yopolskyi.taskmanager.common.dependencyInjection.splashActivity.DaggerSplashActivityComponent;
import com.example.yopolskyi.taskmanager.common.dependencyInjection.splashActivity.SplashActivityComponent;
import com.example.yopolskyi.taskmanager.common.dependencyInjection.splashActivity.SplashActivityModule;
import com.example.yopolskyi.taskmanager.views.loginView.LoginActivity;
import com.example.yopolskyi.taskmanager.views.mainView.MainActivity;

import javax.inject.Inject;

public class SplashActivity extends AppCompatActivity implements ISplashView {

    // For logs.
    private final String TAG = "SplashActivity";

    @Inject
    ISplashPresenter presenter;

    @Inject
    PreferencesAssistant preferencesAssistant;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        Log.d(TAG, ">> [onCreate]");
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        // Dependency injection splash activity component instance creating.
        SplashActivityComponent component = DaggerSplashActivityComponent
                .builder()
                .splashActivityModule(new SplashActivityModule(this))
                .mainComponent(MainComponentAssistant.getComponent())
                .build();

        // Inject class fields.
        component.injectSplashActivity(this);

        Log.d(TAG, "Calls preference assistance getTokensCombination method");
        TokensCombination tokensCombination = preferencesAssistant.getTokensCombination();

        Log.d(TAG, "Calling the presenter processTokensAndLaunchNextActivity method");
        presenter.processTokensAndLaunchNextActivity(
                tokensCombination.getAccessToken().getTokenValue(),
                tokensCombination.getAccessToken().getTokenExpirationDate(),
                tokensCombination.getRefreshToken().getTokenValue(),
                tokensCombination.getRefreshToken().getTokenExpirationDate());

        Log.d(TAG, "<< [onCreate]");
    }

    @Override
    protected void onResume() {
        super.onResume();

        App.setActiveActivity(this);
    }

    @Override
    protected void onPause() {
        super.onPause();

        App.setActiveActivity(null);
    }

    public void launchLoginActivity(){
        Intent intent = new Intent(getApplicationContext(), LoginActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
        finish();
    }
    public void launchMainActivity(){
        Intent intent = new Intent(getApplicationContext(), MainActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
        finish();
    }

    /**
     * Calls the preference assistant saveTokensCombination method
     * @param tokensCombination tokens combination that must be saved
     */
    @Override
    public void saveTokensCombination(TokensCombination tokensCombination){
        Log.d(TAG, ">> [saveTokensCombination]");

        Log.d(TAG, "Calls preference assistance saveTokensCombination method");
        preferencesAssistant.saveTokensCombination(tokensCombination);

        Log.d(TAG, "<< [saveTokensCombination]");
    }

    @Override
    public void showInternalErrorDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(getString(R.string.internal_error_title))
                .setMessage(getString(R.string.internal_error_message))
                .setCancelable(false)
                .setNegativeButton("Ok",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                finish();
                            }
                        });
        AlertDialog alert = builder.create();
        alert.show();
    }
}
