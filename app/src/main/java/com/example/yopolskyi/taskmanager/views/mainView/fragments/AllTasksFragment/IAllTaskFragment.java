package com.example.yopolskyi.taskmanager.views.mainView.fragments.AllTasksFragment;


import com.example.yopolskyi.taskmanager.common.enumerations.TaskExecutionStatuses;

public interface IAllTaskFragment {
    void showInProgressTaskCategoryName(int visibilityState);
    void showOverdueTaskCategoryName(int visibilityState);
    void showFinishedTaskCategoryName(int visibilityState);
    void makeOnlyOneTaskSelected(TaskExecutionStatuses executionStatus);
}
