package com.example.yopolskyi.taskmanager.common.dependencyInjection.loginActivity;

import javax.inject.Scope;

@Scope
public @interface LoginActivityScope {
}
