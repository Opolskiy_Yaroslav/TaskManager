package com.example.yopolskyi.taskmanager.common.enumerations;


public enum TaskExecutionStatuses {
    inProgress,
    overdue,
    finished
}
