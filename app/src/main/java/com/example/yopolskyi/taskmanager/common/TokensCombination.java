package com.example.yopolskyi.taskmanager.common;

/**
 *  Represents access and refresh tokens combination.
 */
public class TokensCombination {
    private AuthorizationToken accessToken;
    private AuthorizationToken refreshToken;


    public TokensCombination(AuthorizationToken accessToken, AuthorizationToken refreshToken) {
        this.accessToken = accessToken;
        this.refreshToken = refreshToken;
    }


    public AuthorizationToken getAccessToken() {
        return accessToken;
    }

    public void setAccessToken(AuthorizationToken accessToken) {
        this.accessToken = accessToken;
    }

    public AuthorizationToken getRefreshToken() {
        return refreshToken;
    }

    public void setRefreshToken(AuthorizationToken refreshToken) {
        this.refreshToken = refreshToken;
    }
}
