package com.example.yopolskyi.taskmanager.views.loginView;

import com.example.yopolskyi.taskmanager.common.TokensCombination;

public interface ILoginView {
    void showUsernameEmptyError();
    void showPasswordEmptyError();
    void showAuthorizationFailedError();
    void showInternalErrorDialog();
    void launchMainActivity();
    void saveTokensCombination(TokensCombination tokensCombination);
}
