package com.example.yopolskyi.taskmanager.common.dependencyInjection.main;

import javax.inject.Scope;

@Scope
public @interface MainScope {
}
