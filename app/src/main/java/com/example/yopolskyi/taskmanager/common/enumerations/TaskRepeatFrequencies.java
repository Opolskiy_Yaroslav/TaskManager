package com.example.yopolskyi.taskmanager.common.enumerations;


public enum TaskRepeatFrequencies {
    noRepeat("No repeat"),
    day("Every day"),
    week("Every week"),
    month("Every month"),
    year("Every year");

    private String friendlyName;

    TaskRepeatFrequencies(String friendlyName){
        this.friendlyName = friendlyName;
    }

    @Override public String toString(){
        return friendlyName;
    }
}
