package com.example.yopolskyi.taskmanager.views.mainView.fragments.SettingsFragment;

import android.content.Context;
import android.content.res.TypedArray;
import android.support.v7.preference.DialogPreference;
import android.util.AttributeSet;

import com.example.yopolskyi.taskmanager.R;

public class TimePreference extends DialogPreference {

    // TimePicker time in minutes after midnight.
    private int timeInMinutes;

    public TimePreference(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public int getTime() {
        return timeInMinutes;
    }

    public void setTime(int time) {
        timeInMinutes = time;

        // Save to Shared Preferences
        persistInt(time);
    }

    @Override
    protected Object onGetDefaultValue(TypedArray a, int index) {
        // Default value from attribute. Fallback value is set to 0.
        return a.getInt(index, 0);
    }

    @Override
    protected void onSetInitialValue(boolean restorePersistedValue,
                                     Object defaultValue) {
        // Read the value. Use the default value if it is not possible.
        setTime(restorePersistedValue ?
                getPersistedInt(timeInMinutes) : (int) defaultValue);
    }

    @Override
    public int getDialogLayoutResource() {
        return R.layout.fragment_settings_time_picker;
    }
}
