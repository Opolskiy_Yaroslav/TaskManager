package com.example.yopolskyi.taskmanager.common;

import android.support.annotation.Nullable;
import android.util.Log;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.Locale;

public class Utils {
    private static final String TAG = "Utils";

    /**
     * Converts date in the string format to date.
     * @param string date in the string format
     * @param dateFormat date format for parsing
     * @return converted date
     */
    @Nullable
    public static Date convertStringToDate(String string, String dateFormat){
        if(string == null || string.equals("")){
            return null;
        }

        DateFormat format = new SimpleDateFormat(dateFormat, Locale.getDefault());

        Date resultDate = null;
        try {
            resultDate = format.parse(string);
        } catch (ParseException e) {
            Log.d(TAG, "Could not parse date. " + e.toString());
        }

        return resultDate;
    }

    /**
     * Converts date to string
     * @param date date that must be converted
     * @param sDateFormat date format for result string
     * @return converted string
     */
    @Nullable
    public static String convertDateToString(Date date, String sDateFormat){
        if(date == null){
            return null;
        }

        DateFormat dateFormat = new SimpleDateFormat(sDateFormat, Locale.getDefault());

        return dateFormat.format(date);
    }

    /**
     * Sort task list by due date and time.
     * @param taskList list of tasks that must be sorted
     * @return sorted task list
     */
    public static List<Task> sortTaskListByDate(List<Task> taskList){

        if(taskList == null){
            throw new NullPointerException();
        }

        Collections.sort(taskList, new Comparator<Task>() {
            public int compare(Task task1, Task task2) {
                // If due date not set - put task to the bottom.
                if(task1.getDueDate() == null || task2.getDueDate() == null){
                    return -1;
                }
                if(task1.getDueDate().after(task2.getDueDate())){
                    return 1;
                }
                if(task1.getDueDate().before(task2.getDueDate())){
                    return -1;
                }
                if(task1.getDueDate().equals(task2.getDueDate())){
                    if(task1.getDueTime() == null || task2.getDueTime() == null){
                        return -1;
                    }
                    if(task1.getDueTime().after(task2.getDueTime())){
                        return 1;
                    }
                    if(task1.getDueTime().before(task2.getDueTime())){
                        return -1;
                    }
                }
                return 0;
            }
        });
        return taskList;
    }
}
