package com.example.yopolskyi.taskmanager.common.dependencyInjection.splashActivity;

import com.example.yopolskyi.taskmanager.common.dependencyInjection.main.MainComponent;
import com.example.yopolskyi.taskmanager.views.splashView.SplashActivity;

import dagger.Component;

@Component(modules = SplashActivityModule.class, dependencies = MainComponent.class)
@SplashActivityScope
public interface SplashActivityComponent {
    void injectSplashActivity(SplashActivity splashActivity);
}
