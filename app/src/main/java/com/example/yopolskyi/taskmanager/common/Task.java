package com.example.yopolskyi.taskmanager.common;

import com.example.yopolskyi.taskmanager.common.enumerations.TaskExecutionStatuses;
import com.example.yopolskyi.taskmanager.common.enumerations.TaskRepeatFrequencies;

import java.util.Date;

public class Task {

    // Fields
    private int id;
    private String content;
    private Date dueDate;
    private Date dueTime;
    private TaskRepeatFrequencies repeatFrequency;
    private TaskExecutionStatuses taskExecutionStatus;
    private String username;

    //Getters and setters
    public int getId() {
        return id;
    }
    public void setId(int id) {
        this.id = id;
    }

    public String getContent() {
        return content;
    }
    public void setContent(String content) {
        this.content = content;
    }

    public Date getDueDate() {
        return dueDate;
    }
    public void setDueDate(Date dueDate) {
        this.dueDate = dueDate;
    }

    public Date getDueTime() {
        return dueTime;
    }
    public void setDueTime(Date dueTime) {
        this.dueTime = dueTime;
    }

    public TaskRepeatFrequencies getRepeatFrequency() {
        return repeatFrequency;
    }
    public void setRepeatFrequency(TaskRepeatFrequencies repeatFrequency) {
        this.repeatFrequency = repeatFrequency;
    }

    public TaskExecutionStatuses getTaskExecutionStatus() {
        return taskExecutionStatus;
    }
    public void setTaskExecutionStatus(TaskExecutionStatuses taskExecutionStatus) {
        this.taskExecutionStatus = taskExecutionStatus;
    }

    public String getUsername() {
        return username;
    }
    public void setUsername(String username) {
        this.username = username;
    }


    @Override
    public String toString() {
        return  "Task ID = " + getId() +
                " Content = " + getContent() +
                " Due date = " + Utils.convertDateToString(getDueDate(),
                Constants.EXPIRATION_DATE_FORMAT) +
                " Due time = " + Utils.convertDateToString(getDueTime(),
                Constants.EXPIRATION_TIME_FORMAT) +
                " RepeatFrequency = " + getRepeatFrequency() +
                " Execution status = " + getTaskExecutionStatus() +
                " Username = " + getUsername();
    }
}
