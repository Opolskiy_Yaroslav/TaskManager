package com.example.yopolskyi.taskmanager.views.mainView.fragments.NewTaskFragment;


import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.ArrayAdapter;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.example.yopolskyi.taskmanager.R;
import com.example.yopolskyi.taskmanager.common.Constants;
import com.example.yopolskyi.taskmanager.common.enumerations.TaskRepeatFrequencies;
import com.example.yopolskyi.taskmanager.views.mainView.IMainPresenter;
import com.example.yopolskyi.taskmanager.views.mainView.IMainView;
import com.example.yopolskyi.taskmanager.views.mainView.MainActivity;
import com.example.yopolskyi.taskmanager.views.mainView.ToolbarMenuStates;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;

public class NewTaskFragment extends Fragment implements INewTaskFragment {

    private final String TAG = "NewTaskFragment";

    protected IMainPresenter presenter;
    protected IMainView parentView;

    protected Toolbar toolbar;
    protected EditText edTaskContent;
    protected EditText edDueDate;
    protected ImageButton btnDueDateClear;
    protected EditText edDueTime;
    protected ImageButton btnDueTimeClear;
    protected TextView tvRepeat;
    protected Spinner spinnerRepeatFrequency;

    protected Calendar calendar;
    protected DatePickerDialog datePickerDialog;
    protected TimePickerDialog timePickerDialog;
    protected DateFormat dateFormatter;


    // Constructors
    public NewTaskFragment() {
        // Required empty public constructor
    }


    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        Log.d(TAG, ">> [onCreateView]");

        setHasOptionsMenu(true);
        View view = inflater.inflate(R.layout.fragment_new_task, container, false);

        initItems(view);
        setItemsEventsListeners();

        Log.d(TAG, "<< [onCreateView]");

        return view;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);

        menu.setGroupVisible(R.id.activity_main_toolbar_item_group_main, false);
        menu.setGroupVisible(R.id.activity_main_toolbar_item_group_context, false);
        menu.setGroupVisible(R.id.activity_main_toolbar_item_group_edit, true);

        toolbar = (Toolbar) getActivity().findViewById(R.id.toolbar);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                parentView.onBackPressed();
            }
        });
    }

    @Override
    public void onResume() {
        super.onResume();

        parentView.setActionBarTitle(getResources().getString(R.string.activity_main_new_task_fragment_toolbar_title));
        parentView.changeToolbarMenuState(ToolbarMenuStates.edit);
    }


    /**
     * Shows due date edit text error and fills it's text into error color
     * @param isEnabled error is displayed or hidden
     */
    public void showDueDateEditTextError(Boolean isEnabled){
        if(isEnabled){
            edDueDate.setTextColor(getResources().getColor(
                    R.color.activity_main_new_task_fragment_ed_text_error_color));
            edDueDate.setError(getString(R.string.activity_main_new_task_fragment_due_date_error_toast));
        }
        else{
            edDueDate.setTextColor(getResources().getColor(
                    R.color.activity_main_new_task_fragment_ed_text_color));
            edDueDate.setError(null);
        }
    }

    /**
     * Shows due time edit text error and fills it's text into error color
     * @param isEnabled error is displayed or hidden
     */
    public void showDueTimeEditTextError(Boolean isEnabled){
        if(isEnabled){
            edDueTime.setTextColor(getResources().getColor(
                    R.color.activity_main_new_task_fragment_ed_text_error_color));
            edDueTime.setError(getString(R.string.activity_main_new_task_fragment_due_date_error_toast));
        }
        else{
            edDueTime.setTextColor(getResources().getColor(
                    R.color.activity_main_new_task_fragment_ed_text_color));
            edDueTime.setError(null);
        }
    }

    @Override
    public void showTaskContentEditTextErrorToast() {
        Toast.makeText(getActivity(),
                getString(R.string.activity_main_new_task_fragment_task_content_error_toast), Toast.LENGTH_LONG).show();
    }

    @Override
    public void showDueDateEditTextErrorToast() {
        Toast.makeText(getActivity(),
                getString(R.string.activity_main_new_task_fragment_due_date_error_toast),
                Toast.LENGTH_LONG).show();
    }

    @Override
    public void showDueTimeEditTextErrorToast() {
        Toast.makeText(getActivity(),
                getString(R.string.activity_main_new_task_fragment_due_time_error_toast),
                Toast.LENGTH_LONG).show();
    }

    @Override
    public void showTaskAddedToast(){
        Toast.makeText(getContext(),
                getString(R.string.activity_main_new_task_fragment_task_added_toast_message),
                Toast.LENGTH_LONG).show();
    }


    /**
     * Checks entered value and if calls presenter addTask method if values are correct
     */
    @Override
    public void onConfirmButtonClick(){
        Log.d(TAG, ">> [onConfirmButtonClick]");

        Log.d(TAG, "Calls presenter addTask method");
        presenter.addTask(edTaskContent.getText().toString(), edDueDate.getText().toString(),
                edDueTime.getText().toString(), spinnerRepeatFrequency.getSelectedItemPosition());

        Log.d(TAG, "<< [onConfirmButtonClick]");
    }

    /**
     * Checks if fields contains data.
     * @return "true" if fields contains data and "false" in another case.
     */
    @Override
    public boolean wereFieldsChanged() {

        if(!edTaskContent.getText().toString().isEmpty()){
            return true;
        }

        if(!edDueDate.getText().toString().isEmpty()){
            return true;
        }

        if(!edDueTime.getText().toString().isEmpty()){
            return true;
        }

        if(spinnerRepeatFrequency.getSelectedItemPosition() != 0){
            return true;
        }

        return false;
    }

    /**
     * Initializes all fragment items
     */
    protected void initItems(View view){
        Log.d(TAG, ">> [initItems]");

        parentView = (MainActivity)getActivity();
        presenter = parentView.getPresenter();

        edTaskContent = (EditText) view.findViewById(R.id.activity_main_new_task_fragment_task_content_et);
        edDueDate = (EditText) view.findViewById(R.id.activity_main_new_task_fragment_due_date_et);
        edDueTime = (EditText) view.findViewById(R.id.activity_main_new_task_fragment_due_time_et);
        calendar = Calendar.getInstance();
        btnDueDateClear = (ImageButton) view.findViewById(
                R.id.activity_main_new_task_fragment_due_date_clear_button);
        btnDueTimeClear = (ImageButton) view.findViewById(
                R.id.activity_main_new_task_fragment_due_time_clear_button);
        tvRepeat = (TextView) view.findViewById(R.id.activity_main_new_task_fragment_repeat_tv);
        spinnerRepeatFrequency = (Spinner) view.findViewById(R.id.activity_main_new_task_fragment_repeat_spinner);
        spinnerRepeatFrequency.setAdapter(new ArrayAdapter<>((MainActivity) parentView,
                android.R.layout.simple_list_item_1, TaskRepeatFrequencies.values()));

        Log.d(TAG, "<< [initItems]");
    }

    /**
     * Sets event listeners for fragment items
     */
    protected void setItemsEventsListeners(){
        Log.d(TAG, ">> [setItemsEventsListeners]");

        // Task content edit text listeners.

        // To hide keyboard when edit text is not focused.
        edTaskContent.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (!hasFocus) {
                    InputMethodManager inputMethodManager =
                            (InputMethodManager)((MainActivity) parentView).getSystemService(
                                    Activity.INPUT_METHOD_SERVICE);

                            if (inputMethodManager != null) {
                                inputMethodManager.hideSoftInputFromWindow(v.getWindowToken(), 0);
                            }
                }
            }
        });

        // Due date edit text listeners.

        edDueDate.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                datePickerDialog = new DatePickerDialog((MainActivity) parentView, R.style.DatePickerTheme,
                        new DatePickerDialog.OnDateSetListener() {

                            @Override
                            public void onDateSet(DatePicker view, int year,
                                                  int monthOfYear, int dayOfMonth){
                                calendar.set(year, monthOfYear, dayOfMonth);
                                dateFormatter = new SimpleDateFormat(
                                        Constants.DATE_FORMAT_FOR_USER, Locale.getDefault());
                                edDueDate.setText(dateFormatter.format(calendar.getTime()));
                            }
                        }, calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH),
                        calendar.get(Calendar.DAY_OF_MONTH));
                datePickerDialog.show();
            }
        });

        edDueDate.addTextChangedListener(new TextWatcher(){

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                presenter.isDateValid(edDueDate.getText().toString());

                // Show due time edit text and repeat spinner only if due date is selected.
                if(!edDueDate.getText().toString().isEmpty()){
                    btnDueDateClear.setVisibility(View.VISIBLE);
                    edDueTime.setVisibility(View.VISIBLE);
                    tvRepeat.setVisibility(View.VISIBLE);
                    spinnerRepeatFrequency.setVisibility(View.VISIBLE);
                }
                // Hide due time edit text and repeat spinner and zeroing it's values if due
                // date is empty.
                else {
                    btnDueDateClear.setVisibility(View.GONE);
                    btnDueTimeClear.setVisibility(View.GONE);
                    edDueTime.setVisibility(View.GONE);
                    tvRepeat.setVisibility(View.GONE);
                    spinnerRepeatFrequency.setVisibility(View.GONE);
                    showDueDateEditTextError(false);
                    showDueTimeEditTextError(false);
                    spinnerRepeatFrequency.setSelection(0);
                    edDueTime.setText("");
                }
            }
        });


        // Due date clear button listeners.

        btnDueDateClear.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                edDueDate.setText("");
            }
        });


        // Due time edit text listeners.

        edDueTime.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                timePickerDialog = new TimePickerDialog((MainActivity) parentView, R.style.TimePickerTheme,
                        new TimePickerDialog.OnTimeSetListener() {

                            @Override
                            public void onTimeSet(TimePicker timePicker, int selectedHour,
                                                  int selectedMinute){
                                dateFormatter = new SimpleDateFormat(
                                        Constants.EXPIRATION_TIME_FORMAT, Locale.getDefault());

                                calendar.set(Calendar.HOUR_OF_DAY, selectedHour);
                                calendar.set(Calendar.MINUTE, selectedMinute);

                                edDueTime.setText(dateFormatter.format(calendar.getTime()));
                            }
                        }, calendar.get(Calendar.HOUR_OF_DAY), calendar.get(Calendar.MINUTE), true);
                timePickerDialog.show();
            }
        });

        edDueTime.addTextChangedListener(new TextWatcher(){

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                presenter.isTimeValid(edDueDate.getText().toString(), edDueTime.getText().toString());

                if(!edDueTime.getText().toString().isEmpty()){
                    btnDueTimeClear.setVisibility(View.VISIBLE);
                }
                else {
                    btnDueTimeClear.setVisibility(View.GONE);
                    showDueTimeEditTextError(false);
                }
            }
        });


        // Due time clear button listeners.

        btnDueTimeClear.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                edDueTime.setText("");
            }
        });

        Log.d(TAG, "<< [setItemsEventsListeners]");
    }
}
