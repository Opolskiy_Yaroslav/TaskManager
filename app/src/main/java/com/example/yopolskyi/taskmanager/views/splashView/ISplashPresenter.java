package com.example.yopolskyi.taskmanager.views.splashView;

import java.util.Date;

public interface ISplashPresenter {
    void processTokensAndLaunchNextActivity(String accessToken, Date accessTokenExpirationDate, String refreshToken,
                                            Date refreshTokenExpirationDate);
}
