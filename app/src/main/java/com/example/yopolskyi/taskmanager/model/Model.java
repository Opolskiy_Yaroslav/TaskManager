package com.example.yopolskyi.taskmanager.model;

import android.util.Log;

import com.example.yopolskyi.taskmanager.common.AuthorizationToken;
import com.example.yopolskyi.taskmanager.common.Constants;
import com.example.yopolskyi.taskmanager.common.ResultObject;
import com.example.yopolskyi.taskmanager.common.Task;
import com.example.yopolskyi.taskmanager.common.TokensCombination;
import com.example.yopolskyi.taskmanager.common.Utils;
import com.example.yopolskyi.taskmanager.common.dependencyInjection.main.MainComponentAssistant;
import com.example.yopolskyi.taskmanager.common.enumerations.AuthorizationTokenTypes;
import com.example.yopolskyi.taskmanager.common.enumerations.ResultMessage;
import com.example.yopolskyi.taskmanager.common.enumerations.TaskExecutionStatuses;
import com.example.yopolskyi.taskmanager.common.enumerations.TaskRepeatFrequencies;
import com.example.yopolskyi.taskmanager.model.dal.IRepository;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import javax.inject.Inject;

/**
 * Represents application model. Contains main application logic.
 */
public class Model implements IModel {

    @Inject
    IRepository repository;

    // For logging.
    private final String TAG = "Model";


    public Model(){
        MainComponentAssistant.getComponent().injectModel(this);
    }

    @Override
    public void setRepository(IRepository repository) {
        this.repository = repository;
    }

    /**
     * Calls repository method to check username-password combination exiting.
     * @param username entered username
     * @param password entered password
     * @return ResultMessage.SUCCESSFUL if successful, ResultMessage.DATABASE_ERROR in another
     * case as ResultMessage; user ID if combination was founded or "-1" in other case as resultValue
     */
    @Override
    public ResultObject doLogin(String username, String password){
        Log.d(TAG, ">> [doLogin]");
        Log.d(TAG, "Input arguments:" + " Username = " + username + " Password = " + password);

        Log.d(TAG, "Calling the repository getUser method.");

        int userId;
        try{
            userId = repository.getUser(username, password);
        }
        catch (Exception e){
            Log.e(TAG, "Could not log in. " + e.toString());
            return new ResultObject(ResultMessage.DATABASE_ERROR, null);
        }

        Log.d(TAG, "Returned user ID = " + userId);

        Log.d(TAG, "<< [doLogin]");
        return new ResultObject(ResultMessage.SUCCESSFUL, userId);
    }

    /**
     * Calls repository getUserIdByAccessToken method.
     * @param accessToken search criterion
     * @return ResultMessage.SUCCESSFUL if successful, ResultMessage.DATABASE_ERROR in another
     * case as ResultMessage; user ID if user was founded or "-1" in other case as resultValue
     */
    @Override
    public ResultObject getUserIdByAccessToken(String accessToken){
        Log.d(TAG, ">> [getUserIdByAccessToken]");
        Log.d(TAG, "Input arguments:" + " Access token = " + accessToken);

        int userId;
        try{
            userId = repository.getUserIdByAccessToken(accessToken);
        }
        catch (Exception e){
            Log.e(TAG, "Could not get user ID. " + e.toString());
            return new ResultObject(ResultMessage.DATABASE_ERROR, null);
        }

        Log.d(TAG, "Returned user ID = " + userId);

        Log.d(TAG, "<< [getUserIdByAccessToken]");
        return new ResultObject(ResultMessage.SUCCESSFUL, userId);
    }

    /**
     * Calls repository getUsernameByUserId method.
     * @return ResultMessage.SUCCESSFUL if successful, ResultMessage.DATABASE_ERROR in another
     * case as ResultMessage; username if user was founded or "null" in other case as resultValue
     */
    @Override
    public ResultObject getUsernameByUserId(int userID){
        Log.d(TAG, ">> [getUserIdByAccessToken]");
        Log.d(TAG, "Input arguments:" + " User ID = " + userID);

        String username;
        try{
            username = repository.getUsernameByUserId(userID);
        }
        catch (Exception e){
            Log.e(TAG, "Could not get user ID. " + e.toString());
            return new ResultObject(ResultMessage.DATABASE_ERROR, null);
        }

        Log.d(TAG, "Returned username = " + username);

        Log.d(TAG, "<< [getUserIdByAccessToken]");
        return new ResultObject(ResultMessage.SUCCESSFUL, username);
    }

    /**
     * Generates new tokens for current user.
     * @param userId current user
     * @return created tokens combination
     */
    @Override
    public TokensCombination generateTokens(int userId){
        Log.d(TAG, ">> [generateTokens]");
        Log.d(TAG, "Input argument: " + "User ID = " + userId);

        // Tokens generating.
        Log.d(TAG, "Try to generate tokens.");

        String sCurrentDate = Utils.convertDateToString(new Date(), Constants.TOKENS_DATE_FORMAT);

        String accessTokenValue = userId + "_" + sCurrentDate;
        String refreshTokenValue = userId + "_" + sCurrentDate;

        Log.d(TAG, "Access token = " + accessTokenValue);
        Log.d(TAG, "Refresh token = " + refreshTokenValue);

        // Expiration dates generating.
        Log.d(TAG, "Try to generate tokens expiration dates.");

        // Calendar.getInstance() returns a calendar with current date.
        Calendar calendar = Calendar.getInstance();
        calendar.add(Calendar.DATE, Constants.ACCESS_TOKEN_VALIDITY_IN_DAYS);
        Date accessTokenExpirationDate = calendar.getTime();

        calendar = Calendar.getInstance();
        calendar.add(Calendar.DATE, Constants.REFRESH_TOKEN_VALIDITY_IN_DAYS);
        Date refreshTokenExpirationDate = calendar.getTime();

        Log.d(TAG, "Access token exp date = " + Utils.convertDateToString(accessTokenExpirationDate,
                Constants.TOKENS_DATE_FORMAT));
        Log.d(TAG, "Refresh token exp date = " + Utils.convertDateToString(refreshTokenExpirationDate,
                Constants.TOKENS_DATE_FORMAT));

        AuthorizationToken accessToken = new AuthorizationToken(userId, accessTokenValue,
                accessTokenExpirationDate, AuthorizationTokenTypes.Access);
        AuthorizationToken refreshToken = new AuthorizationToken(userId, refreshTokenValue,
                refreshTokenExpirationDate, AuthorizationTokenTypes.Refresh);

        Log.d(TAG, "Returned tokens:");
        Log.d(TAG, accessToken.toString());
        Log.d(TAG, refreshToken.toString());

        Log.d(TAG, "<< [generateTokens]");
        return new TokensCombination(accessToken, refreshToken);
    }

    /**
     * Calls repository addTokens method.
     * @param tokensCombination tokens combination that must be added to the storage
     * @return ResultMessage.SUCCESSFUL if successful, ResultMessage.DATABASE_ERROR in another
     * case as ResultMessage;
     */
    @Override
    public ResultObject addTokens(TokensCombination tokensCombination) {
        Log.d(TAG, ">> [addTokens]");
        Log.d(TAG, "Input argument: ");
        Log.d(TAG, "Access token = " + tokensCombination.getAccessToken().toString());
        Log.d(TAG, " Refresh token = " + tokensCombination.getRefreshToken().toString());

        Log.d(TAG, "Calls repository addTokens method.");

        try{
            repository.addTokens(tokensCombination);
        }
        catch (Exception e){
            Log.e(TAG, "Could not add token to the storage. " + e.toString());
            return new ResultObject(ResultMessage.DATABASE_ERROR, null);
        }

        Log.d(TAG, "Completed.");
        Log.d(TAG, "<< [addTokens]");

        return new ResultObject(ResultMessage.SUCCESSFUL, null);
    }

    /**
     * Calls repository methods to generate new tokens and update it in the storage.
     * @return ResultMessage.SUCCESSFUL if successful, ResultMessage.DATABASE_ERROR in another
     * case as ResultMessage; generated combination of access and refresh tokens as resultValue
     */
    @Override
    public ResultObject refreshTokens(int userId){
        Log.d(TAG, ">> [refreshTokens]");
        Log.d(TAG, "Input argument: " + "User ID = " + userId);

        Log.d(TAG, "Calls generateTokens method.");
        TokensCombination tokensCombination = generateTokens(userId);

        Log.d(TAG, "Calls updateTokens method.");
        try {
            repository.updateTokens(userId, tokensCombination);
        }
        catch (Exception e){
            Log.e(TAG, "Could not refresh tokens in the storage. " + e.toString());
            return new ResultObject(ResultMessage.DATABASE_ERROR, null);
        }

        Log.d(TAG, "Refreshed tokens:");
        Log.d(TAG, "Access token: " + tokensCombination.getAccessToken());
        Log.d(TAG, "Refreshed tokens: " + tokensCombination.getRefreshToken());

        Log.d(TAG, "<< [refreshTokens]");
        return new ResultObject(ResultMessage.SUCCESSFUL, tokensCombination);
    }

    /**
     * Calls repository deleteTokens method
     * @return ResultMessage.SUCCESSFUL if successful, ResultMessage.DATABASE_ERROR in another
     * case as ResultMessage;
     */
    @Override
    public ResultObject deleteTokens(int userId) {
        Log.d(TAG, ">> [deleteTokens]");
        Log.d(TAG, "Input argument: " + "User ID = " + userId);

        Log.d(TAG, "Calls repository removeTokens method.");
        try{
            repository.removeTokens(userId);
        }
        catch (Exception e){
            Log.e(TAG, "Could not delete tokens from the storage. " + e.toString());
            return new ResultObject(ResultMessage.DATABASE_ERROR, null);
        }

        Log.d(TAG, "<< [deleteTokens]");
        return new ResultObject(ResultMessage.SUCCESSFUL, null);
    }

    /**
     * Calls repository addTask method
     * @param task task that must be added
     * @return ResultMessage.SUCCESSFUL if successful, ResultMessage.DATABASE_ERROR in another
     * case as ResultMessage and task id as ResultValue;
     */
    @Override
    public ResultObject addTask(Task task) {
        Log.d(TAG, ">> [addTask]");
        Log.d(TAG, "Input argument:");
        Log.d(TAG, task.toString());

        Log.d(TAG, "Calls repository addTask method.");
        long taskId;
        try{
            taskId = repository.addTask(task);
        }
        catch (Exception e){
            Log.e(TAG, "Could not add task to the storage. " + e.toString());
            return new ResultObject(ResultMessage.DATABASE_ERROR, null);
        }

        Log.d(TAG, "<< [addTask]");
        return new ResultObject(ResultMessage.SUCCESSFUL, taskId);
    }

    /**
     * Calls the repository getTask method
     * @param taskId criterion for searching
     * @param accessToken current user access token
     * @return ResultMessage.SUCCESSFUL if successful, ResultMessage.DATABASE_ERROR in another
     * case as ResultMessage; task with the specified ID as resultValue
     */
    @Override
    public ResultObject getTask(int taskId, String accessToken) {
        Log.d(TAG, ">> [getTask]");
        Log.d(TAG, "Input argument:" + " Task ID = " + taskId + " Access token = " + accessToken);

        Log.d(TAG, "Calls repository getUserIdByAccessToken method.");
        int userId;
        try{
           userId = repository.getUserIdByAccessToken(accessToken);
        }
        catch (Exception e){
            Log.e(TAG, "Could not get user ID from the storage. " + e.toString());
            return new ResultObject(ResultMessage.DATABASE_ERROR, null);
        }
        Log.d(TAG, "User ID = " + userId);


        Log.d(TAG, "Calls repository getTaskByTaskId method.");
        Task task;
        try{
            task = repository.getTaskByTaskId(taskId, userId);
        }
        catch (Exception e){
            Log.e(TAG, "Could not get task from the storage. " + e.toString());
            return new ResultObject(ResultMessage.DATABASE_ERROR, null);
        }


        Log.d(TAG, "Returned task:" + task);

        Log.d(TAG, "<< [getTask]");
        return new ResultObject(ResultMessage.SUCCESSFUL, task);
    }

    /**
     * Calls the repository getTasks method
     * @param accessToken current user access token
     * @return ResultMessage.SUCCESSFUL if successful, ResultMessage.DATABASE_ERROR in another
     * case as ResultMessage; list of all task for current user as resultValue
     */
    @Override
    public ResultObject getTasks(String accessToken){
        Log.d(TAG, ">> [getTasks]");
        Log.d(TAG, "Input argument:" + " Access token = " + accessToken);


        Log.d(TAG, "Calls repository getUserIdByAccessToken method.");
        int userId;
        try{
            userId = repository.getUserIdByAccessToken(accessToken);
        }
        catch (Exception e){
            Log.e(TAG, "Could not get user ID from the storage. " + e.toString());
            return new ResultObject(ResultMessage.DATABASE_ERROR, null);
        }
        Log.d(TAG, "User ID = " + userId);


        Log.d(TAG, "Calls repository getTasks method.");
        List<Task> taskList;
        try{
            taskList = repository.getTasks(userId);
        }
        catch (Exception e){
            Log.e(TAG, "Could not get task list from the storage. " + e.toString());
            return new ResultObject(ResultMessage.DATABASE_ERROR, null);
        }


        Log.d(TAG, "Returned task list:");
        for(Task task : taskList){
            Log.d(TAG, task.toString());
        }

        Log.d(TAG, "<< [getTasks]");
        return new ResultObject(ResultMessage.SUCCESSFUL, taskList);
    }

    /**
     * Calls the repository getTasksByContent method
     * @param criterion criterion for searching by content
     * @param accessToken current user access token
     * @return ResultMessage.SUCCESSFUL if successful, ResultMessage.DATABASE_ERROR in another
     * case as ResultMessage; list of task with the content fields that contains criterion string
     * as resultValue
     */
    @Override
    public ResultObject getTasks(String criterion, String accessToken){
        Log.d(TAG, ">> [getTasks]");
        Log.d(TAG, "Input arguments:"
                + " Criterion = " + criterion
                + " Access token = " + accessToken);


        Log.d(TAG, "Calls repository getUserIdByAccessToken method.");
        int userId;
        try{
            userId = repository.getUserIdByAccessToken(accessToken);
        }
        catch (Exception e){
            Log.e(TAG, "Could not get user ID from the storage. " + e.toString());
            return new ResultObject(ResultMessage.DATABASE_ERROR, null);
        }
        Log.d(TAG, "User ID = " + userId);


        Log.d(TAG, "Calls repository getTasksByContent method.");
        List<Task> taskList;
        try{
            taskList = repository.getTasksByContent(criterion, userId);
        }
        catch (Exception e){
            Log.e(TAG, "Could not get task list from the storage. " + e.toString());
            return new ResultObject(ResultMessage.DATABASE_ERROR, null);
        }


        Log.d(TAG, "Returned list:");
        for(Task task : taskList){
            Log.d(TAG, task.toString());
        }

        Log.d(TAG, "<< [getTasks]");
        return new ResultObject(ResultMessage.SUCCESSFUL, taskList);
    }

    /**
     * Calls the repository getTasksByStatus method
     * @param executionStatus criterion for searching by execution status
     * @param accessToken current user access token
     * @return ResultMessage.SUCCESSFUL if successful, ResultMessage.DATABASE_ERROR in another
     * case as ResultMessage; list of task with specified execution status
     * as resultValue
     */
    @Override
    public ResultObject getTasks(TaskExecutionStatuses executionStatus, String accessToken){
        Log.d(TAG, ">> [getTasks]");
        Log.d(TAG, "Input arguments:"
                + " Execution status = " + executionStatus
                + " Access token = " + accessToken);


        Log.d(TAG, "Calls repository getUserIdByAccessToken method.");
        int userId;
        try{
            userId = repository.getUserIdByAccessToken(accessToken);
        }
        catch (Exception e){
            Log.e(TAG, "Could not get user ID from the storage. " + e.toString());
            return new ResultObject(ResultMessage.DATABASE_ERROR, null);
        }
        Log.d(TAG, "User ID = " + userId);


        Log.d(TAG, "Calls repository getTasksByExecutionStatus method.");
        List<Task> taskList;
        try{
            taskList = repository.getTasksByExecutionStatus(executionStatus, userId);
        }
        catch (Exception e){
            Log.e(TAG, "Could not get task list from the storage. " + e.toString());
            return new ResultObject(ResultMessage.DATABASE_ERROR, null);
        }


        Log.d(TAG, "Returned list:");
        for(Task task : taskList){
            Log.d(TAG, task.toString());
        }

        Log.d(TAG, "<< [getTasks]");
        return new ResultObject(ResultMessage.SUCCESSFUL, taskList);
    }

    /**
     * Calls the repository updateTask method
     * @param taskId id of the task that must be updated
     * @param task new task for replacing
     * @param accessToken current user access token
     * @return ResultMessage.SUCCESSFUL if successful, ResultMessage.DATABASE_ERROR in another
     * case as ResultMessage
     */
    @Override
    public ResultObject updateTask(int taskId, Task task, String accessToken) {
        Log.d(TAG, ">> [updateTask]");
        Log.d(TAG, "Input arguments:"
                + " Task ID = " + taskId
                + " Task = " + task.toString()
                + " Access token = " + accessToken);


        Log.d(TAG, "Calls repository getUserIdByAccessToken method.");
        int userId;
        try{
            userId = repository.getUserIdByAccessToken(accessToken);
        }
        catch (Exception e){
            Log.e(TAG, "Could not get user ID from the storage. " + e.toString());
            return new ResultObject(ResultMessage.DATABASE_ERROR, null);
        }
        Log.d(TAG, "User ID = " + userId);


        Log.d(TAG, "Calls repository update method.");
        try{
            repository.updateTask(taskId, task, userId);
        }
        catch (Exception e){
            Log.e(TAG, "Could not update task list the storage. " + e.toString());
            return new ResultObject(ResultMessage.DATABASE_ERROR, null);
        }

        Log.d(TAG, "<< [updateTasks]");
        return new ResultObject(ResultMessage.SUCCESSFUL, null);
    }

    /**
     * Calls the repository getTasks method, verifies the tasks due dates with current date and
     * updates tasks execution status to "Overdue" if date is past due
     * @param accessToken current user accessToken
     * @return ResultMessage.SUCCESSFUL if successful, ResultMessage.DATABASE_ERROR in another
     * case as ResultMessage
     */
    @Override
    public ResultObject refreshTaskStatuses(String accessToken) {
        Log.d(TAG, ">> [refreshTaskStatuses]");
        Log.d(TAG, "Input argument: " + " Access token = " + accessToken);

        // Get user ID.
        Log.d(TAG, "Calls repository getUserIdByAccessToken method.");
        int userId;
        try{
            userId = repository.getUserIdByAccessToken(accessToken);
        }
        catch (Exception e){
            Log.e(TAG, "Could not get user ID from the storage. " + e.toString());
            return new ResultObject(ResultMessage.DATABASE_ERROR, null);
        }
        Log.d(TAG, "User ID = " + userId);


        // Get all tasks for current user.
        Log.d(TAG, "Calls repository getTasks method.");
        List<Task> taskList;
        try{
            taskList = repository.getTasks(userId);
        }
        catch (Exception e){
            Log.e(TAG, "Could not get task list from the storage. " + e.toString());
            return new ResultObject(ResultMessage.DATABASE_ERROR, null);
        }


        Calendar calendar = Calendar.getInstance();
        Date currentDate = calendar.getTime();

        DateFormat dateFormat = new SimpleDateFormat(Constants.EXPIRATION_DATE_FORMAT,
                Locale.getDefault());
        DateFormat timeFormat = new SimpleDateFormat(Constants.EXPIRATION_TIME_FORMAT,
                Locale.getDefault());

        for(Task task : taskList){

            // If task date does not specified - ignore this task.
            if(task.getDueDate() == null){
                continue;
            }

            // If "In Progress".
            if(task.getTaskExecutionStatus() == TaskExecutionStatuses.inProgress){

                // If due date is equal or less than current.
                if(dateFormat.format(task.getDueDate()).compareTo(dateFormat.format(currentDate)) <= 0 ){

                    // If due date and current date is equal - check time.
                    if(dateFormat.format(task.getDueDate()).compareTo(dateFormat.format(currentDate)) == 0 ){
                        if(task.getDueTime() == null){
                            continue;
                        }
                        // If dates is equal but due time is less than current time.
                        else if(timeFormat.format(task.getDueTime()).compareTo(timeFormat.format(currentDate)) <= 0 ){
                            task.setTaskExecutionStatus(TaskExecutionStatuses.overdue);
                        }
                    }
                    // If due date is less than current date.
                    else{
                        task.setTaskExecutionStatus(TaskExecutionStatuses.overdue);
                    }
                }
            }
            // If "Overdue".
            else if(task.getTaskExecutionStatus() == TaskExecutionStatuses.overdue){
                if(dateFormat.format(task.getDueDate()).compareTo(dateFormat.format(currentDate)) >= 0 ){

                    // If due date and current date is equal - check time.
                    if(dateFormat.format(task.getDueDate()).compareTo(dateFormat.format(currentDate)) == 0 ){
                        if(task.getDueTime() == null){
                            continue;
                        }
                        // If dates is equal but due time is after than current time.
                        else if(timeFormat.format(task.getDueTime()).compareTo(timeFormat.format(currentDate)) > 0 ){
                            task.setTaskExecutionStatus(TaskExecutionStatuses.inProgress);
                        }
                    }
                    // If due date is after than current date.
                    else{
                        task.setTaskExecutionStatus(TaskExecutionStatuses.inProgress);
                    }
                }
            }


            Log.d(TAG, "Calls repository update method.");
            try{
                repository.updateTask(task.getId(), task, userId);
            }
            catch (Exception e){
                Log.e(TAG, "Could not update task list the storage. " + e.toString());
                return new ResultObject(ResultMessage.DATABASE_ERROR, null);
            }
        }

        Log.d(TAG, "<< [refreshTaskStatuses]");
        return new ResultObject(ResultMessage.SUCCESSFUL, null);
    }

    /**
     * Calls the refreshRepeatableTaskDueDates for all current user tasks.
     * @param accessToken current user access token
     * @return ResultMessage.SUCCESSFUL if successful, ResultMessage.DATABASE_ERROR in another
     * case as ResultMessage
     */
    @Override
    public ResultObject refreshRepeatableTaskDueDates(String accessToken) {
        Log.d(TAG, ">> [refreshRepeatableTaskDueDates]");
        Log.d(TAG, "Input argument: " + " Access token = " + accessToken);

        // Get user ID.
        Log.d(TAG, "Calls repository getUserIdByAccessToken method.");
        int userId;
        try{
            userId = repository.getUserIdByAccessToken(accessToken);
        }
        catch (Exception e){
            Log.e(TAG, "Could not get user ID from the storage. " + e.toString());
            return new ResultObject(ResultMessage.DATABASE_ERROR, null);
        }
        Log.d(TAG, "User ID = " + userId);


        // Get all tasks for current user.
        Log.d(TAG, "Calls repository getTasks method.");
        List<Task> taskList;
        try{
            taskList = repository.getTasks(userId);
        }
        catch (Exception e){
            Log.e(TAG, "Could not get task list from the storage. " + e.toString());
            return new ResultObject(ResultMessage.DATABASE_ERROR, null);
        }

        Log.d(TAG, "Calling the model refreshRepeatableTaskDueDates method for each task.");
        for(Task task : taskList){
            ResultObject refreshRepeatableTaskDueDateResult = refreshRepeatableTaskDueDate(
                    task.getId(), accessToken);
            if(refreshRepeatableTaskDueDateResult.getResultMessage() != ResultMessage.SUCCESSFUL){
                return new ResultObject(ResultMessage.DATABASE_ERROR, null);
            }
        }

        Log.d(TAG, "<< [refreshRepeatableTaskDueDates]");
        return new ResultObject(ResultMessage.SUCCESSFUL, null);
    }

    /**
     * Extends the due date for overdue repeatable tasks (if task is repeatable and overdue -
     * makes it inProgress and adds repeat interval to due date).
     * @param taskId task ID
     * @param accessToken current user access token
     * @return ResultMessage.SUCCESSFUL if successful, ResultMessage.DATABASE_ERROR in another
     * case as ResultMessage
     */
    @Override
    public ResultObject refreshRepeatableTaskDueDate(int taskId, String accessToken) {
        Log.d(TAG, ">> [refreshRepeatableTaskDueDates]");
        Log.d(TAG, "Input argument: " + "Task ID = " + taskId + " Access token = " + accessToken);

        // Get user ID.
        Log.d(TAG, "Calls repository getUserIdByAccessToken method.");
        int userId;
        try{
            userId = repository.getUserIdByAccessToken(accessToken);
        }
        catch (Exception e){
            Log.e(TAG, "Could not get user ID from the storage. " + e.toString());
            return new ResultObject(ResultMessage.DATABASE_ERROR, null);
        }
        Log.d(TAG, "User ID = " + userId);


        // Get task by task ID for current user.
        Log.d(TAG, "Calls repository getTaskByTaskId method.");
        Task task;
        try{
            task = repository.getTaskByTaskId(taskId, userId);
        }
        catch (Exception e){
            Log.e(TAG, "Could not get task from the storage. " + e.toString());
            return new ResultObject(ResultMessage.DATABASE_ERROR, null);
        }

        // If task is not repeatable.
        if(task.getRepeatFrequency() == TaskRepeatFrequencies.noRepeat){
            return new ResultObject(ResultMessage.SUCCESSFUL, null);
        }

        // If task date does not specified - ignore this task.
        if(task.getDueDate() == null){
            return new ResultObject(ResultMessage.SUCCESSFUL, null);
        }


        Calendar taskDueDateCalendar = Calendar.getInstance();
        taskDueDateCalendar.setTime(task.getDueDate());
        Calendar currentDateCalendar = Calendar.getInstance();

        DateFormat dateFormat = new SimpleDateFormat(Constants.EXPIRATION_DATE_FORMAT,
                Locale.getDefault());
        DateFormat timeFormat = new SimpleDateFormat(Constants.EXPIRATION_TIME_FORMAT,
                Locale.getDefault());


        Log.d(TAG, "Current task due date: " + task.getDueDate());

        // If due date is equal or less than current.
        if(dateFormat.format(task.getDueDate()).compareTo(dateFormat.format(currentDateCalendar.getTime())) <= 0 ){

            // If due date and current date is equal - check time.
            if(dateFormat.format(task.getDueDate()).compareTo(dateFormat.format(currentDateCalendar.getTime())) == 0 ){
                if(task.getDueTime() != null){
                    // If dates is equal but due time is less than current time.
                    if(timeFormat.format(task.getDueTime()).compareTo(timeFormat.format(currentDateCalendar.getTime())) <= 0 ){
                        switch (task.getRepeatFrequency()){
                            case day:
                                taskDueDateCalendar.add(Calendar.DAY_OF_WEEK, 1);
                                break;
                            case week:
                                taskDueDateCalendar.add(Calendar.WEEK_OF_MONTH, 1);
                                break;
                            case month:
                                taskDueDateCalendar.add(Calendar.MONTH, 1);
                                break;
                            case year:
                                taskDueDateCalendar.add(Calendar.YEAR, 1);
                                break;
                        }
                    }
                }
            }
            // If due date is less than current date.
            else{
                switch (task.getRepeatFrequency()){
                    case day:
                        taskDueDateCalendar.add(Calendar.DAY_OF_WEEK, 1);
                        break;
                    case week:
                        taskDueDateCalendar.add(Calendar.WEEK_OF_MONTH, 1);
                        break;
                    case month:
                        taskDueDateCalendar.add(Calendar.MONTH, 1);
                        break;
                    case year:
                        taskDueDateCalendar.add(Calendar.YEAR, 1);
                        break;
                }
            }
        }

        task.setDueDate(taskDueDateCalendar.getTime());

        Log.d(TAG, "New due date: " + task.getDueDate());

        // Try to update task in the storage.
        Log.d(TAG, "Calls repository updateTask method.");
        try{
            repository.updateTask(taskId, task, userId);
        }
        catch (Exception e){
            Log.e(TAG, "Could not update task list the storage. " + e.toString());
            return new ResultObject(ResultMessage.DATABASE_ERROR, null);
        }

        return new ResultObject(ResultMessage.SUCCESSFUL, null);
    }

    /**
     * Extends due date of the repeatable tasks.
     * @param taskId task ID
     * @param accessToken current user access token
     * @return ResultMessage.SUCCESSFUL if successful, ResultMessage.DATABASE_ERROR in another
     * case as ResultMessage
     */
    @Override
    public ResultObject extendRepeatableTaskDueDate(int taskId, String accessToken){
        Log.d(TAG, ">> [refreshRepeatableTaskDueDates]");
        Log.d(TAG, "Input argument: " + "Task ID = " + taskId + " Access token = " + accessToken);

        // Get user ID.
        Log.d(TAG, "Calls repository getUserIdByAccessToken method.");
        int userId;
        try{
            userId = repository.getUserIdByAccessToken(accessToken);
        }
        catch (Exception e){
            Log.e(TAG, "Could not get user ID from the storage. " + e.toString());
            return new ResultObject(ResultMessage.DATABASE_ERROR, null);
        }
        Log.d(TAG, "User ID = " + userId);


        // Get task by task ID for current user.
        Log.d(TAG, "Calls repository getTaskByTaskId method.");
        Task task;
        try{
            task = repository.getTaskByTaskId(taskId, userId);
        }
        catch (Exception e){
            Log.e(TAG, "Could not get task from the storage. " + e.toString());
            return new ResultObject(ResultMessage.DATABASE_ERROR, null);
        }

        // If task is not repeatable.
        if(task.getRepeatFrequency() == TaskRepeatFrequencies.noRepeat){
            return new ResultObject(ResultMessage.SUCCESSFUL, null);
        }

        // If task date does not specified - ignore this task.
        if(task.getDueDate() == null){
            return new ResultObject(ResultMessage.SUCCESSFUL, null);
        }


        Calendar taskDueDateCalendar = Calendar.getInstance();
        taskDueDateCalendar.setTime(task.getDueDate());

        Log.d(TAG, "Current task due date: " + task.getDueDate());

        switch (task.getRepeatFrequency()){
            case day:
                taskDueDateCalendar.add(Calendar.DAY_OF_WEEK, 1);
                break;
            case week:
                taskDueDateCalendar.add(Calendar.WEEK_OF_MONTH, 1);
                break;
            case month:
                taskDueDateCalendar.add(Calendar.MONTH, 1);
                break;
            case year:
                taskDueDateCalendar.add(Calendar.YEAR, 1);
                break;
        }
        task.setDueDate(taskDueDateCalendar.getTime());

        Log.d(TAG, "New due date: " + task.getDueDate());

        // Try to update task in the storage.
        Log.d(TAG, "Calls repository updateTask method.");
        try{
            repository.updateTask(taskId, task, userId);
        }
        catch (Exception e){
            Log.e(TAG, "Could not update task list the storage. " + e.toString());
            return new ResultObject(ResultMessage.DATABASE_ERROR, null);
        }

        return new ResultObject(ResultMessage.SUCCESSFUL, null);
    }

    /**
     * Calls the repository removeTask method
     * @param taskId id of the task that must be deleted
     * @param accessToken current user access token
     */
    @Override
    public ResultObject removeTask(int taskId, String accessToken) {
        Log.d(TAG, ">> [removeTask]");
        Log.d(TAG, "Input arguments:"
                + " Task ID = " + taskId
                + " Access token = " + accessToken);


        Log.d(TAG, "Calls repository getUserIdByAccessToken method.");
        int userId;
        try{
            userId = repository.getUserIdByAccessToken(accessToken);
        }
        catch (Exception e){
            Log.e(TAG, "Could not get user ID from the storage. " + e.toString());
            return new ResultObject(ResultMessage.DATABASE_ERROR, null);
        }
        Log.d(TAG, "User ID = " + userId);


        Log.d(TAG, "Calls repository removeTask method.");
        try{
            repository.removeTask(taskId, userId);
        }
        catch (Exception e){
            Log.e(TAG, "Could not remove task from the storage. " + e.toString());
            return new ResultObject(ResultMessage.DATABASE_ERROR, null);
        }

        Log.d(TAG, "<< [removeTask]");
        return new ResultObject(ResultMessage.SUCCESSFUL, null);
    }
}