package com.example.yopolskyi.taskmanager.views.mainView;

public enum ToolbarMenuStates {
    // Common toolbar menu state. Contains title, spinner and search button.
    common,
    // Search toolbar menu state. Contains search field, clear and back buttons.
    search,
    // Context toolbar menu state. Contains edit task, remove task and back buttons.
    context,
    // Edit toolbar menu state. Contains confirm and back buttons.
    edit,
    // Settings toolbar menu state. Contains only back button.
    settings
}
