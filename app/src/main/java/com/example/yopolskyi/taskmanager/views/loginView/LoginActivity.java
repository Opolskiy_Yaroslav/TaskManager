package com.example.yopolskyi.taskmanager.views.loginView;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.yopolskyi.taskmanager.App;
import com.example.yopolskyi.taskmanager.R;
import com.example.yopolskyi.taskmanager.common.PreferencesAssistant;
import com.example.yopolskyi.taskmanager.common.TokensCombination;
import com.example.yopolskyi.taskmanager.common.dependencyInjection.loginActivity.DaggerLoginActivityComponent;
import com.example.yopolskyi.taskmanager.common.dependencyInjection.loginActivity.LoginActivityModule;
import com.example.yopolskyi.taskmanager.common.dependencyInjection.main.MainComponentAssistant;
import com.example.yopolskyi.taskmanager.views.mainView.MainActivity;

import javax.inject.Inject;

public class LoginActivity extends AppCompatActivity implements ILoginView {

    // For logging.
    private final String TAG = "LoginActivity";

    @Inject
    ILoginPresenter presenter;

    @Inject
    PreferencesAssistant preferencesAssistant;

    private EditText edUsername;
    private EditText edPassword;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // Hide status bar.
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);

        setContentView(R.layout.activity_login);

        initItems();
    }

    @Override
    protected void onResume() {
        super.onResume();

        App.setActiveActivity(this);
    }

    @Override
    protected void onPause() {
        super.onPause();

        App.setActiveActivity(null);
    }

    private void initItems(){
        // Dependency injection splash activity component instance creating.
        DaggerLoginActivityComponent
                .builder()
                .loginActivityModule(new LoginActivityModule(this))
                .mainComponent(MainComponentAssistant.getComponent())
                .build()
                .injectLoginActivity(this);

        edUsername = (EditText) findViewById(R.id.activity_login_ed_username);
        edPassword = (EditText) findViewById(R.id.activity_login_ed_password);
        final Button btnSignIn = (Button) findViewById(R.id.activity_login_btn_sign_in);

        edPassword.setOnEditorActionListener(new EditText.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_DONE) {
                    btnSignIn.performClick();

                    // Hide a keyboard.
                    InputMethodManager inputMethodManager =
                            (InputMethodManager)getSystemService(
                                    Activity.INPUT_METHOD_SERVICE);
                    inputMethodManager.hideSoftInputFromWindow(v.getWindowToken(), 0);
                    return true;
                }
                return false;
            }
        });

        btnSignIn.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                presenter.doLogin(edUsername.getText().toString(), edPassword.getText().toString());
            }
        });
    }


    @Override
    public void showUsernameEmptyError() {
        Toast.makeText(getBaseContext(), "Username field is empty!", Toast.LENGTH_LONG).show();
    }

    @Override
    public void showPasswordEmptyError() {
        Toast.makeText(getBaseContext(), "Password field is empty!", Toast.LENGTH_LONG).show();
    }

    @Override
    public void showAuthorizationFailedError() {
        Toast.makeText(getBaseContext(), "Incorrect password or username!", Toast.LENGTH_LONG).show();
    }

    @Override
    public void showInternalErrorDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(getString(R.string.internal_error_title))
                .setMessage(getString(R.string.internal_error_message))
                .setCancelable(false)
                .setNegativeButton("Ok",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                finish();
                            }
                        });
        AlertDialog alert = builder.create();
        alert.show();
    }


    @Override
    public void launchMainActivity() {
        finish();
        startActivity(new Intent(getApplicationContext(), MainActivity.class));
    }

    /**
     * Calls preference assistant method to save tokens combination into shared preferences.
     */
    @Override
    public void saveTokensCombination(TokensCombination tokensCombination) {
        Log.d(TAG, ">> [saveTokensCombination]");

        Log.d(TAG, "Calls preference assistance saveTokensCombination method");

        if(preferencesAssistant.getContext() == null){
            preferencesAssistant.setContext(getApplicationContext());
        }

        preferencesAssistant.saveTokensCombination(tokensCombination);

        Log.d(TAG, "<< [saveTokensCombination]");
    }
}
