package com.example.yopolskyi.taskmanager.common.dependencyInjection.mainActivity;

import com.example.yopolskyi.taskmanager.common.dependencyInjection.main.MainComponent;
import com.example.yopolskyi.taskmanager.views.mainView.MainActivity;

import dagger.Component;

@MainActivityScope
@Component(modules = MainActivityModule.class, dependencies = MainComponent.class)
public interface MainActivityComponent {
    void injectMainActivity(MainActivity mainActivity);
}
