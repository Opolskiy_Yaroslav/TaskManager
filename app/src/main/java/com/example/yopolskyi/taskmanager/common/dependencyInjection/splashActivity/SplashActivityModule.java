package com.example.yopolskyi.taskmanager.common.dependencyInjection.splashActivity;

import com.example.yopolskyi.taskmanager.views.splashView.ISplashPresenter;
import com.example.yopolskyi.taskmanager.views.splashView.SplashActivity;
import com.example.yopolskyi.taskmanager.views.splashView.SplashPresenter;

import dagger.Module;
import dagger.Provides;

@Module
public class SplashActivityModule {

    private final SplashActivity splashActivity;

    public SplashActivityModule(SplashActivity splashActivity) {
        this.splashActivity = splashActivity;
    }

    @Provides
    @SplashActivityScope
    ISplashPresenter provideSplashPresenter(){
        return new SplashPresenter(splashActivity);
    }
}
