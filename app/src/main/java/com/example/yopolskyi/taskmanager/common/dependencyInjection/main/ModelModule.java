package com.example.yopolskyi.taskmanager.common.dependencyInjection.main;

import com.example.yopolskyi.taskmanager.model.IModel;
import com.example.yopolskyi.taskmanager.model.Model;

import dagger.Module;
import dagger.Provides;

@Module
public class ModelModule {

    @Provides
    @MainScope
    IModel provideModel(){
        return new Model();
    }
}
