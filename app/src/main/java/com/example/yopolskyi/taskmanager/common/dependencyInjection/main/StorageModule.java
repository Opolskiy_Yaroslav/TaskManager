package com.example.yopolskyi.taskmanager.common.dependencyInjection.main;

import com.example.yopolskyi.taskmanager.common.PreferencesAssistant;
import com.example.yopolskyi.taskmanager.model.dal.DBHelper;
import com.example.yopolskyi.taskmanager.model.dal.IRepository;
import com.example.yopolskyi.taskmanager.model.dal.Repository;

import dagger.Module;
import dagger.Provides;

@Module
public class StorageModule {

    @Provides
    @MainScope
    DBHelper provideDBHelper() {
        return new DBHelper();
    }

    @Provides
    @MainScope
    IRepository provideRepository(){
            return new Repository();
    }

    @Provides
    @MainScope
    PreferencesAssistant providePreferenceAssistant(){
        return new PreferencesAssistant();
    }
}
