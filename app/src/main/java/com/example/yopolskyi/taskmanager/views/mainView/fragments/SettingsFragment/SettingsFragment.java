package com.example.yopolskyi.taskmanager.views.mainView.fragments.SettingsFragment;


import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v7.preference.CheckBoxPreference;
import android.support.v7.preference.Preference;
import android.support.v7.preference.PreferenceFragmentCompat;
import android.support.v7.preference.PreferenceManager;
import android.view.Menu;
import android.view.MenuInflater;

import com.example.yopolskyi.taskmanager.R;
import com.example.yopolskyi.taskmanager.views.mainView.IMainView;
import com.example.yopolskyi.taskmanager.views.mainView.MainActivity;
import com.example.yopolskyi.taskmanager.views.mainView.ToolbarMenuStates;

public class SettingsFragment extends PreferenceFragmentCompat {

    private IMainView parentView;

    private CheckBoxPreference soundPreference;
    private CheckBoxPreference vibrationPreference;
    private CheckBoxPreference daySummaryPreference;
    private TimePreference daySummaryTimePreference;


    public SettingsFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreatePreferences(Bundle savedInstanceState, String rootKey) {
        setHasOptionsMenu(true);
        setPreferencesFromResource(R.xml.preferences, rootKey);
        PreferenceManager.setDefaultValues(getContext(), R.xml.preferences, false);

        initFields();
        setPreferenceListeners();
    }

    @Override
    public void onDisplayPreferenceDialog(Preference preference) {

        // Try if the preference is one of our custom Preferences
        DialogFragment dialogFragment = null;
        if (preference instanceof TimePreference) {
            // Create a new instance of TimePreferenceDialogFragment with the key of the related
            // preference
            dialogFragment = TimePreferenceFragmentCompat.newInstance(preference.getKey());
        }

        // If it was one of our custom Preferences, show its dialog
        if (dialogFragment != null) {
            dialogFragment.setTargetFragment(this, 0);
            dialogFragment.show(this.getFragmentManager(),
                    "android.support.v7.preference.PreferenceFragment.DIALOG");
        }
        // Could not be handled here. Try with the super method.
        else {
            super.onDisplayPreferenceDialog(preference);
        }
    }

    @Override
    public void onResume() {
        super.onResume();

        parentView.setActionBarTitle(getResources().getString(R.string.activity_main_settings_fragment_toolbar_title));
        parentView.changeToolbarMenuState(ToolbarMenuStates.settings);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);

        menu.setGroupVisible(R.id.activity_main_toolbar_item_group_main, false);
        menu.setGroupVisible(R.id.activity_main_toolbar_item_group_context, false);
        menu.setGroupVisible(R.id.activity_main_toolbar_item_group_edit, false);
    }

    private void initFields(){
        parentView = (MainActivity) getActivity();

        soundPreference = (CheckBoxPreference) findPreference("pref_key_settings_sound");
        setEnabledDisabledPreferenceSummary(soundPreference, soundPreference.isChecked());

        vibrationPreference = (CheckBoxPreference) findPreference("pref_key_settings_vibration");
        setEnabledDisabledPreferenceSummary(vibrationPreference, vibrationPreference.isChecked());

        daySummaryPreference = (CheckBoxPreference) findPreference("pref_key_settings_day_summary");
        setEnabledDisabledPreferenceSummary(daySummaryPreference, daySummaryPreference.isChecked());

        daySummaryTimePreference = (TimePreference) findPreference("pref_key_settings_day_summary_time");
        daySummaryTimePreference.setEnabled(daySummaryPreference.isChecked());
        updateDaySummaryTime(-1);
    }

    private void setPreferenceListeners(){

        soundPreference.setOnPreferenceChangeListener(new Preference.OnPreferenceChangeListener() {
            @Override
            public boolean onPreferenceChange(Preference preference, Object newValue) {
                setEnabledDisabledPreferenceSummary(preference, (Boolean) newValue);
                return true;
            }
        });

        vibrationPreference.setOnPreferenceChangeListener(new Preference.OnPreferenceChangeListener() {
            @Override
            public boolean onPreferenceChange(Preference preference, Object newValue) {
                setEnabledDisabledPreferenceSummary(preference, (Boolean) newValue);
                return true;
            }
        });

        daySummaryPreference.setOnPreferenceChangeListener(new Preference.OnPreferenceChangeListener() {
            @Override
            public boolean onPreferenceChange(Preference preference, Object newValue) {
                setEnabledDisabledPreferenceSummary(preference, (Boolean) newValue);
                daySummaryTimePreference.setEnabled((Boolean)newValue);

                parentView.setUpDaySummaryNotificationAlarmManager((Boolean)newValue);
                return true;
            }
        });

        daySummaryTimePreference.setOnPreferenceChangeListener(new Preference.OnPreferenceChangeListener() {
            @Override
            public boolean onPreferenceChange(Preference preference, Object newValue) {
                updateDaySummaryTime((int)newValue);

                parentView.setUpDaySummaryNotificationAlarmManager(daySummaryPreference.isChecked());
                return true;
            }
        });
    }

    private void setEnabledDisabledPreferenceSummary(Preference preference, boolean state){
        if(state){
            preference.setSummary(R.string.activity_main_settings_fragment_enabled_summary);
        }
        else {
            preference.setSummary(R.string.activity_main_settings_fragment_disabled_summary);
        }
    }

    /**
     * Updates daySummaryTimePreference summary value based on the input value or value from shared
     * preferences.
     * @param daySummaryTimeInMinutesAfterMidnight -1 if we need to get value from shared
     * preferences and day summary notification time in minutes after midnight format in the another case.
     */
    private void updateDaySummaryTime(int daySummaryTimeInMinutesAfterMidnight){

        // If input argument "-1" - get value from shared preferences.
        if(daySummaryTimeInMinutesAfterMidnight == -1){
            SharedPreferences sharedPref = android.preference.PreferenceManager
                    .getDefaultSharedPreferences(getContext());

            daySummaryTimeInMinutesAfterMidnight = sharedPref.getInt(
                    "pref_key_settings_day_summary_time", -1);
        }

        int hours = daySummaryTimeInMinutesAfterMidnight / 60;
        int minutes = daySummaryTimeInMinutesAfterMidnight % 60;


        // Make hh:mm format.

        String sHours, sMinutes;

        if(hours < 10){
            sHours = "0" + Integer.toString(hours);
        }
        else {
            sHours = Integer.toString(hours);
        }

        if(minutes < 10){
            sMinutes = "0" + Integer.toString(minutes);
        }
        else {
            sMinutes = Integer.toString(minutes);
        }

        daySummaryTimePreference.setSummary(sHours + ":" + sMinutes);
    }
}
