package com.example.yopolskyi.taskmanager.model.dal;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.example.yopolskyi.taskmanager.common.AuthorizationToken;
import com.example.yopolskyi.taskmanager.common.Constants;
import com.example.yopolskyi.taskmanager.common.Task;
import com.example.yopolskyi.taskmanager.common.TokensCombination;
import com.example.yopolskyi.taskmanager.common.Utils;
import com.example.yopolskyi.taskmanager.common.dependencyInjection.main.MainComponentAssistant;
import com.example.yopolskyi.taskmanager.common.enumerations.TaskExecutionStatuses;
import com.example.yopolskyi.taskmanager.common.enumerations.TaskRepeatFrequencies;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.inject.Inject;

import static com.example.yopolskyi.taskmanager.model.dal.DBHelper.TABLE_AUTHORIZATION_TOKENS;
import static com.example.yopolskyi.taskmanager.model.dal.DBHelper.TABLE_USERS;

/**
 * Represents access to data using a "Repository" pattern.
 */
public class Repository implements IRepository {

    // For logs.
    private final String TAG = "Repository";

    private ContentValues contentValues;

    @Inject
    DBHelper dbHelper;

    /**
     * Class constructor.
     */
    public Repository() {
        contentValues = new ContentValues();
    }

    public void setContext(Context context){
        DBHelper.setContext(context);
        MainComponentAssistant.getComponent().injectRepository(this);
    }

    /**
     * Inserts an record in the "Tasks" table.
     * @param newTask an instance of the "Task" class that must be inserted into the table
     * @return added task ID in the database
     */
    @Override
    public long addTask(Task newTask) {
        Log.d(TAG, ">> [addTask]");
        Log.d(TAG, "Input argument value: " + newTask.toString());

        Log.d(TAG, "Try to put task into content values.");
        putTaskIntoContentValues(newTask);


        Log.d(TAG, "Try to insert a record in a table.");

        long id;
        try (SQLiteDatabase database = dbHelper.getWritableDatabase()) {

            id = database.insert(DBHelper.TABLE_TASKS, null, contentValues);

            Log.d(TAG, "Row inserted. ID = " + id + " " + newTask.toString());

        } catch (Exception e) {
            Log.e(TAG, "Could not insert a record into the table. " + e.toString());
            throw e;
        } finally {
            contentValues.clear();
        }

        Log.d(TAG, "<< [addTask]");

        return id;
    }

    /**
     * Updates an record in the "Tasks" table.
     * @param taskId ID of the record that must be updated
     * @param task "Task" instance that include all changes
     */
    @Override
    public void updateTask(int taskId, Task task, int userId) {
        Log.d(TAG, ">> [updateTask]");
        Log.d(TAG, "Input arguments:" + "ID = " + taskId + "" + task.toString());

        Log.d(TAG, "Try to put task into content values.");
        putTaskIntoContentValues(task);

        Log.d(TAG, "Try to update a record in a table.");

        try (SQLiteDatabase database = dbHelper.getWritableDatabase()) {

            int updatedRowsNumber = database.update(DBHelper.TABLE_TASKS, contentValues,
                    DBHelper.TABLE_TASKS_KEY_ID + " = ? AND "
                            + DBHelper.TABLE_TASKS_KEY_USER_ID + " = ?",
                    new String[]{ Integer.toString(taskId), Integer.toString(userId) });

            Log.d(TAG, "Updated rows number: " + updatedRowsNumber);
        } catch (Exception e) {
            Log.e(TAG, "Could not update a record into the table. " + e.toString());
            throw e;
        } finally {
            contentValues.clear();
        }

        Log.d(TAG, "<< [updateTask]");
    }

    /**
     * Removes an record in the "Tasks" table.
     * @param taskId ID of the record that must be removed
     * @param userId additional criterion for record searching
     */
    @Override
    public void removeTask(int taskId, int userId) {
        Log.d(TAG, ">> [removeTask]");
        Log.d(TAG, "Input arguments:" + " Task ID = " + taskId + " User ID = " + userId);

        Log.d(TAG,"Try to remove a record in a table.");

        try (SQLiteDatabase database = dbHelper.getWritableDatabase()) {

            int removedRowsNumber = database.delete(DBHelper.TABLE_TASKS,
                    DBHelper.TABLE_TASKS_KEY_ID + " = ? AND " +
                            DBHelper.TABLE_TASKS_KEY_USER_ID +" = ?",
                    new String[] {
                            Integer.toString(taskId),
                            Integer.toString(userId)
                                });

            Log.d(TAG, "Number of deleted rows: " + removedRowsNumber);
        } catch (Exception e) {
            Log.e(TAG, "Could not remove a record from the table. " + e.toString());
            throw e;
        } finally {
            contentValues.clear();
        }

        Log.d(TAG, "<< [removeTask]");
    }

    /**
     * Puts task fields into content value instance.
     */
    private void putTaskIntoContentValues(Task task){
        Log.d(TAG, ">> [putTaskIntoContentValues]");
        Log.d(TAG, "Input argument value: " + task.toString());


        Log.d(TAG, "Try to put task content into content values");
        contentValues.put(DBHelper.TABLE_TASKS_KEY_CONTENT, task.getContent());

        if(task.getDueDate() != null){
            Log.d(TAG, "Due date is not null. Try to convert date to string.");
            String sDate = Utils.convertDateToString(task.getDueDate(),
                    Constants.EXPIRATION_DATE_FORMAT);

            Log.d(TAG, "Try to put due date into content values. Value = " + sDate);
            contentValues.put(DBHelper.TABLE_TASKS_KEY_DUE_DATE, sDate);
        }
        else {
            Log.d(TAG, "Due date is null. Try to put empty string into content values.");
            contentValues.put(DBHelper.TABLE_TASKS_KEY_DUE_DATE, "");
        }

        if(task.getDueTime() != null){
            Log.d(TAG, "Due time is not null. Try to convert date to string.");
            String sTime = Utils.convertDateToString(task.getDueTime(), Constants.EXPIRATION_TIME_FORMAT);

            Log.d(TAG, "Try to put due time into content values. Value = " + sTime);
            contentValues.put(DBHelper.TABLE_TASKS_KEY_DUE_TIME, sTime);
        }
        else {
            Log.d(TAG, "Due time is null. Try to put empty string into content values.");
            contentValues.put(DBHelper.TABLE_TASKS_KEY_DUE_TIME, "");
        }

        Log.d(TAG, "Try to put task repeat frequency index into content values. Value = "
                + task.getRepeatFrequency().ordinal() + 1);
        contentValues.put(DBHelper.TABLE_TASKS_KEY_REPEAT_FREQUENCY, task.getRepeatFrequency().ordinal() + 1);

        Log.d(TAG, "Try to put task execution status index into content values. Value = "
                + task.getTaskExecutionStatus().ordinal() + 1);
        contentValues.put(DBHelper.TABLE_TASKS_KEY_STATUS_ID, task.getTaskExecutionStatus().ordinal() + 1);

        Log.d(TAG, "Try to put user ID into content values. Value = "
                + getUserIdByUsername(task.getUsername()));
        contentValues.put(DBHelper.TABLE_TASKS_KEY_USER_ID, getUserIdByUsername(task.getUsername()));

        Log.d(TAG, "<< [putTaskIntoContentValues]");
    }

    /**
     * Returns task by task ID
     * @param taskId id of the task that must be returned
     * @return task
     */
    @Override
    public Task getTaskByTaskId(int taskId, int userId) {
        Log.d(TAG, ">> [getTaskByTaskId]");
        Log.d(TAG, "Input arguments:" + " Task ID = " + taskId);

        Log.d(TAG, "Try to execute a database query to get task.");
        SQLiteDatabase database = dbHelper.getWritableDatabase();

        Cursor cursor;
        try {
            cursor = database.query(DBHelper.TABLE_TASKS, null,
                    DBHelper.TABLE_TASKS_KEY_ID + " = ? AND " +
                            DBHelper.TABLE_TASKS_KEY_USER_ID + " = ?",
                    new String[]{
                            Integer.toString(taskId),
                            Integer.toString(userId)
                    }, null, null, null);
        } catch (Exception e) {
            Log.e(TAG, "Could not execute a query." + e.toString());
            throw e;
        }

        Log.d(TAG, "Try to create and fill task");
        Task resultTask = createTaskByCursor(cursor, userId);

        database.close();

        Log.d(TAG, "Resulted task = " + resultTask);

        Log.d(TAG, "<< [getTaskByTaskId]");
        return resultTask;
    }
    /**
     * Returns all tasks for current user
     * @param userId current user ID
     * @return list "Tasks" (all table records)
     */
    @Override
    public List<Task> getTasks(int userId) {
        Log.d(TAG, ">> [getTasks]");
        Log.d(TAG, "Input arguments:" + " User ID = " + userId);


        Log.d(TAG, "Try to execute a database query to get task list.");

        List<Task> taskList;
        try (SQLiteDatabase database = dbHelper.getWritableDatabase();
             Cursor cursor = database.rawQuery("select * from " + DBHelper.TABLE_TASKS, null)) {

            Log.d(TAG, "Try to create a tasks list.");
            taskList = createTaskListByCursor(cursor, userId);
        } catch (Exception e) {
            Log.e(TAG, "Could not get task list." + e.toString());
            throw e;
        }

        Log.d(TAG, "<< [getTasks]");
        return taskList;
    }

    /**
     * Searches for records that contain a "criterion" string in a content field.
     * @param criterion criterion for searching
     * @param userId current user ID
     * @return list "Tasks" instances that contain a "criterion" string
     */
    @Override
    public List<Task> getTasksByContent(String criterion, int userId) {
        Log.d(TAG, ">> [getTasksByContent]");
        Log.d(TAG, "Input arguments:" + " Criterion = " + criterion + " User ID = " + userId);


        Log.d(TAG, "Try to execute a database query to get task list.");

        List<Task> taskList;
        try (SQLiteDatabase database = dbHelper.getWritableDatabase();
             Cursor cursor = database.query(DBHelper.TABLE_TASKS, null,
                DBHelper.TABLE_TASKS_KEY_CONTENT + " LIKE ? AND " +
                        DBHelper.TABLE_TASKS_KEY_STATUS_ID + " = ?",
                new String[]{
                        criterion,
                        Integer.toString(userId)},
                null, null, null)) {

            Log.d(TAG, "Try to create a tasks list.");
            taskList = createTaskListByCursor(cursor, userId);
        } catch (Exception e) {
            Log.e(TAG, "Could not get task list." + e.toString());
            throw e;
        }


        Log.d(TAG, "<< [getTasksByContent]");
        return taskList;
    }

    /**
     * Searches for records that contain a specified status.
     * @param executionStatus criterion for searching
     * @param userId current user ID
     * @return list "Tasks" instances that contain a specified status
     */
    @Override
    public List<Task> getTasksByExecutionStatus(TaskExecutionStatuses executionStatus, int userId) {
        Log.d(TAG, ">> [getTasksByExecutionStatus]");
        Log.d(TAG, "Input arguments:"
                + " Task execution status = " + executionStatus
                + " User ID = " + userId);

        List<Task> taskList;
        try (SQLiteDatabase database = dbHelper.getWritableDatabase();
             Cursor cursor = database.query(DBHelper.TABLE_TASKS, null,
                DBHelper.TABLE_TASKS_KEY_STATUS_ID + " = ? AND " +
                        DBHelper.TABLE_TASKS_KEY_USER_ID + " = ?",
                new String[]{
                        Integer.toString(executionStatus.ordinal() + 1),
                        Integer.toString(userId)},
                null, null, null)) {

            Log.d(TAG, "Try to create a tasks list.");
            taskList = createTaskListByCursor(cursor, userId);
        } catch (Exception e) {
            Log.e(TAG, "Could not get task list." + e.toString());
            throw e;
        }


        Log.d(TAG, "<< [getTasksByExecutionStatus]");
        return taskList;
    }

    /**
     * Creates and populates task from the database query cursor.
     * @return created task instance
     */
    private Task createTaskByCursor(Cursor cursor, int userId){
        Log.d(TAG, ">> [createTaskByCursor]");
        Log.d(TAG, "Input argument:" + " User ID = " + userId);


        Log.d(TAG, "Try to create and populate task instance.");

        Task resultTask = new Task();

        if (cursor.moveToFirst()) {

            Log.d(TAG, "Try to get column indices.");
            int idColumnIndex = cursor.getColumnIndex(DBHelper.TABLE_TASKS_KEY_ID);
            int contentColumnIndex = cursor.getColumnIndex(DBHelper.TABLE_TASKS_KEY_CONTENT);
            int dueDateColumnIndex = cursor.getColumnIndex(DBHelper.TABLE_TASKS_KEY_DUE_DATE);
            int dueTimeColumnIndex = cursor.getColumnIndex(DBHelper.TABLE_TASKS_KEY_DUE_TIME);
            int repeatFrequencyIdColumnIndex = cursor.getColumnIndex(
                    DBHelper.TABLE_TASKS_KEY_REPEAT_FREQUENCY);
            int statusIdColumnIndex = cursor.getColumnIndex(DBHelper.TABLE_TASKS_KEY_STATUS_ID);
            int userIdColumnIndex = cursor.getColumnIndex(DBHelper.TABLE_TASKS_KEY_USER_ID);


            //Task ID
            resultTask.setId(cursor.getInt(idColumnIndex));

            // Task content.
            resultTask.setContent(cursor.getString(contentColumnIndex));

            // Task due date.
            String sDate = cursor.getString(dueDateColumnIndex);
            Date date = Utils.convertStringToDate(sDate, Constants.EXPIRATION_DATE_FORMAT);
            resultTask.setDueDate(date);

            // Task due time.
            String sTime = cursor.getString(dueTimeColumnIndex);
            Date time = Utils.convertStringToDate(sTime, Constants.EXPIRATION_TIME_FORMAT);
            resultTask.setDueTime(time);

            // Task repeat frequency.
            int repeatFrequencyIndex = cursor.getInt(repeatFrequencyIdColumnIndex);
            TaskRepeatFrequencies repeatFrequency = TaskRepeatFrequencies
                    .values()[repeatFrequencyIndex - 1];
            resultTask.setRepeatFrequency(repeatFrequency);

            // Task execution status.
            int taskStatusIndex = cursor.getInt(statusIdColumnIndex);
            TaskExecutionStatuses taskExecutionStatus = TaskExecutionStatuses
                    .values()[taskStatusIndex - 1];
            resultTask.setTaskExecutionStatus(taskExecutionStatus);

            // Username
            String username = getUsernameByUserId(cursor.getInt(userIdColumnIndex));
            resultTask.setUsername(username);
        }

        cursor.close();

        Log.d(TAG, "Returned task: " + resultTask.toString());

        Log.d(TAG, "<< [createTaskByCursor]");
        return resultTask;
    }

    /**
     * Creates and populates List<Task> from the database query cursor.
     * @return created List<Task>
     */
    private List<Task> createTaskListByCursor(Cursor cursor, int userId) {
        Log.d(TAG, ">> [createTaskListByCursor]");
        Log.d(TAG, "Input argument:" + " User ID = " + userId);


        List<Task> taskList = new ArrayList<>();

        if (cursor != null) {
            if (cursor.moveToFirst()) {

                int idColumnIndex = cursor.getColumnIndex(DBHelper.TABLE_TASKS_KEY_ID);
                int contentColumnIndex = cursor.getColumnIndex(DBHelper.TABLE_TASKS_KEY_CONTENT);
                int dueDateColumnIndex = cursor.getColumnIndex(DBHelper.TABLE_TASKS_KEY_DUE_DATE);
                int dueTimeColumnIndex = cursor.getColumnIndex(DBHelper.TABLE_TASKS_KEY_DUE_TIME);
                int repeatFrequencyIdColumnIndex = cursor.getColumnIndex(
                        DBHelper.TABLE_TASKS_KEY_REPEAT_FREQUENCY);
                int statusIdColumnIndex = cursor.getColumnIndex(DBHelper.TABLE_TASKS_KEY_STATUS_ID);

                do {
                    Task tmpTask = new Task();

                    //Task ID
                    tmpTask.setId(cursor.getInt(idColumnIndex));

                    // Task content.
                    tmpTask.setContent(cursor.getString(contentColumnIndex));

                    // Task due date.
                    String sDate = cursor.getString(dueDateColumnIndex);
                    Date date = Utils.convertStringToDate(sDate, Constants.EXPIRATION_DATE_FORMAT);
                    tmpTask.setDueDate(date);

                    // Task due time.
                    String sTime = cursor.getString(dueTimeColumnIndex);
                    Date time = Utils.convertStringToDate(sTime, Constants.EXPIRATION_TIME_FORMAT);
                    tmpTask.setDueTime(time);

                    // Task repeat frequency.
                    int repeatFrequencyIndex = cursor.getInt(repeatFrequencyIdColumnIndex);
                    TaskRepeatFrequencies repeatFrequency = TaskRepeatFrequencies
                            .values()[repeatFrequencyIndex - 1];
                    tmpTask.setRepeatFrequency(repeatFrequency);

                    // Task execution status.
                    int taskStatusIndex = cursor.getInt(statusIdColumnIndex);
                    TaskExecutionStatuses taskExecutionStatus = TaskExecutionStatuses
                            .values()[taskStatusIndex -1];
                    tmpTask.setTaskExecutionStatus(taskExecutionStatus);

                    // Username
                    String username = getUsernameByUserId(userId);
                    tmpTask.setUsername(username);

                    taskList.add(tmpTask);
                } while (cursor.moveToNext());
            }
        }

        Log.d(TAG, "Result tasks list: ");
        for(Task filteredTask : taskList){
            Log.d(TAG, filteredTask.toString());
        }

        Log.d(TAG, "<< [createTaskListByCursor]");

        return taskList;
    }

    /**
     * Finds a username-password combination in the "Users" table.
     * @param username entered username
     * @param password entered password
     * @return user ID if combination was founded or "-1" in other case
     */
    @Override
    public int getUser(String username, String password) {
        Log.d(TAG, ">> [getUser]");
        Log.d(TAG, "Input arguments:" + " Username = " + username + " Password = " + password);


        SQLiteDatabase database = dbHelper.getWritableDatabase();

        Log.d(TAG, "Try to execute a query to get user.");

        Cursor cursor;
        try{
            cursor = database.query(DBHelper.TABLE_USERS, null,
                    DBHelper.TABLE_USERS_KEY_USERNAME + " = ? AND " +
                            DBHelper.TABLE_USERS_KEY_PASSWORD + " = ?",
                    new String[]{ username, password }, null, null, null, null);

            Log.d(TAG, "Successful.");
        }
        catch (Exception e){
            Log.e(TAG, "Could not execute a query." + e.toString());
            throw e;
        }

        // Method returns "-1" if user was not found.
        int userId = -1;
        if(cursor != null){
            if(cursor.moveToFirst()){
                String tmp = cursor.getString(cursor.getColumnIndex(DBHelper.TABLE_USERS_KEY_ID));

                try{
                    userId = Integer.parseInt(tmp);
                }
                catch (Exception e){
                    Log.e(TAG, "Could not parse User ID .");
                    throw e;
                }
            }
            cursor.close();
        }
        database.close();


        Log.d(TAG, "Returned User ID = " + userId);

        Log.d(TAG, "<< [getUser]");
        return userId;
    }

    /**
     * Calls addToken method for access and refresh tokens.
     */
    @Override
    public void addTokens(TokensCombination tokensCombination){
        Log.d(TAG, ">> [addTokens]");

        Log.d(TAG, "Calls addToken method for access and refresh tokens");
        try{
            addToken(tokensCombination.getAccessToken());
            addToken(tokensCombination.getRefreshToken());
        }
        catch (Exception e){
            Log.e(TAG, "Could not add token to the database table. " + e.toString());
            throw e;
        }

        Log.d(TAG, "<< [addTokens]");
    }
    /**
     * Inserts a record into "Authorization tokens" table.
     */
    private void addToken(AuthorizationToken token) {
        Log.d(TAG, ">> [addToken]");
        Log.d(TAG, "Input argument: " + token.toString());


        Log.d(TAG, "Try to put token into content values.");
        putTokenIntoContentValues(token);


        Log.d(TAG, "Try to insert token in a table.");

        try (SQLiteDatabase database = dbHelper.getWritableDatabase()) {
            long id = database.insert(TABLE_AUTHORIZATION_TOKENS, null, contentValues);
            Log.d(TAG, "Record inserted: ");
            Log.d(TAG, "Token ID = " + id);
        } catch (Exception e) {
            Log.e(TAG, "Could not insert a record into the table. " + e.toString());
            throw e;
        } finally {
            contentValues.clear();
        }

        Log.d(TAG, "<< [addToken]");
    }

    /**
     * Calls updateToken method for access and refresh tokens.
     */
    @Override
    public void updateTokens(int userId, TokensCombination tokensCombination) {
        Log.d(TAG, ">> [updateTokens]");
        Log.d(TAG, "Input arguments:" +
                "ID = " + userId +
                "Access token = " + tokensCombination.getAccessToken().toString() +
                "Refresh token = " + tokensCombination.getRefreshToken().toString());

        Log.d(TAG, "Calls the updateToken method");

        try {
            updateToken(userId, tokensCombination.getAccessToken());
            updateToken(userId, tokensCombination.getRefreshToken());
        }
        catch (Exception e){
            Log.e(TAG, "Could not update token in the database table. " + e.toString());
            throw e;
        }

        Log.d(TAG, "<< [updateTokens]");
    }

    /**
     * Updates a record in "Authorization tokens" table.
     * @param userId current user ID
     * @param token new record values
     */
    private void updateToken(int userId, AuthorizationToken token){
        Log.d(TAG, ">> [updateToken]");
        Log.d(TAG, "Input arguments:" + "ID = " + userId + " Token = " + token.toString());

        Log.d(TAG, "Try to put token into content values.");
        putTokenIntoContentValues(token);

        Log.d(TAG, "Try to update a record in a table.");

        try (SQLiteDatabase database = dbHelper.getWritableDatabase()) {

            int updatedRowsNumber = database.update(DBHelper.TABLE_AUTHORIZATION_TOKENS, contentValues,
                    DBHelper.TABLE_AUTHORIZATION_TOKENS_KEY_USER_ID + " = ? AND " +
                            DBHelper.TABLE_AUTHORIZATION_TOKENS_KEY_TOKEN_TYPE_ID + " = ?",
                    new String[]{ Integer.toString(userId),
                            Integer.toString(token.getTokenType().ordinal() + 1 ) });

            Log.d(TAG, "Updated rows number: " + updatedRowsNumber);
        } catch (Exception e) {
            Log.e(TAG, "Could not update a record into the table. " + e.toString());
            throw e;
        } finally {
            contentValues.clear();
        }

        Log.d(TAG, "<< [updateToken]");
    }

    /**
     * Removes a record from the "Authorization tokens" table.
     * @param userId ID of the record that must be removed
     */
    @Override
    public void removeTokens(int userId) {
        Log.d(TAG, ">> [removeTokens]");
        Log.d(TAG, "Input argument:" + " ID = " + userId);

        Log.d(TAG, "Try to remove a record in a table.");

        try (SQLiteDatabase database = dbHelper.getWritableDatabase()) {

            int removedRowsNumber = database.delete(
                    TABLE_AUTHORIZATION_TOKENS,
                    DBHelper.TABLE_AUTHORIZATION_TOKENS_KEY_USER_ID + " = ?",
                    new String[]{ Integer.toString(userId) });

            Log.d(TAG, "Number of deleted rows: " + removedRowsNumber);
        } catch (Exception e) {
            Log.e(TAG, "Could not remove a record from the table. " + e.toString());
            throw e;
        }

        Log.d(TAG, "<< [removeToken]");
    }

    /**
     * Puts token fields into content values
     */
    private void putTokenIntoContentValues(AuthorizationToken token){
        Log.d(TAG, ">> [putTokenIntoContentValues]");
        Log.d(TAG, "Input argument: " + token.toString());

        int userID = token.getUserID();
        Log.d(TAG, "Try to put user ID into content values. Value = " + userID);
        contentValues.put(DBHelper.TABLE_AUTHORIZATION_TOKENS_KEY_USER_ID, userID);

        String tokenValue = token.getTokenValue();
        Log.d(TAG, "Try to put token value into content values. Value = " + tokenValue);
        contentValues.put(DBHelper.TABLE_AUTHORIZATION_TOKENS_KEY_VALUE, tokenValue);

        // Item indexes in enums starts from 0, and in db tables - from 1.
        int tokenTypeId = token.getTokenType().ordinal() + 1;
        Log.d(TAG, "Try to put token type index into content values. Value = " + tokenTypeId);
        contentValues.put(DBHelper.TABLE_AUTHORIZATION_TOKENS_KEY_TOKEN_TYPE_ID, tokenTypeId);

        String tokenExpirationDate = Utils.convertDateToString(token.getTokenExpirationDate(),
                Constants.TOKENS_DATE_FORMAT);
        Log.d(TAG, "Try to put token expiration date into content values. Value = "
                + tokenExpirationDate);
        contentValues.put(DBHelper.TABLE_AUTHORIZATION_TOKENS_KEY_EXPIRATION_DATE, tokenExpirationDate);

        Log.d(TAG, "<< [putTokenIntoContentValues]");
    }

    /**
     * Finds user in the database by access token and returns user ID.
     * @return user ID if user was founded and "-1" in another case
     */
    @Override
    public int getUserIdByAccessToken(String accessToken) {
        Log.d(TAG, ">> [getUserIdByAccessToken]");
        Log.d(TAG, "Input argument:" + " Access token = " + accessToken);

        Log.d(TAG, "Try to execute a query.");

        SQLiteDatabase database = dbHelper.getWritableDatabase();

        Cursor cursor;
        try{
            cursor = database.query(
                    TABLE_AUTHORIZATION_TOKENS,
                    null,
                    DBHelper.TABLE_AUTHORIZATION_TOKENS_KEY_VALUE + " = ?",
                    new String[]{ accessToken }, null, null, null, null);
        } catch (Exception e) {
            Log.e(TAG, "Could not execute a query." + e.toString());
            database.close();
            throw e;
        }

        int userId = -1;
        if(cursor != null){
            if(cursor.moveToFirst()){
                String tmp = cursor.getString(cursor.getColumnIndex(DBHelper.TABLE_USERS_KEY_ID));

                try{
                    userId = Integer.parseInt(tmp);
                }
                catch (Exception e){
                    Log.e(TAG, "Could not parse User ID .");
                    cursor.close();
                    database.close();
                    throw e;
                }
            }
            cursor.close();
        }
        else {
            Log.e(TAG, "Could not find a record.");
        }

        database.close();

        Log.d(TAG, "Returned value = " + userId);
        Log.d(TAG, "<< [getUserIdByAccessToken]");

        return userId;
    }

    /**
     * Finds user in the database by user ID and returns username.
     * @return username if user was founded and "null" in another case
     */
    @Override
    public String getUsernameByUserId(int userId) {
        Log.d(TAG, ">> [getUsernameByUserId]");
        Log.d(TAG, "Input argument:" + " User ID = " + userId);

        Log.d(TAG, "Try to execute a database query to get username.");

        SQLiteDatabase database = dbHelper.getWritableDatabase();

        Cursor cursor;
        try{
            cursor = database.query(
                    TABLE_USERS,
                    null,
                    DBHelper.TABLE_USERS_KEY_ID + " = ?",
                    new String[]{ Integer.toString(userId) }, null, null, null, null);
        } catch (Exception e) {
            Log.e(TAG, "Could not execute a query." + e.toString());
            database.close();
            throw e;
        }

        String username = null;
        if(cursor != null){
            if(cursor.moveToFirst()){
                username = cursor.getString(cursor.getColumnIndex(DBHelper.TABLE_USERS_KEY_USERNAME));
            }
            cursor.close();
        }
        else {
            Log.e(TAG, "Could not find a record.");
        }

        database.close();

        Log.d(TAG, "Returned username = " + username);
        Log.d(TAG, "<< [getUsernameByUserId]");

        return username;
    }

    /**
     * Finds user in the database by username and returns user ID.
     * @return user ID if user was founded and "-1" in another case
     */
    @Override
    public int getUserIdByUsername(String username) {
        Log.d(TAG, ">> [getUserIdByUsername]");
        Log.d(TAG, "Input argument:" + " Username = " + username);

        Log.d(TAG, "Try to execute a database query to get user ID.");

        SQLiteDatabase database = dbHelper.getWritableDatabase();

        Cursor cursor;
        try{
            cursor = database.query(
                    TABLE_USERS,
                    null,
                    DBHelper.TABLE_USERS_KEY_USERNAME + " = ?",
                    new String[]{ username }, null, null, null, null);
        } catch (Exception e) {
            Log.e(TAG, "Could not execute a query." + e.toString());
            database.close();
            throw e;
        }

        int userId = -1;
        if(cursor != null){
            if(cursor.moveToFirst()){
                String sUserId = cursor.getString(cursor.getColumnIndex(DBHelper.TABLE_USERS_KEY_ID));

                try{
                    userId = Integer.parseInt(sUserId);
                }
                catch (Exception e){
                    Log.e(TAG, "Could not parse User ID .");
                    throw e;
                }
            }
            cursor.close();
        }
        else {
            Log.e(TAG, "Could not find a record.");
        }

        database.close();

        Log.d(TAG, "Returned value = " + userId);
        Log.d(TAG, "<< [getUserIdByUsername]");

        return userId;
    }
}
