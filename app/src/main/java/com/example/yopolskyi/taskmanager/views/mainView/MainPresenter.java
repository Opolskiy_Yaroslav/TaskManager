package com.example.yopolskyi.taskmanager.views.mainView;


import android.content.SharedPreferences;
import android.util.Log;

import com.example.yopolskyi.taskmanager.common.Constants;
import com.example.yopolskyi.taskmanager.common.ResultObject;
import com.example.yopolskyi.taskmanager.common.Task;
import com.example.yopolskyi.taskmanager.common.Utils;
import com.example.yopolskyi.taskmanager.common.dependencyInjection.main.MainComponentAssistant;
import com.example.yopolskyi.taskmanager.common.enumerations.ResultMessage;
import com.example.yopolskyi.taskmanager.common.enumerations.TaskExecutionStatuses;
import com.example.yopolskyi.taskmanager.common.enumerations.TaskRepeatFrequencies;
import com.example.yopolskyi.taskmanager.model.IModel;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.inject.Inject;

public class MainPresenter implements IMainPresenter {

    private IMainView view;

    @Inject
    IModel model;

    // For logging.
    private final String TAG = "MainPresenter";

    public MainPresenter(IMainView view) {
        this.view = view;
        MainComponentAssistant.getComponent().injectMainPresenter(this);
    }

    /**
     * Checks if entered date is less than current date (user can only add a future tasks)
     * @param date entered date
     */
    @Override
    public Boolean isDateValid(String date) {
        Log.d(TAG, ">> [isDateValid]");
        Log.d(TAG, "Input argument:" + " Entered date = " + date);

        if(date.isEmpty()){
            Log.d(TAG, "Field is empty. Return true");
            return true;
        }

        Date currentDate = Calendar.getInstance().getTime();
        Log.d(TAG, "Current date = " + currentDate);


        Log.d(TAG, "Try to parse entered date from string to date.");
        Date enteredDate = Utils.convertStringToDate(date, Constants.DATE_FORMAT_FOR_USER);

        // Convert do string to avoid comparing time, we need to compare only date values.
        String sCurrentDate = Utils.convertDateToString(currentDate, Constants.EXPIRATION_DATE_FORMAT);
        String sEnteredDate = Utils.convertDateToString(enteredDate, Constants.EXPIRATION_DATE_FORMAT);

        if (sCurrentDate.compareTo(sEnteredDate) <= 0)
        {
            Log.d(TAG, "Entered date is equal or later than " +
                "current date. Calls view error method with false parameter.");
            view.showNewTaskFragmentDueDateEditTextError(false);

            Log.d(TAG, "Return true");
            return true;
        }
        Log.d(TAG, "Entered date is less than " +
                "current date. Calls view error method with true parameter.");

        view.showNewTaskFragmentDueDateEditTextError(true);

        Log.d(TAG, "Return false");
        Log.d(TAG, "<< [isDateValid]");

        return false;
    }

    /**
     * Checks if entered time is less than current if entered and current dates equal
     */
    @Override
    public Boolean isTimeValid(String date, String time) {
        Log.d(TAG, ">> [isTimeValid]");
        Log.d(TAG, "Input arguments:"
                + " Entered date = " + date
                + " Entered time = " + time);

        // If date or time is not defined.
        if(date.isEmpty() || time.isEmpty()){
            Log.d(TAG, "Date or time is not defined. Return true.");
            return true;
        }

        Date currentDate = Calendar.getInstance().getTime();
        Log.d(TAG, "Current date = " + currentDate);


        Log.d(TAG, "Try to parse entered date from string to date.");
        Date enteredDate = Utils.convertStringToDate(date, Constants.DATE_FORMAT_FOR_USER);

        // Convert do string to avoid comparing time, we need to compare only date values.
        String sCurrentDate = Utils.convertDateToString(currentDate, Constants.EXPIRATION_DATE_FORMAT);
        String sEnteredDate = Utils.convertDateToString(enteredDate, Constants.EXPIRATION_DATE_FORMAT);


        Log.d(TAG, "Try to compare entered and current dates.");

        if (sCurrentDate.compareTo(sEnteredDate) == 0)
        {
            Log.d(TAG, "Entered and current dates are equal. Try to check time.");

            Date currentTime = Calendar.getInstance().getTime();
            Log.d(TAG, "Current date = " + currentDate);

            Log.d(TAG, "Try to parse entered time from string to date.");
            Date enteredTime = Utils.convertStringToDate(time, Constants.EXPIRATION_TIME_FORMAT);

            Log.d(TAG, "Try to compare entered and current time.");

            // Convert do string to avoid comparing date, we need to compare only time values.
            String sCurrentTime = Utils.convertDateToString(currentTime, Constants.EXPIRATION_TIME_FORMAT);
            String sEnteredTime = Utils.convertDateToString(enteredTime, Constants.EXPIRATION_TIME_FORMAT);

            if (sCurrentTime.compareTo(sEnteredTime) < 0){
                Log.d(TAG, "Entered time is later than " +
                        "current time. Calls view error method with false parameter.");
                view.showNewTaskFragmentDueTimeEditTextError(false);
                Log.d(TAG, "Return true");
                return true;
            }
            else{
                Log.d(TAG, "Entered time is less than " +
                        "current time. Calls view error method with true parameter.");
                view.showNewTaskFragmentDueTimeEditTextError(true);
                Log.d(TAG, "Return false");
                return false;
            }
        }

        Log.d(TAG, "Entered and current dates are different. " +
                "Calls view error method with false parameter.");
        view.showNewTaskFragmentDueTimeEditTextError(false);

        Log.d(TAG, "Return true");
        Log.d(TAG, "<< [isTimeValid]");

        return true;
    }

    /**
     * Checks entered values and calls model addTask method if values are correct
     */
    @Override
    public void addTask(String content, String date, String time, int repeatFrequencyId) {
        Log.d(TAG, ">> [addTask]");
        Log.d(TAG, "Input arguments:"
                + " Task content = " + content
                + " Date = " + date
                + " Time = " + time
                + " Repeat frequency ID = " + repeatFrequencyId);

        // Validate values.
        if(content.isEmpty()){
            Log.d(TAG, "Content is empty, show error and return");
            view.showNewTaskFragmentTaskContentEditTextErrorToast();
            return;
        }
        Log.d(TAG, "Content is not empty");

        if(!isDateValid(date)){
            Log.d(TAG, "Date is invalid, show error and return");
            view.showNewTaskFragmentDueDateEditTextErrorToast();
            return;
        }
        Log.d(TAG, "Date is valid");

        if(!isTimeValid(date, time)){
            Log.d(TAG, "Time is invalid, show error and return");
            view.showNewTaskFragmentDueTimeEditTextErrorToast();
            return;
        }
        Log.d(TAG, "Time is valid");


        Log.d(TAG, "Values is correct, create new task instance");

        Task newTask = new Task();

        // Content value.
        newTask.setContent(content);

        // Due date value.
        Date newDate = Utils.convertStringToDate(date, Constants.DATE_FORMAT_FOR_USER);
        newTask.setDueDate(newDate);

        //Due time value.
        Date newTime = Utils.convertStringToDate(time, Constants.EXPIRATION_TIME_FORMAT);
        newTask.setDueTime(newTime);

        // Repeat frequency value.
        TaskRepeatFrequencies newRepeatFrequency = TaskRepeatFrequencies.values()[repeatFrequencyId];
        newTask.setRepeatFrequency(newRepeatFrequency);

        // Username.
        String accessToken = view.getAccessToken();

        Log.d(TAG, "Calling the model getUserIdByAccessToken method.");
        ResultObject getUserIdResult = model.getUserIdByAccessToken(accessToken);
        if(getUserIdResult.getResultMessage() != ResultMessage.SUCCESSFUL){
            view.showInternalErrorDialog();
            return;
        }

        int userId = (int) getUserIdResult.getResultValue();
        Log.d(TAG, "User ID = " + userId);

        Log.d(TAG, "Calling the model getUsernameByUserId method.");
        ResultObject getUsernameResult = model.getUsernameByUserId(userId);
        if(getUsernameResult.getResultMessage() != ResultMessage.SUCCESSFUL){
            view.showInternalErrorDialog();
            return;
        }

        String username = (String) getUsernameResult.getResultValue();
        Log.d(TAG, "Username = " + username);

        newTask.setUsername(username);


        // Execution status.
        newTask.setTaskExecutionStatus(TaskExecutionStatuses.inProgress);


        Log.d(TAG, "Calls model addTask method");

        ResultObject addTaskResult = model.addTask(newTask);
        if(addTaskResult.getResultMessage() != ResultMessage.SUCCESSFUL){
            view.showInternalErrorDialog();
            return;
        }

        int taskId = (int) (long) addTaskResult.getResultValue();

        // Refresh task status(for example if we changed date for overdue task to further - we need to
        // change its status to "in progress").
        refreshTaskStatuses(accessToken);

        view.showNewTaskFragmentTaskAddedToast();

        // Create notification of due date and time has been set.
        setUpTaskNotificationAlarmManager(taskId);

        view.onBackPressedDefault();

        Log.d(TAG, "<< [addTask]");
    }

    /**
     * Calls model getTask method
     * @param taskId criterion for searching
     * @param accessToken current user access token
     * @return task with specified task ID
     */
    @Override
    public Task getTask(int taskId, String accessToken) {
        Log.d(TAG, ">> [getTasks]");
        Log.d(TAG, "Input argument:" + " Task ID = " + taskId + " Access token = " + accessToken);

        Log.d(TAG, "Calling the model getTask method.");
        ResultObject getTaskResult = model.getTask(taskId, accessToken);
        if(getTaskResult.getResultMessage() != ResultMessage.SUCCESSFUL){
            view.showInternalErrorDialog();
            return null;
        }

        Task task = (Task) getTaskResult.getResultValue();

        Log.d(TAG, "Returned task: " + task.toString());

        Log.d(TAG, "<< [getTasks]");

        return task;
    }

    /**
     * Calls model getTasks method
     * @param accessToken current user access token
     * @return list of tasks
     */
    @Override
    public List<Task> getTasks(String accessToken) {
        Log.d(TAG, ">> [getTasks]");
        Log.d(TAG, "Input argument:" + " Access token = " + accessToken);

        Log.d(TAG, "Calling the model getTasks method.");
        ResultObject getTasksResult = model.getTasks(accessToken);
        if(getTasksResult.getResultMessage() != ResultMessage.SUCCESSFUL){
            view.showInternalErrorDialog();
            return null;
        }

        List<Task> taskList = (ArrayList) getTasksResult.getResultValue();

        Log.d(TAG, "Returned list:");
        for(Task task : taskList){
            Log.d(TAG, task.toString());
        }

        Log.d(TAG, "<< [getTasks]");

        return taskList;
    }

    /**
     * Calls model getTasks method
     * @param criterion criterion for searching by content
     * @param accessToken current user access token
     * @return list of tasks
     */
    @Override
    public List<Task> getTasks(String criterion, String accessToken){
        Log.d(TAG, ">> [getTasks]");
        Log.d(TAG, "Input arguments:"
                + " Criterion = " + criterion
                + " Access token = " + accessToken);

        Log.d(TAG, "Calling the model getTasks method.");
        ResultObject getTasksResult = model.getTasks(criterion, accessToken);
        if(getTasksResult.getResultMessage() != ResultMessage.SUCCESSFUL){
            view.showInternalErrorDialog();
            return null;
        }

        List<Task> taskList = (ArrayList) getTasksResult.getResultValue();

        Log.d(TAG, "Returned list:");
        for(Task task : taskList){
            Log.d(TAG, task.toString());
        }

        Log.d(TAG, "<< [getTasks]");

        return taskList;
    }

    /**
     * Calls model getTasks method
     * @param executionStatus criterion for searching by execution status
     * @param accessToken current user access token
     * @return list of tasks
     */
    @Override
    public List<Task> getTasks(TaskExecutionStatuses executionStatus, String accessToken) {
        Log.d(TAG, ">> [getTasks]");
        Log.d(TAG, "Input arguments:"
                + " Task execution status = " + executionStatus
                + " Access token = " + accessToken);

        Log.d(TAG, "Calling the model getTasks method.");
        ResultObject getTasksResult = model.getTasks(executionStatus, accessToken);
        if(getTasksResult.getResultMessage() != ResultMessage.SUCCESSFUL){
            view.showInternalErrorDialog();
            return null;
        }

        List<Task> taskList = (ArrayList) getTasksResult.getResultValue();


        Log.d(TAG, "Returned list:");
        for(Task task : taskList){
            Log.d(TAG, task.toString());
        }

        Log.d(TAG, "<< [getTasks]");

        return taskList;
    }

    /**
     * Calls the model refreshTaskStatuses method
     * @param accessToken current user access token
     */
    @Override
    public void refreshTaskStatuses(String accessToken) {
        Log.d(TAG, ">> [refreshTaskStatuses]");
        Log.d(TAG, "Input arguments:" + " Access token = " + accessToken);

        Log.d(TAG, "Calling the model refreshTaskStatuses method.");
        ResultObject refreshTaskStatusesResult = model.refreshTaskStatuses(accessToken);
        if(refreshTaskStatusesResult.getResultMessage() != ResultMessage.SUCCESSFUL){
            view.showInternalErrorDialog();
            return;
        }

        Log.d(TAG, "<< [refreshTaskStatuses]");
    }

    /**
     * Calls model updateTask method. Uses for task editing.
     * @param taskId id of the task that must be updated
     */
    @Override
    public void updateTask(int taskId, String content, String date, String time, int repeatFrequencyId,
                           TaskExecutionStatuses taskExecutionStatus) {
        Log.d(TAG, ">> [updateTask]");
        Log.d(TAG, "Input arguments:"
                + " Task id = " + taskId
                + " Task content = " + content
                + " Task date =  " + date
                + " Task time =  " + time
                + " Repeat frequency ID = " + repeatFrequencyId
                + " Task execution status = " + taskExecutionStatus.toString());

        // Validate values.
        if(content.isEmpty()){
            Log.d(TAG, "Content is empty, show error and return");
            view.showNewTaskFragmentTaskContentEditTextErrorToast();
            return;
        }
        Log.d(TAG, "Content is not empty");

        if(!isDateValid(date)){
            Log.d(TAG, "Date is invalid, show error and return");
            view.showNewTaskFragmentDueDateEditTextErrorToast();
            return;
        }
        Log.d(TAG, "Date is valid");

        if(!isTimeValid(date, time)){
            Log.d(TAG, "Time is invalid, show error and return");
            view.showNewTaskFragmentDueTimeEditTextErrorToast();
            return;
        }
        Log.d(TAG, "Time is valid");


        Log.d(TAG, "Values is correct, create new task instance");

        Task newTask = new Task();

        // Content value.
        newTask.setContent(content);

        // Due date value.
        Date newDate;
        if(!date.isEmpty()){
            newDate = Utils.convertStringToDate(date, Constants.DATE_FORMAT_FOR_USER);
            newTask.setDueDate(newDate);
        }

        //Due time value.
        Date newTime;
        if(!time.isEmpty()){
            newTime = Utils.convertStringToDate(time, Constants.EXPIRATION_TIME_FORMAT);
            newTask.setDueTime(newTime);
        }

        // Repeat frequency value.
        TaskRepeatFrequencies newRepeatFrequency = TaskRepeatFrequencies.values()[repeatFrequencyId];
        newTask.setRepeatFrequency(newRepeatFrequency);

        // Username.
        String accessToken = view.getAccessToken();

        Log.d(TAG, "Calling the model getUserIdByAccessToken method.");
        ResultObject getUserIdResult = model.getUserIdByAccessToken(accessToken);
        if(getUserIdResult.getResultMessage() != ResultMessage.SUCCESSFUL){
            view.showInternalErrorDialog();
            return;
        }

        int userId = (int) getUserIdResult.getResultValue();
        Log.d(TAG, "User ID = " + userId);

        Log.d(TAG, "Calling the model getUsernameByUserId method.");
        ResultObject getUsernameResult = model.getUsernameByUserId(userId);
        if(getUsernameResult.getResultMessage() != ResultMessage.SUCCESSFUL){
            view.showInternalErrorDialog();
            return;
        }

        String username = (String) getUsernameResult.getResultValue();
        Log.d(TAG, "Username = " + username);

        newTask.setUsername(username);


        // Execution status.
        newTask.setTaskExecutionStatus(taskExecutionStatus);


        Log.d(TAG, "Calls model updateTask method");

        ResultObject updateTaskResult = model.updateTask(taskId, newTask, accessToken);
        if(updateTaskResult.getResultMessage() != ResultMessage.SUCCESSFUL){
            view.showInternalErrorDialog();
            return;
        }

        // Refresh task status(for example if we changed date for overdue task to further - we need to
        // change its status to "in progress").
        refreshTaskStatuses(accessToken);

        view.showNewTaskFragmentTaskAddedToast();

        setUpTaskNotificationAlarmManager(taskId);

        view.onBackPressedDefault();

        Log.d(TAG, "<< [updateTask]");
    }

    /**
     * Calls model updateTask method. Uses for task finishing.
     */
    @Override
    public void updateTask(Task newTask) {
        Log.d(TAG, ">> [updateTask]");
        Log.d(TAG, "Input arguments:"
                + " New task = " + newTask);

        String accessToken = view.getAccessToken();

        Log.d(TAG, "Calls model updateTask method");

        ResultObject updateTaskResult = model.updateTask(newTask.getId(), newTask, accessToken);
        if(updateTaskResult.getResultMessage() != ResultMessage.SUCCESSFUL){
            view.showInternalErrorDialog();
            return;
        }

        // Refresh task status(for example if we changed date for overdue task to later - we need to
        // change its status to "in progress").
        refreshTaskStatuses(accessToken);

        Log.d(TAG, "<< [updateTask]");
    }

    /**
     * Calls model refreshRepeatableTaskDueDate method
     * @param taskId task that must be refreshed ID
     * @param accessToken current user access token
     */
    @Override
    public void refreshRepeatableTaskDueDate(int taskId, String accessToken) {
        Log.d(TAG, ">> [refreshRepeatableTaskDueDate]");
        Log.d(TAG, "Input argument:" + " Task ID: " + taskId + " Access token: " + accessToken);

        Log.d(TAG, "Calling the model refreshRepeatableTaskDueDate method.");
        ResultObject refreshRepeatableTaskDueDateResult = model.refreshRepeatableTaskDueDate(
                taskId, accessToken);
        if(refreshRepeatableTaskDueDateResult.getResultMessage() != ResultMessage.SUCCESSFUL){
            view.showInternalErrorDialog();
            return;
        }

        // Refresh notification for this task.
        setUpTaskNotificationAlarmManager(taskId);

        Log.d(TAG, ">> [refreshRepeatableTaskDueDate]");
    }

    /**
     * Calls model refreshRepeatableTaskDueDates method
     * @param accessToken current user access token
     */
    @Override
    public void refreshRepeatableTaskDueDates(String accessToken) {
        Log.d(TAG, ">> [refreshRepeatableTaskDueDate]");
        Log.d(TAG, "Input argument:" + " Access token: " + accessToken);

        Log.d(TAG, "Calling the model refreshRepeatableTaskDueDates method.");
        ResultObject refreshRepeatableTaskDueDatesResult =
                model.refreshRepeatableTaskDueDates(accessToken);
        if(refreshRepeatableTaskDueDatesResult.getResultMessage() != ResultMessage.SUCCESSFUL){
            view.showInternalErrorDialog();
            return;
        }

        Log.d(TAG, ">> [refreshRepeatableTaskDueDate]");
    }

    /**
     * Calls model extendRepeatableTaskDueDate method
     * @param accessToken current user access token
     */
    @Override
    public void extendRepeatableTaskDueDate(int taskId, String accessToken) {
        Log.d(TAG, ">> [extendRepeatableTaskDueDate]");
        Log.d(TAG, "Input argument:" + " Access token: " + accessToken);

        Log.d(TAG, "Calling the model extendRepeatableTaskDueDate method.");
        ResultObject extendRepeatableTaskDueDateResult =
                model.extendRepeatableTaskDueDate(taskId, accessToken);
        if(extendRepeatableTaskDueDateResult.getResultMessage() != ResultMessage.SUCCESSFUL){
            view.showInternalErrorDialog();
            return;
        }

        // Refresh notification for this task.
        setUpTaskNotificationAlarmManager(taskId);

        Log.d(TAG, ">> [extendRepeatableTaskDueDate]");
    }

    /**
     * Calls model removeTask method
     * @param taskId id of the task that must be removed
     * @param accessToken current user access token
     */
    @Override
    public void removeTask(int taskId, String accessToken) {
        Log.d(TAG, ">> [removeTask]");
        Log.d(TAG, "Input arguments:"
                + " Task ID = " + taskId
                + " Access token = " + accessToken);

        Log.d(TAG, "Calling the model removeTasks method.");
        ResultObject removeTaskResult = model.removeTask(taskId, accessToken);
        if(removeTaskResult.getResultMessage() != ResultMessage.SUCCESSFUL){
            view.showInternalErrorDialog();
            return;
        }

        view.showTaskCategoryFragmentTaskDeletedToast();

        Log.d(TAG, "<< [removeTask]");
    }

    /**
     * Calls the model deleteTokens method
     */
    @Override
    public void deleteTokens(String accessToken) {
        Log.d(TAG, ">> [deleteTokens]");
        Log.d(TAG, "Input argument: " + "Access token = " + accessToken);

        Log.d(TAG, "Calling the model getUserIdByAccessToken method.");
        ResultObject getUserIdResult = model.getUserIdByAccessToken(accessToken);
        if(getUserIdResult.getResultMessage() != ResultMessage.SUCCESSFUL){
            view.showInternalErrorDialog();
            return;
        }

        int userId = (int) getUserIdResult.getResultValue();
        Log.d(TAG, "User ID = " + userId);


        Log.d(TAG, "Calls model deleteTokens method.");
        model.deleteTokens(userId);
        ResultObject deleteTokensResult = model.getUserIdByAccessToken(accessToken);
        if(deleteTokensResult.getResultMessage() != ResultMessage.SUCCESSFUL){
            view.showInternalErrorDialog();
            return;
        }

        Log.d(TAG, "<< [deleteTokens]");
    }

    /**
     * Enables all task notifications and day summary notification if its enabled in settings.
     * Uses after device rebooting, task statuses refreshing, user log in.
     */
    @Override
    public void refreshesAllUserNotificationAlarms() {
        Log.d(TAG, ">> [refreshesAllUserNotificationAlarms]");

        // Day summary notification alarms.
        SharedPreferences sharedPref = android.preference.PreferenceManager
                .getDefaultSharedPreferences((MainActivity)view);

        boolean isDaySummaryNotificationEnabledInSettings = sharedPref.getBoolean(
                "pref_key_settings_day_summary", false);

        view.setUpDaySummaryNotificationAlarmManager(isDaySummaryNotificationEnabledInSettings);


        // Task notification alarms.

        // Get all tasks for current user.
        List<Task> taskList = getTasks(view.getAccessToken());

        // Refresh notification for every task.
        for (Task task : taskList) {
            setUpTaskNotificationAlarmManager(task.getId());
        }

        Log.d(TAG, "<< [refreshesAllUserNotificationAlarms]");
    }

    /**
     * Disables all task notifications and day summary notification.
     * Called when user logs out.
     */
    @Override
    public void disableAllUserNotificationAlarms() {
        Log.d(TAG, ">> [disableAllUserNotificationAlarms]");

        // Day summary notification alarms.
        view.setUpDaySummaryNotificationAlarmManager(false);


        // Task notification alarms.

        // Get all tasks for current user.
        List<Task> taskList = getTasks(view.getAccessToken());

        // Refresh notification for every task.
        for (Task task : taskList) {
            view.deleteTaskNotificationAlarm(task.getId());
        }

        Log.d(TAG, "<< [disableAllUserNotificationAlarms]");
    }

    /**
     * Enables task notification alarm if task is in a progress and task due time set.
     * @param taskId for notification creating
     */
    @Override
    public void setUpTaskNotificationAlarmManager(int taskId) {
        Log.d(TAG, ">> [setUpTaskNotificationAlarmManager]");

        Task currentTask = getTask(taskId, view.getAccessToken());

        if (currentTask.getTaskExecutionStatus() == TaskExecutionStatuses.inProgress) {
            if (currentTask.getDueDate() != null) {
                if (currentTask.getDueTime() != null) {
                    view.createTaskNotificationAlarm(taskId, currentTask.getDueDate(),
                            currentTask.getDueTime());
                    return;
                }

                Log.d(TAG, "<< [setUpTaskNotificationAlarmManager]");
            }
        }

        view.deleteTaskNotificationAlarm(taskId);
    }
}