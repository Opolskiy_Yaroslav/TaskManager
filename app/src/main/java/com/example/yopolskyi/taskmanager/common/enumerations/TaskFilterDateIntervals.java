package com.example.yopolskyi.taskmanager.common.enumerations;

public enum TaskFilterDateIntervals {
    noFilter("No filter"),
    today("Today"),
    thisWeek("This week"),
    thisMonth("This month"),
    thisYear("This year"),
    noDate("No date");

    private String friendlyName;

    TaskFilterDateIntervals(String friendlyName){
        this.friendlyName = friendlyName;
    }

    @Override public String toString(){
        return friendlyName;
    }
}
