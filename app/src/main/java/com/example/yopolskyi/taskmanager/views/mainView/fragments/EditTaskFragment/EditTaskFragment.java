package com.example.yopolskyi.taskmanager.views.mainView.fragments.EditTaskFragment;


import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.example.yopolskyi.taskmanager.R;
import com.example.yopolskyi.taskmanager.common.Constants;
import com.example.yopolskyi.taskmanager.common.Task;
import com.example.yopolskyi.taskmanager.common.Utils;
import com.example.yopolskyi.taskmanager.views.mainView.ToolbarMenuStates;
import com.example.yopolskyi.taskmanager.views.mainView.fragments.NewTaskFragment.NewTaskFragment;

public class EditTaskFragment extends NewTaskFragment {

    private final String TAG = "EditTaskFragment";

    private int selectedTaskId;
    private Task oldTask;


    // Constructors
    public EditTaskFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Bundle args = getArguments();

        if(args != null){
            selectedTaskId = args.getInt("selectedTaskId", -1);
        }
    }

    @Override
    public void onResume() {
        super.onResume();

        parentView.setActionBarTitle(getResources()
                .getString(R.string.activity_main_edit_task_fragment_toolbar_title));

        parentView.changeToolbarMenuState(ToolbarMenuStates.edit);
    }

    /**
     * Checks entered value and if calls presenter addTask method if values are correct
     */
    @Override
    public void onConfirmButtonClick(){
        Log.d(TAG, ">> [onConfirmButtonClick]");

        Log.d(TAG, "Calls presenter updateTask method");
        presenter.updateTask(oldTask.getId(), edTaskContent.getText().toString(),
                edDueDate.getText().toString(), edDueTime.getText().toString(), spinnerRepeatFrequency
                        .getSelectedItemPosition(), oldTask.getTaskExecutionStatus());

        Log.d(TAG, "<< [onConfirmButtonClick]");
    }

    /**
     * Initializes all fragment items
     */
    @Override
    protected void initItems(View view){
        super.initItems(view);

        Log.d(TAG, ">> [initItems]");

        // Fill the fields.
        oldTask = presenter.getTask(selectedTaskId, parentView.getAccessToken());

        edTaskContent.setText(oldTask.getContent());

        String dueDate = null;
        if(oldTask.getDueDate() != null){
            dueDate = Utils.convertDateToString(oldTask.getDueDate(), Constants.DATE_FORMAT_FOR_USER);
            edDueDate.setText(dueDate);
        }

        String dueTime = null;
        if(oldTask.getDueTime() != null){
            dueTime = Utils.convertDateToString(oldTask.getDueTime(), Constants.EXPIRATION_TIME_FORMAT);
            edDueTime.setText(dueTime);
        }

        if(oldTask.getRepeatFrequency() != null){
            spinnerRepeatFrequency.setSelection(oldTask.getRepeatFrequency().ordinal());
        }

        if(dueDate != null){
            edDueTime.setVisibility(View.VISIBLE);
            btnDueDateClear.setVisibility(View.VISIBLE);
            tvRepeat.setVisibility(View.VISIBLE);
            spinnerRepeatFrequency.setVisibility(View.VISIBLE);
        }

        if(dueTime != null){
            btnDueTimeClear.setVisibility(View.VISIBLE);
        }

        presenter.isDateValid(edDueDate.getText().toString());
        presenter.isTimeValid(edDueDate.getText().toString(), edDueTime.getText().toString());

        Log.d(TAG, "<< [initItems]");
    }

    /**
     * Checks if fields data match old task fields.
     * @return "false" if all fields are the same and "true" in another case.
     */
    @Override
    public boolean wereFieldsChanged() {

        if(edTaskContent.getText().toString().equals(oldTask.getContent())){
            return true;
        }

        String oldTaskDueDate = Utils.convertDateToString(oldTask.getDueDate(),
                Constants.DATE_FORMAT_FOR_USER);
        if(edDueDate.getText().toString().equals(oldTaskDueDate)){
            return true;
        }

        String oldTaskDueTime = Utils.convertDateToString(oldTask.getDueTime(),
                Constants.EXPIRATION_TIME_FORMAT);
        if(edDueTime.getText().toString().equals(oldTaskDueTime)){
            return true;
        }

        if(spinnerRepeatFrequency.getSelectedItemPosition() != oldTask.getRepeatFrequency().ordinal()){
            return true;
        }

        return false;
    }


    /**
     * Show task edited toast in this case.
     */
    @Override
    public void showTaskAddedToast(){
        Toast.makeText(getContext(),
                getString(R.string.activity_main_edit_task_fragment_task_edited_toast_message),
                Toast.LENGTH_LONG).show();
    }
}
