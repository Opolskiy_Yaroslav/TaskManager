package com.example.yopolskyi.taskmanager.common;

import com.example.yopolskyi.taskmanager.common.enumerations.AuthorizationTokenTypes;

import java.util.Date;

/**
 * Encapsulates user authorization token.
 */
public class AuthorizationToken {

    // Fields.
    private int userID;
    private String tokenValue;
    private Date tokenExpirationDate;
    private AuthorizationTokenTypes tokenType;

    // Constructor.

    public AuthorizationToken(int userID, String tokenValue, Date tokenExpirationDate, AuthorizationTokenTypes tokenType) {
        this.userID = userID;
        this.tokenValue = tokenValue;
        this.tokenExpirationDate = tokenExpirationDate;
        this.tokenType = tokenType;
    }

    // Getters and setters.

    public int getUserID() {
        return userID;
    }

    public void setUserID(int userID) {
        this.userID = userID;
    }

    public String getTokenValue() {
        return tokenValue;
    }

    public void setTokenValue(String tokenValue) {
        this.tokenValue = tokenValue;
    }

    public Date getTokenExpirationDate() {
        return tokenExpirationDate;
    }

    public void setTokenExpirationDate(Date tokenExpirationDate) {
        this.tokenExpirationDate = tokenExpirationDate;
    }

    public AuthorizationTokenTypes getTokenType() {
        return tokenType;
    }

    public void setTokenType(AuthorizationTokenTypes tokenType) {
        this.tokenType = tokenType;
    }

    // Methods.
    @Override
    public String toString() {
        return "User ID = " + getUserID() +
                " Token value =" + getTokenValue() +
                " Token expiration date = " + getTokenExpirationDate() +
                " Token type = " + getTokenType();
    }
}
