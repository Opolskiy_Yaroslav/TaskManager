package com.example.yopolskyi.taskmanager.views.mainView.fragments.SettingsFragment;

import android.os.Bundle;
import android.support.v7.preference.DialogPreference;
import android.support.v7.preference.PreferenceDialogFragmentCompat;
import android.text.format.DateFormat;
import android.view.View;
import android.widget.TimePicker;

import com.example.yopolskyi.taskmanager.R;

public class TimePreferenceFragmentCompat extends PreferenceDialogFragmentCompat {

    private TimePicker timePicker = null;

    public static TimePreferenceFragmentCompat newInstance(String key) {
        final TimePreferenceFragmentCompat fragment = new TimePreferenceFragmentCompat();
        final Bundle b = new Bundle(1);
        b.putString(ARG_KEY, key);
        fragment.setArguments(b);

        return fragment;
    }

    @Override
    protected void onBindDialogView(View view) {
        super.onBindDialogView(view);

        timePicker = (TimePicker) view.findViewById(R.id.activity_main_settings_fragment_time_picker);

        // Exception when there is no TimePicker
        if (timePicker == null) {
            throw new IllegalStateException("Dialog view must contain" +
                    " a TimePicker with id 'activity_main_settings_fragment_time_picker'");
        }

        // Get the time from the related Preference
        Integer minutesAfterMidnight = null;
        DialogPreference preference = getPreference();
        if (preference instanceof TimePreference) {
            minutesAfterMidnight = ((TimePreference) preference).getTime();
        }

        // Set the time to the TimePicker
        if (minutesAfterMidnight != null) {
            int hours = minutesAfterMidnight / 60;
            int minutes = minutesAfterMidnight % 60;
            boolean is24hour = DateFormat.is24HourFormat(getContext());

            timePicker.setIs24HourView(is24hour);
            timePicker.setCurrentHour(hours);
            timePicker.setCurrentMinute(minutes);
        }
    }

    @Override
    public void onDialogClosed(boolean positiveResult) {
        if (positiveResult) {
            int hours = timePicker.getCurrentHour();
            int minutes = timePicker.getCurrentMinute();
            int minutesAfterMidnight = (hours * 60) + minutes;

            // Get the related Preference and save the value
            DialogPreference preference = getPreference();
            if (preference instanceof TimePreference) {
                TimePreference timePreference = ((TimePreference) preference);

                timePreference.setTime(minutesAfterMidnight);
                timePreference.callChangeListener(minutesAfterMidnight);
            }
        }
    }
}
